﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CancelScene : MonoBehaviour
{
    public KeyCode[] cancelKey;
    public string SceneName;

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
            foreach (KeyCode key in cancelKey)
                if (Input.GetKeyDown(key))
                    SceneManager.LoadScene(SceneName);
    }
}
