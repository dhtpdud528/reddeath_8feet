﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using System;

public class SteamAchievements : MonoBehaviour
{
    public static float clearTime;
    public static bool finish;

    public static float runnimgDistance;
    public static int killCount_Total;
    public static int killCount_Melee;
    public static float reloadSpeed;
    public static int modingCount;
    public static int fullModing;
    public static int confusedCount;
    public static int bandageCount;

    public static int pushCount;

    public static float totalReceivedDamage;

    private void Start()
    {
        StartCoroutine(RecordPlayTime());
    }

    IEnumerator RecordPlayTime()
    {
        while (!finish)
        {
            clearTime += Time.deltaTime;
            if (clearTime >= (60*30))
                UnlockAchievement("ACHIEVEMENT_Compass");
            yield return null;
        }
        UnlockAchievement("ACHIEVEMENT_Saved");
        if (clearTime < 60 * 5)
            UnlockAchievement("ACHIEVEMENT_Laggard");
        if (modingCount <= 0)
            UnlockAchievement("ACHIEVEMENT_Economize");
    }
    public static void UnlockAchievement(string name)
    {
        if (FindObjectOfType<DifficultyProfile>().itemDistribution > 1) return;
        try
        {
            SteamUserStats.SetAchievement(name);
            SteamUserStats.StoreStats();
        }
        catch (InvalidOperationException e)
        {
        };
    }
    public static bool CheckAchievement(string name)
    {
        bool Check_It;
        try
        {
            SteamUserStats.GetAchievement(name, out Check_It); ;
        } catch (InvalidOperationException e)
        {
            Check_It = false;
        };
        
        return Check_It;
        //return false;
    }
}
