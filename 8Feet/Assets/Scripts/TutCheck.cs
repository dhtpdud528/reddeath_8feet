﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutCheck : MonoBehaviour
{
    public EventHandler tutEvent;
    // Start is called before the first frame update
    void Start()
    {
        if (SteamAchievements.CheckAchievement("ACHIEVEMENT_Survive"))
            tutEvent.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
