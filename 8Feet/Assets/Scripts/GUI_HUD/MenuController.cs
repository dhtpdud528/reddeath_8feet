﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Steamworks;

public class MenuController : MonoBehaviour
{
    private GameObject blackOut;
    public Animator anim;

    public PlayerProfile[] playerProfiles;
    public DifficultyProfile[] difficultyProfile;
    public OptionProfile[] optionProfile;

    private int profileIndex = 0;
    private Image stamp;
    //private GameObject chooseProfileButton;
    [SerializeField]private GameObject specialNote;
    private bool doneMain;

    // Use this for initialization
    void Awake()
    {
        stamp = GameObject.Find("CLASSIFIED").GetComponent<Image>();
        stamp.gameObject.SetActive(false);
        //chooseProfileButton = GameObject.Find("ChooseProfile");
        blackOut = GameObject.Find("BlackOut");
        anim = FindObjectOfType<MenuController>().gameObject.GetComponent<Animator>();
        specialNote.SetActive(false);
        FindObjectOfType<ModsProfile>().ResetMods();

    }
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 1;
        if (SteamAchievements.CheckAchievement("ACHIEVEMENT_Dead"))
            anim.SetBool("Clear", true);
    }

    // Update is called once per frame
    void Update()
    {
        if (!doneMain && Input.GetKeyDown(KeyCode.Escape))
        {
            anim.CrossFadeInFixedTime("Base", 0);
        }
    }
    public void setActiveSNote(bool i)
    {
        SteamAchievements.UnlockAchievement("ACHIEVEMENT_SpecialNote");
        specialNote.SetActive(i);
    }
    public void ButtonContinueGame()
    {

    }
    public void ButtonNewGame()
    {
        anim.CrossFadeInFixedTime("MainMenu", 0);
        //StartCoroutine(ChangeScene());
    }
    public void ButtonOption()
    {
    }
    public void ButtonQuitGame()
    {
        Application.Quit();
    }
    public void ButtonDifficultyCoward()
    {
        if (GameObject.Find("GameProfiles").GetComponent<DifficultyProfile>() != null)
            Destroy(GameObject.Find("GameProfiles").GetComponent<DifficultyProfile>());
        CopyComponent(difficultyProfile[0], GameObject.Find("GameProfiles"));
    }
    public void ButtonDifficultySuvival()
    {
        if (GameObject.Find("GameProfiles").GetComponent<DifficultyProfile>() != null)
            Destroy(GameObject.Find("GameProfiles").GetComponent<DifficultyProfile>());
        CopyComponent(difficultyProfile[1], GameObject.Find("GameProfiles"));
    }
    public void ButtonDifficultyBrave()
    {
        if (GameObject.Find("GameProfiles").GetComponent<DifficultyProfile>() != null)
            Destroy(GameObject.Find("GameProfiles").GetComponent<DifficultyProfile>());
        CopyComponent(difficultyProfile[2], GameObject.Find("GameProfiles"));
    }
    public void GoPlayerProfiles()
    {
        anim.CrossFadeInFixedTime("PlayerProfile", 0);
    }
    IEnumerator WaitSeconds(float t)
    {
        yield return new WaitForSeconds(t);
    }
    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(2f);
        anim.CrossFadeInFixedTime("FadeIn", 0);
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene("Loading");
    }
    public void ButtonNextProfile()
    {
        if (profileIndex < playerProfiles.Length-1)
            profileIndex++;
        else
            profileIndex = 0;
        for (int i = 0; i < playerProfiles.Length; i++)
        {
            if (i == profileIndex)
                playerProfiles[i].gameObject.SetActive(true);
            else
                playerProfiles[i].gameObject.SetActive(false);
        }
    }
    public void ButtonPreviousProfile()
    {
        if (profileIndex > 0)
            profileIndex--;
        else
            profileIndex = playerProfiles.Length-1;
        for (int i = 0; i < playerProfiles.Length; i++)
        {
            if (i == profileIndex)
                playerProfiles[i].gameObject.SetActive(true);
            else
                playerProfiles[i].gameObject.SetActive(false);
        }
    }
    public void ButtonChooseProfile()
    {
        doneMain = true;
        if (GameObject.Find("GameProfiles").GetComponent<PlayerProfile>() != null)
            Destroy(GameObject.Find("GameProfiles").GetComponent<PlayerProfile>());
        CopyComponent(playerProfiles[profileIndex],GameObject.Find("GameProfiles"));
        FindObjectOfType<musicOnOff>().originVol = 0;
        FindObjectOfType<musicOnOff>().spicker.volume = 0;
        stamp.gameObject.SetActive(true);
        StartCoroutine(ChangeScene());
    }
    public void ApplyMods(Transform modlist)
    {
        ModsProfile modsProfile = FindObjectOfType<ModsProfile>();

        for (int i =0; i< modlist.childCount; i++)
            modlist.GetChild(i).GetComponent<UISpecialModComponent>().ApplyMods(modsProfile);
    }
    Component CopyComponent(Component original, GameObject destination)
    {
        System.Type type = original.GetType();
        Component copy = destination.AddComponent(type);
        // Copied fields can be restricted with BindingFlags
        System.Reflection.FieldInfo[] fields = type.GetFields();
        foreach (System.Reflection.FieldInfo field in fields)
        {
            field.SetValue(copy, field.GetValue(original));
        }
        return copy;
    }
}
