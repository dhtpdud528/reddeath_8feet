﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemMenu : MonoBehaviour
{
    
    public Item itemInfo;
    public Text scrapsNum;
    private void Awake()
    {
        
        scrapsNum = transform.GetChild(1).GetChild(1).GetComponent<Text>();
    }
    public void Start()
    {
        
    }
    public void OpenModification()
    {
        if (!(itemInfo is ItemGun)) return;
        GameManager.instance.uiHand.transform.parent.gameObject.SetActive(false);
        GameManager.instance.uiMod.transform.parent.parent.gameObject.SetActive(true);
        GameManager.instance.uiMod.item = itemInfo;
        GameManager.instance.uiMod.UpdateUI();
    }
    public void DoDisassemble()
    {
        if (itemInfo is ItemAmmo|| Input.GetKey(KeyCode.LeftShift))
        {
            GameManager.instance.uiCount.gameObject.SetActive(true);
            GameManager.instance.uiCount.splitItem = itemInfo.gameObject;
            GameManager.instance.uiCount.from = GameManager.instance.playerInv.backpackInv;
            GameManager.instance.uiCount.toTypes = UICount.ToTypes.toDisassemble;
            
        }
        else
        {
            GameManager.instance.playerInv.DropItem_fromBackpack(itemInfo);
            Destroy(itemInfo.gameObject);
            GameManager.instance.playerInv.scraps += itemInfo.scraps;
        }
        GameManager.instance.playerInv.UIUpdate();
    }
}
