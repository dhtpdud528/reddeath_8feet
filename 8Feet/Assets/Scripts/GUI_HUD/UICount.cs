﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICount : MonoBehaviour
{
    private GameManager gm;
    public GameObject splitItem;
    public List<GameObject> moveItems;
    public enum ToTypes { toInv, toVicini, toBox, toDisassemble }
    public ToTypes toTypes;

    public List<GameObject> from = new List<GameObject>();
    // Start is called before the first frame update
    void Awake()
    {
        
    }
    private void Start()
    {
        
    }
    public void OnDisable()
    {
        GetComponent<InputField>().text = "";
    }
    public void OnEnable()
    {
        GetComponent<InputField>().Select();
        GetComponent<InputField>().ActivateInputField();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void SplitItems()
    {
        moveItems.Clear();
        int count = Convert.ToInt32(GetComponent<InputField>().text);
        if (splitItem.GetComponent<Item>() is ItemAmmo)
            moveItems = searchAmmo(from, splitItem, count);
        else
            moveItems = searchItems(from, splitItem.GetComponent<Item>().itemName, count);
        if (moveItems == null) return;
        switch (toTypes)
        {
            case ToTypes.toInv:
                for (int i = 0; i < moveItems.Count; i++)
                {
                    GameManager.instance.playerInv.PickUpItemToinv(moveItems[i]);
                    if (GameManager.instance.uiItemBox.box != null)
                        GameManager.instance.uiItemBox.box.BoxInv.Remove(moveItems[i]);
                }
                break;
            case ToTypes.toVicini:
                for (int i = 0; i < moveItems.Count; i++)
                {
                    if (splitItem.GetComponent<Item>() is ItemAmmo)
                    {
                        moveItems[i].GetComponent<Item>().DropItem_fromPlayer();
                        continue;
                    }
                    GameManager.instance.playerInv.DropItem_fromBackpack(moveItems[i].GetComponent<Item>());
                }
                break;
            case ToTypes.toBox:
                for (int i = 0; i < moveItems.Count; i++)
                {
                    GameManager.instance.uiItemBox.box.AddItem(moveItems[i]);
                    GameManager.instance.playerInv.DrawItem_fromBackpack(moveItems[i].GetComponent<Item>());
                }
                break;
            case ToTypes.toDisassemble:
                for (int i = 0; i < moveItems.Count; i++)
                {
                    if (splitItem.GetComponent<Item>() is ItemAmmo)
                    {
                        moveItems[i].GetComponent<Item>().DropItem_fromPlayer();
                        GameManager.instance.playerInv.scraps += moveItems[i].GetComponent<Item>().scraps * moveItems[i].GetComponent<Item>().totalNum;
                        continue;
                    }
                    GameManager.instance.playerInv.DropItem_fromBackpack(moveItems[i].GetComponent<Item>());
                    GameManager.instance.playerInv.scraps += moveItems[i].GetComponent<Item>().scraps;
                }
                foreach (GameObject item in moveItems)
                    Destroy(item);
                break;
        }
        GameManager.instance.playerInv.UIUpdate();
        gameObject.SetActive(false);
    }
    public List<GameObject> searchItems(List<GameObject> inv, string search, int count)
    {
        List<GameObject> findList = new List<GameObject>();
        int foundCount = 0;
        if (inv.Count == 0)
            return null;
        for (int i = 0; i < inv.Count; i++)
        {
            if (foundCount < count && inv[i] != null && inv[i].GetComponent<Item>().itemName == search)
            {
                findList.Add(inv[i]);
                foundCount++;
            }
        }
        return findList;
    }
    public List<GameObject> searchAmmo(List<GameObject> inv, GameObject search, int count)
    {
        if (count <= 0)
            return null;
        List<GameObject> findList = new List<GameObject>();

        GameObject ammo = Instantiate(GameManager.instance.itemPactory.FindItem(search.GetComponent<Item>().itemName));
        int leftCount = count;
        int returnCount = 0;

        if (inv.Count == 0)
            return null;
        for (int i = 0; i < inv.Count; i++)
        {
            if (leftCount > 0 && inv[i] != null && inv[i].GetComponent<Item>().itemName == search.GetComponent<Item>().itemName)
            {
                if (inv[i].GetComponent<Item>().totalNum - leftCount <= 0)
                {
                    int originTotalNum = inv[i].GetComponent<Item>().totalNum;
                    inv[i].GetComponent<Item>().AddTotalNum(-leftCount);
                    leftCount -= originTotalNum;
                    returnCount += originTotalNum;
                }
                else
                {
                    returnCount += leftCount;
                    inv[i].GetComponent<Item>().AddTotalNum(-leftCount);
                    leftCount = 0;
                }
            }
        }


        ammo.GetComponent<ItemAmmo>().totalNum = returnCount;
        findList.Add(ammo);


        return findList;
    }

}
