﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIBox : MonoBehaviour, IDropHandler, IPointerClickHandler
{
    
    public ItemBox box;
    [SerializeField] private GameObject iconItem;
    public int checkTime;
    private Transform list;

    // Start is called before the first frame update
    void Awake()
    {
        
        checkTime = 2;
        list = transform.parent.GetChild(1);
    }
    public void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        if (checkTime < 2)
        {
            checkTime++;
            UpdateUIInfo();
        }
    }
    public void UpdateUIInfo()
    {
        int maxChildCount = list.childCount;
        for (int i = 0; i < maxChildCount; i++)
            GameObject.DestroyImmediate(list.GetChild(0).gameObject);
        for (int i = 0; i < box.BoxInv.Count; i++)
        {
            if (list.childCount == 0)
            {
                GameObject item = Instantiate(iconItem);
                item.GetComponent<UIItemComponent>().item = box.BoxInv[i].GetComponent<Item>().gameObject;
                item.transform.SetParent(list);
                item.GetComponent<UIItemComponent>().parent = list.GetComponent<UIBox>();
                item.transform.localScale = new Vector3(1, 1, 1);
                item.transform.GetChild(1).GetComponent<Text>().text = box.BoxInv[i].GetComponent<Item>().itemName;
                if (box.BoxInv[i].GetComponent<Item>().isModed)
                    item.transform.GetChild(1).GetComponent<Text>().text += " (Moded)";
                item.transform.GetChild(2).GetComponent<Text>().text = box.BoxInv[i].GetComponent<Item>().totalNum.ToString();
            }
            else
            {
                bool exist = false;
                for (int z = 0; z < list.childCount; z++)
                {
                    if (box.BoxInv[i].GetComponent<Item>().itemName.Equals(list.GetChild(z).GetChild(1).GetComponent<Text>().text))
                    {
                        if (GameManager.instance.playerInv.backpackInv[i].GetComponent<Item>().isModed)
                            continue;
                        list.GetChild(z).GetChild(2).GetComponent<Text>().text = (System.Convert.ToInt32(list.GetChild(z).GetChild(2).GetComponent<Text>().text) + 1).ToString();
                        exist = true;
                    }
                }
                if (!exist)
                {
                    GameObject item = Instantiate(iconItem);
                    item.GetComponent<UIItemComponent>().item = box.BoxInv[i].GetComponent<Item>().gameObject;
                    item.transform.SetParent(list);
                    item.GetComponent<UIItemComponent>().parent = list.GetComponent<UIBox>();
                    item.transform.localScale = new Vector3(1, 1, 1);
                    item.transform.GetChild(1).GetComponent<Text>().text = box.BoxInv[i].GetComponent<Item>().itemName;
                    if (box.BoxInv[i].GetComponent<Item>().isModed)
                        item.transform.GetChild(1).GetComponent<Text>().text += " (Moded)";
                    item.transform.GetChild(2).GetComponent<Text>().text = box.BoxInv[i].GetComponent<Item>().totalNum.ToString();
                }
            }
        }
    }
    public GameObject searchItem(Item item)
    {
        GameObject find = null;
        if (box.BoxInv.Count == 0)
            return null;
        for (int i = 0; i < box.BoxInv.Count; i++)
        {
            if (box.BoxInv[i] != null && box.BoxInv[i].GetComponent<Item>().id == item.id)
            {
                find = box.BoxInv[i];
                break;
            }
        }
        return find;
    }

    public void OnDrop(PointerEventData eventData)
    {
        UIItemComponent droppedItem = eventData.pointerDrag.GetComponent<UIItemComponent>();
        if(searchItem(droppedItem.item.GetComponent<Item>()) == null)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                GameManager.instance.uiCount.gameObject.SetActive(true);
                GameManager.instance.uiCount.GetComponent<UICount>().splitItem = eventData.pointerDrag.GetComponent<UIItemComponent>().item;
                GameManager.instance.uiCount.from = GameManager.instance.playerInv.backpackInv;
                GameManager.instance.uiCount.toTypes = UICount.ToTypes.toBox;
            }
            else
            {
                box.AddItem(droppedItem.item);
                GameManager.instance.playerInv.DrawItem_fromBackpack(droppedItem.item.GetComponent<Item>());
            }
            droppedItem.item.transform.position = new Vector3(0, -1000, 0);
            GameManager.instance.playerInv.UIUpdate();
        }
        
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.pointerDrag.GetComponent<UIItemComponent>() == null) return;
        UIItemComponent droppedItem = eventData.pointerDrag.GetComponent<UIItemComponent>();
        if (Input.GetKeyUp(KeyCode.Mouse1))
            GameManager.instance.uiInv.OnDrop(eventData);
    }
}
