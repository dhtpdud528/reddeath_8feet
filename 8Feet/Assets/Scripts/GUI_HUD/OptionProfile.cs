﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class OptionProfile : MonoBehaviour
{
    //public bool bloom = true;
    //public bool motionBlur = true;
    //public bool vignette = true;
    //public bool grain = true;
    //public bool ambientOcclusion = true;
    public float mouseSensitivity;

    public enum languageList { K = 0, E = 1 }
    public languageList language = languageList.E;

    // Start is called before the first frame update
    void Start()
    {
        mouseSensitivity = 2;
        if (PlayerPrefs.GetString("use_ambient_occlusion") != null)
            ApplySettings();
        else
            SaveOptions();
    }

    internal void SetMouseSensitivity(float value)
    {
        throw new NotImplementedException();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SaveOptions()
    {
        //PlayerPrefs.SetString("use_bloom", bloom.ToString());
        //PlayerPrefs.SetString("use_motionBlur", motionBlur.ToString());
        //PlayerPrefs.SetString("use_vignette", vignette.ToString());
        //PlayerPrefs.SetString("use_grain", grain.ToString());
        //PlayerPrefs.SetString("use_ambient_occlusion", ambientOcclusion.ToString());
        PlayerPrefs.SetInt("language", (int)language);
    }
    public void ApplySettings()
    {
        //ambientOcclusion = bool.Parse(PlayerPrefs.GetString("use_ambient_occlusion"));
        //bloom = bool.Parse(PlayerPrefs.GetString("use_bloom"));
        //motionBlur = bool.Parse(PlayerPrefs.GetString("use_motionBlur"));
        //vignette = bool.Parse(PlayerPrefs.GetString("use_vignette"));
        //grain = bool.Parse(PlayerPrefs.GetString("use_grain"));
        languageList language = (languageList)PlayerPrefs.GetInt("language");
        // ...
        // Get all volumes (even inactive ones). Assuming you're not
        // using QuickVolume, you can cache this list.
        //var all_volumes = Resources.FindObjectsOfTypeAll<PostProcessVolume>();

        //foreach (var volume in all_volumes)
        //{
        //    var vanilla = volume.sharedProfile;
        //    if (vanilla == null)
        //        continue;
        //    var active = volume.profile;
        //    ApplyRestriction<AmbientOcclusion>(vanilla, active, ambientOcclusion);
        //    ApplyRestriction<Bloom>(vanilla, active, bloom);
        //    ApplyRestriction<MotionBlur>(vanilla, active, motionBlur);
        //    ApplyRestriction<Vignette>(vanilla, active, vignette);
        //    ApplyRestriction<Grain>(vanilla, active, grain);
        //    // ...
        //}
    }

    //void ApplyRestriction<T>(PostProcessProfile vanilla, PostProcessProfile active, bool is_allowed)
    //    where T : PostProcessEffectSettings
    //{
    //    T vanilla_layer = null;
    //    T active_layer = null;
    //    vanilla.TryGetSettings(out vanilla_layer);
    //    active.TryGetSettings(out active_layer);
    //    if (vanilla_layer != null && active_layer != null)
    //        active_layer.enabled.value = is_allowed && vanilla_layer.enabled.value;
    //}
}
