﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIModComponent : MonoBehaviour
{
    
    public Item item;
    public enum ModNames { Recoil, FireRate, BulletsPerMag, ReloadSpeed, Noise }
    public ModNames modName;
    public float amount;
    // Start is called before the first frame update
    private void Awake()
    {
        
    }
    void Start()
    {
        
        transform.GetChild(0).GetComponent<Text>().text = modName.ToString();
        transform.GetChild(1).GetComponent<Text>().text = amount.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void DoModing()
    {
        float cost = item.modCost;
        if (GameManager.instance.modsProfile != null)
                cost *= GameManager.instance.modsProfile.itemModCosts;
        if (GameManager.instance.playerInv.scraps >= cost && (amount < 0 || item.modedCount < item.MaxModCount))
        {
            item.modedList.Add(modName.ToString());
            switch (modName)
            {
                case ModNames.Recoil:           ((ItemGun)item).recoil          -= ((ItemGun)item).recoilOrigin * (amount / 100); break;
                case ModNames.FireRate:         ((ItemGun)item).fireRate        -= ((ItemGun)item).fireRateOrigin * (amount / 100); break;
                case ModNames.BulletsPerMag:    ((ItemGun)item).bulletsPerMag   += (int)(((ItemGun)item).bulletsPerMagOrigin * (amount / 100)); break;
                case ModNames.ReloadSpeed:      ((ItemGun)item).reloadSpeed     += ((ItemGun)item).reloadSpeedOrigin * (amount / 100); break;
                case ModNames.Noise:            ((ItemGun)item).noise           -= ((ItemGun)item).noiseOrigin * (amount / 100); break;
            }
            if (amount > 0)
                item.modedCount++;
            else
                item.modedCount--;
            SteamAchievements.modingCount++;
            GameManager.instance.playerInv.scraps -= (int)cost;
            item.modCost = (int)(item.modCost * 1.5f);
            item.isModed = true;
            GameManager.instance.uiMod.cost.text = cost.ToString();
            GameManager.instance.uiMod.modBar.fillAmount = (float)item.modedCount / (float)item.MaxModCount;
            GameManager.instance.uiMod.modCount.text = item.modedCount + " / " + item.MaxModCount;
            if (item.modedCount >= item.MaxModCount)
            {
                SteamAchievements.UnlockAchievement("ACHIEVEMENT_Manualdexterity");
                if (modName == ModNames.ReloadSpeed)
                    SteamAchievements.UnlockAchievement("ACHIEVEMENT_Fast_hands");
                SteamAchievements.fullModing++;
            }
            if(SteamAchievements.fullModing >= 3)
                SteamAchievements.UnlockAchievement("ACHIEVEMENT_Overconsumption");
            GameManager.instance.playerInv.UIUpdate();
            GameManager.instance.uiMod.UpdateUI();
        }
        else
        {
            //불가능SFX
        }
    }
}
