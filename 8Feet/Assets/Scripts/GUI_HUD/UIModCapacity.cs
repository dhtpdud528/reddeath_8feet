﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIModCapacity : MonoBehaviour
{
    private int curCost;
    [SerializeField]private int maxCost;
    private Image modBar;
    private Text modCount;
    // Start is called before the first frame update
    private void Awake()
    {
        modBar = GameObject.Find("ModFillMask").GetComponent<Image>();
        modCount = GameObject.Find("Cur&MaxCout").GetComponent<Text>();
        
    }
    void Start()
    {
        modBar.fillAmount = curCost / maxCost;
        modCount.text = curCost + "/" + maxCost;
    }

    // Update is called once per frame
    public bool AddCosts(int addCost)
    {
        if(curCost + addCost > maxCost)
            return false;
        curCost += addCost;
        modBar.fillAmount = float.Parse(curCost.ToString()) / float.Parse(maxCost.ToString());
        
        modCount.text = curCost + "/" + maxCost;
        return true;
    }
}
