﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIModNet : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        UISpecialModComponent droppedMod = eventData.pointerDrag.GetComponent<UISpecialModComponent>();
        if(droppedMod.canUse)
            droppedMod.ResetPosition();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
