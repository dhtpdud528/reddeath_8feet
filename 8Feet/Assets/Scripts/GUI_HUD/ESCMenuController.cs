﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class ESCMenuController : MonoBehaviour {

    
    // Update is called once per frame
    public void Awake()
    {
        
    }
    void Update()
    {

    }
    private void Start()
    {
    }
    public void ButtonGoMain()
    {
        Destroy(GameManager.instance.dontDestroyOnOtherScene.gameObject);
        SceneManager.LoadScene("Main");
    }
    public void ButtonQuitGame()
    {
        Application.Quit();
    }
    public void ButtonResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void OnEnable()
    {
        if (GameManager.instance != null && GameManager.instance.playerController != null)
        {
            foreach (GameObject ui in GameManager.instance.uiHowtoPlay.tuts)
                ui.SetActive(false);
            GameManager.instance.uiHowtoPlay.buttons.SetActive(false);
            GameManager.instance.playerController.m_MouseLook.SetCursorLock(false);
            GameManager.instance.playerController.m_MouseLook.XSensitivity = 0;
            GameManager.instance.playerController.m_MouseLook.YSensitivity = 0;
        }
        
    }
    public void OnDisable()
    {
        GameManager.instance.playerController.m_MouseLook.SetCursorLock(true);
        GameManager.instance.playerController.m_MouseLook.XSensitivity = GameManager.instance.optionProfile.mouseSensitivity;
        GameManager.instance.playerController.m_MouseLook.YSensitivity = GameManager.instance.optionProfile.mouseSensitivity;
        GameManager.instance.graphicSetting.gameObject.SetActive(false);
    }
}
