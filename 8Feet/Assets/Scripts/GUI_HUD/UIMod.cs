﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIMod : MonoBehaviour
{
    
    [SerializeField] private List<GameObject> componentsGun;
    [SerializeField] private List<GameObject> componentsGunLoss;
    [SerializeField] private List<GameObject> componentsMelee;
    public Item item;
    public Text cost;
    public Image modBar;
    public Text modCount;
    public Image icon;

    // Start is called before the first frame update
    void Awake()
    {
        
        cost = GameObject.Find("ModCost").transform.GetChild(0).GetComponent<Text>();
        modBar = GameObject.Find("ModFillMask").GetComponent<Image>();
        modCount = GameObject.Find("Cur&MaxCout").GetComponent<Text>();
    }
    private void Start()
    {
        
    }
    public void UpdateUI()
    {
        icon.sprite = item.icon;
        cost.text = item.modCost.ToString();
        modBar.fillAmount = (float)item.modedCount / (float)item.MaxModCount;
        modCount.text = item.modedCount + " / " + item.MaxModCount;
        
        int maxChildCount = transform.childCount;
        for (int i = 0; i < maxChildCount; i++)
            DestroyImmediate(transform.GetChild(0).gameObject);
        if (item is ItemGun)
        {
            for (int i = 0; i < componentsGun.Count; i++)
            {
                GameObject component = Instantiate(componentsGun[i]);
                component.transform.SetParent(transform);
                component.GetComponent<UIModComponent>().item = item;
                component.transform.localScale = new Vector3(1, 1, 1);
            }
            if (GameManager.instance.playerProfile != null && GameManager.instance.playerProfile.manualDexterity || GameManager.instance.modsProfile != null && GameManager.instance.modsProfile.manualDexterity)
                for (int i = 0; i < componentsGunLoss.Count; i++)
                {
                    GameObject component = Instantiate(componentsGunLoss[i]);
                    component.transform.SetParent(transform);
                    component.GetComponent<UIModComponent>().item = item;
                    component.transform.localScale = new Vector3(1, 1, 1);
                }
        }

    }
}
