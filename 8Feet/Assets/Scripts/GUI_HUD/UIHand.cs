﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIHand : MonoBehaviour, IDropHandler, IPointerClickHandler
{
    private GameManager gm;
    [SerializeField] private GameObject iconItem;
    public int checkTime;
    private Transform list;

    // Start is called before the first frame update
    void Awake()
    {
        
        checkTime = 2;
        list = transform.parent.GetChild(1);
    }
    public void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        if (checkTime < 2)
        {
            checkTime++;
            UpdateUIInfo();
        }
    }
    private void UpdateUIInfo()
    {
        int maxChildCount = list.childCount;
        for (int i = 0; i < maxChildCount; i++)
            GameObject.DestroyImmediate(list.GetChild(0).gameObject);
        for (int i = 0; i < GameManager.instance.playerInv.quickInv.Length; i++)
        {
            GameObject item = Instantiate(iconItem);
            item.GetComponent<UIItemComponent>().item = GameManager.instance.playerInv.quickInv[i];
            item.transform.SetParent(list);
            item.GetComponent<UIItemComponent>().parent = list.GetComponent<UIHand>();
            item.transform.localScale = new Vector3(1, 1, 1);
            item.transform.GetChild(1).GetComponent<Text>().text = i+1 + ". " + GameManager.instance.playerInv.quickInv[i].GetComponent<Item>().itemName;
            if (GameManager.instance.playerInv.quickInv[i].GetComponent<Item>().modedCount > 0)
                item.transform.GetChild(1).GetComponent<Text>().text += " (Moded)";
            item.transform.GetChild(2).GetComponent<Text>().text = GameManager.instance.playerInv.quickInv[i].GetComponent<Item>().totalNum.ToString();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.pointerDrag.GetComponent<UIItemComponent>() == null) return;
        UIItemComponent droppedItem = eventData.pointerDrag.GetComponent<UIItemComponent>();
        if (droppedItem.item.GetComponent<Item>().itemName == "") return;

        if (Input.GetKeyUp(KeyCode.Mouse1))
            GameManager.instance.uiInv.OnDrop(eventData);
    }

    public void OnDrop(PointerEventData eventData)
    {
        UIItemComponent droppedItem = eventData.pointerDrag.GetComponent<UIItemComponent>();
        if (GameManager.instance.playerInv.SearchItem_inBackpack(droppedItem.item.GetComponent<Item>()) != null)
        {
            GameManager.instance.playerInv.DrawItem_fromBackpack(droppedItem.item.GetComponent<Item>());
            GameManager.instance.playerInv.EquipItemToHand(droppedItem.item);
            GameManager.instance.playerInv.UIUpdate();
        }
    }
}
