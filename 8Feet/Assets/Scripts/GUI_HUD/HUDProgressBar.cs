﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDProgressBar : MonoBehaviour {

    public Image imageProgressBar;

    private void Awake()
    {
        imageProgressBar = GetComponent<Image>();
    }
    // Use this for initialization
    void Start () {
        imageProgressBar.fillAmount = 0;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
