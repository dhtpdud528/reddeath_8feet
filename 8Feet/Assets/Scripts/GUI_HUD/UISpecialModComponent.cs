﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UISpecialModComponent : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Text uiNameText;
    public Text uiCostText;
    public Text uiModInstruction;
    public string modName;
    public int cost;
    public string modInstruction;
    public UISpecialMod ui;

    private PointerEventData pointerEventData;
    private Transform originalParent;
    private Vector2 offset;

    public bool canUse;

    private Image iconImage;
    public Sprite disIcon;
    public enum ModsTypes
    {
        immunityConfusion,
        immunityBleeding,
        manualDexterity,
        maxStemina,
        maxHealth,
        recoverySteminaSpeed,
        moveSpeed,
        reloadSpeed,
        meleeDamage,
        noiseLimit,
        itemModCosts,

        immunityPushed,
        itemDistribution,
        zombieHealth,
        zombierMoveSpeed,
        zombieDamage,
        withstand,
        compass
    }
    public float amount;
    public ModsTypes types;
    public void Awake()
    {
        iconImage = transform.GetChild(2).GetComponent<Image>();
        uiNameText = transform.GetChild(0).GetComponent<Text>();
        uiCostText = transform.GetChild(1).GetComponent<Text>();
        uiModInstruction = GameObject.Find("TextInstruction").GetComponent<Text>();
        if (SteamAchievements.CheckAchievement("ACHIEVEMENT_" + modName))
            canUse = true;
        if (!canUse)
        {
            iconImage.sprite = disIcon;
            uiNameText.text += "\n(Undiscovered)";
        }
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!canUse) return;
        offset = eventData.position - new Vector2(this.transform.position.x, this.transform.position.y);
        originalParent = transform.parent;
        transform.SetParent(transform.parent.parent.parent);
        transform.position = eventData.position;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!canUse) return;
        pointerEventData = eventData;
        transform.position = eventData.position - offset;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }
    public void PrintModInstruction()
    {
        uiModInstruction.text = modName + "\nCosts : " + cost +
            "\n" + modInstruction;
        if (amount != 0)
            uiModInstruction.text += "\n(x" + amount + ")";
    }
    public void ResetPosition()
    {
        transform.SetParent(originalParent);
        transform.position = originalParent.transform.position;
    }
    public void ApplyMods(ModsProfile profile)
    {
        switch (types)
        {
            case ModsTypes.immunityBleeding:     profile.immunityBleeding = true; break;
            case ModsTypes.immunityConfusion:    profile.immunityConfusion = true; break;
            case ModsTypes.immunityPushed:       profile.immunityPushed = true; break;
            case ModsTypes.itemDistribution:     profile.itemDistribution = amount; break;
            case ModsTypes.itemModCosts:         profile.itemModCosts = amount; break;
            case ModsTypes.manualDexterity:      profile.manualDexterity = true; break;
            case ModsTypes.maxHealth:            profile.maxHealth = amount; break;
            case ModsTypes.maxStemina:           profile.maxStemina = amount; break;
            case ModsTypes.meleeDamage:          profile.meleeDamage = amount; break;
            case ModsTypes.moveSpeed:            profile.moveSpeed = amount; break;
            case ModsTypes.recoverySteminaSpeed: profile.recoverySteminaSpeed = amount; break;
            case ModsTypes.reloadSpeed:          profile.reloadSpeed = amount; break;
            case ModsTypes.noiseLimit:           profile.noiseLimit = amount; break;
            case ModsTypes.zombieDamage:         profile.zombieDamage = amount; break;
            case ModsTypes.zombieHealth:         profile.zombieHealth = amount; break;
            case ModsTypes.zombierMoveSpeed:     profile.zombierMoveSpeed = amount; break;
            case ModsTypes.withstand:            profile.withstand = true; break;
            case ModsTypes.compass:              profile.compass = true; break;
        }
    }
}
