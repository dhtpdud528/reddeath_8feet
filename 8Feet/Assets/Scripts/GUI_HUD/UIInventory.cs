﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class UIInventory : MonoBehaviour, IDropHandler, IPointerClickHandler
{
    
    [SerializeField] private GameObject iconItem;
    public int checkTime;
    private GameObject splitItem;
    [SerializeField] private Transform list;

    [SerializeField] private GameObject itemMenu;

    // Start is called before the first frame update
    void Awake()
    {
        checkTime = 2;
        list = transform.parent.GetChild(1);
    }
    void Start()
    {
        GameManager.instance.uiCount.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        GameManager.instance.hudInvInfo.activeHUD();
        GameManager.instance.hudStateInfo.activeHUD();
        if (checkTime < 2)
        {
            checkTime++;
            UpdateUIInfo();
        }
    }
    private void UpdateUIInfo()
    {
        int maxChildCount = list.childCount;
        for (int i = 0; i < maxChildCount; i++)
            GameObject.DestroyImmediate(list.GetChild(0).gameObject);
        for (int i = 0; i < GameManager.instance.playerInv.backpackInv.Count; i++)
        {
            if (list.childCount == 0)
            {
                GameObject item = Instantiate(iconItem);
                item.GetComponent<UIItemComponent>().item = GameManager.instance.playerInv.backpackInv[i];
                item.transform.SetParent(list);
                item.GetComponent<UIItemComponent>().parent = list.GetComponent<UIInventory>();
                item.transform.localScale = new Vector3(1, 1, 1);
                item.transform.GetChild(1).GetComponent<Text>().text = GameManager.instance.playerInv.backpackInv[i].GetComponent<Item>().itemName;
                if (GameManager.instance.playerInv.backpackInv[i].GetComponent<Item>().isModed)
                    item.transform.GetChild(1).GetComponent<Text>().text += " (Moded)";
                item.transform.GetChild(2).GetComponent<Text>().text = GameManager.instance.playerInv.backpackInv[i].GetComponent<Item>().totalNum.ToString();
            }
            else
            {
                bool exist = false;
                for (int z = 0; z < list.childCount; z++)
                    if (GameManager.instance.playerInv.backpackInv[i].GetComponent<Item>().itemName == list.GetChild(z).GetChild(1).GetComponent<Text>().text)
                    {
                        if (GameManager.instance.playerInv.backpackInv[i].GetComponent<Item>().isModed)
                            continue;
                        list.GetChild(z).GetChild(2).GetComponent<Text>().text = (System.Convert.ToInt32(list.GetChild(z).GetChild(2).GetComponent<Text>().text) + 1).ToString();
                        exist = true;
                    }
                if (!exist)
                {
                    GameObject item = Instantiate(iconItem);
                    item.GetComponent<UIItemComponent>().item = GameManager.instance.playerInv.backpackInv[i];
                    item.transform.SetParent(list);
                    item.GetComponent<UIItemComponent>().parent = list.GetComponent<UIInventory>();
                    item.transform.localScale = new Vector3(1, 1, 1);
                    item.transform.GetChild(1).GetComponent<Text>().text = GameManager.instance.playerInv.backpackInv[i].GetComponent<Item>().itemName;
                    if (GameManager.instance.playerInv.backpackInv[i].GetComponent<Item>().isModed)
                        item.transform.GetChild(1).GetComponent<Text>().text += " (Moded)";
                    item.transform.GetChild(2).GetComponent<Text>().text = GameManager.instance.playerInv.backpackInv[i].GetComponent<Item>().totalNum.ToString();
                }
            }
        }
    }
    public void OnEnable()
    {
        if (GameManager.instance != null&&GameManager.instance.playerController != null)
        {
            GameManager.instance.playerController.m_MouseLook.SetCursorLock(false);
            GameManager.instance.playerController.m_MouseLook.XSensitivity = 0;
            GameManager.instance.playerController.m_MouseLook.YSensitivity = 0;
        }

    }
    public void OnDisable()
    {
        if (GameManager.instance.playerController != null)
        {
            if (GameManager.instance.uiMenu != null)
            {
                GameManager.instance.uiMenu.gameObject.SetActive(false);
                GameManager.instance.uiHand.transform.parent.gameObject.SetActive(true);
                GameManager.instance.uiMod.transform.parent.parent.gameObject.SetActive(false);
            }
            GameManager.instance.uiCount.gameObject.SetActive(false);
            GameManager.instance.playerController.m_MouseLook.SetCursorLock(true);
            GameManager.instance.playerController.m_MouseLook.XSensitivity = 2;
            GameManager.instance.playerController.m_MouseLook.YSensitivity = 2;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        UIItemComponent droppedItem = eventData.pointerDrag.GetComponent<UIItemComponent>();
        if (GameManager.instance.playerInv.SearchItem_inBackpack(droppedItem.item.GetComponent<Item>()) == null)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                GameManager.instance.uiCount.gameObject.SetActive(true);
                GameManager.instance.uiCount.GetComponent<UICount>().splitItem = droppedItem.item;
                if (GameManager.instance.uiVicini.gameObject.active)
                    GameManager.instance.uiCount.from = GameManager.instance.uiVicini.vicinityInv;
                else
                    GameManager.instance.uiCount.from = GameManager.instance.uiItemBox.box.BoxInv;
                GameManager.instance.uiCount.toTypes = UICount.ToTypes.toInv;
            }
            else
            {
                if (droppedItem.parent is UIHand)
                {
                    int i = Convert.ToInt32(droppedItem.transform.GetChild(1).GetComponent<Text>().text.Substring(0, 1)) - 1;
                    droppedItem.item.GetComponent<Item>().DropItem_fromPlayer();
                    GameManager.instance.playerInv.quickInv[i].transform.SetParent(null);
                    GameManager.instance.playerInv.quickInv[i] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                    GameManager.instance.playerInv.quickInv[i].transform.SetParent(GameManager.instance.hand.transform);
                    if (GameManager.instance.playerInv.currentAdress == i)
                        GameManager.instance.playerAnimCon.NoItem();
                }
                GameManager.instance.playerInv.PickUpItemToinv(droppedItem.item, droppedItem.parent);
                if (GameManager.instance.uiItemBox.box != null)
                    GameManager.instance.uiItemBox.box.BoxInv.Remove(GameManager.instance.uiItemBox.searchItem(droppedItem.item.GetComponent<Item>()));
            }
            GameManager.instance.playerInv.UIUpdate();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        UIItemComponent ClickedItem = eventData.pointerDrag.GetComponent<UIItemComponent>();
        if (ClickedItem == null) return;
        GameManager.instance.uiHand.transform.parent.gameObject.SetActive(true);
        GameManager.instance.uiMod.transform.parent.parent.gameObject.SetActive(false);
        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            /*if (OverMind.playerInv.uiVicini.gameObject.active)
                OverMind.playerInv.uiVicini.OnDrop(eventData);
            else
                OverMind.playerInv.uiItemBox.OnDrop(eventData);*/

            if (ClickedItem.item.GetComponent<Item>() is ItemGun ||
                ClickedItem.item.GetComponent<Item>() is ItemDynamite ||
                ClickedItem.item.GetComponent<Item>() is ItemMelee ||
                ClickedItem.item.GetComponent<Item>() is ItemFirstAidKit)
                GameManager.instance.uiHand.OnDrop(eventData);
            //손에 들었지만 동시에 가방에도 들어있음


        }
        else if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            GameManager.instance.uiMenu.transform.position = eventData.position;
            GameManager.instance.uiMenu.GetComponent<UIItemMenu>().itemInfo = ClickedItem.item.GetComponent<Item>();
            GameManager.instance.uiMenu.GetComponent<UIItemMenu>().scrapsNum.text = "(" + ClickedItem.item.GetComponent<Item>().scraps.ToString() + ")";
            GameManager.instance.uiMenu.gameObject.SetActive(true);
        }
        else
            GameManager.instance.uiMenu.gameObject.SetActive(false);

    }
}
