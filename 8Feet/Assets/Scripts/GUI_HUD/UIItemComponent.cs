﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class UIItemComponent : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    
    public GameObject item;
    public MonoBehaviour parent;
    private Transform originalParent;
    private Vector2 offset;
    public void Awake()
    {
        
    }
    public void Start()
    {
        
        if (item == null || item.GetComponent<Item>().itemName == "")
            transform.GetChild(0).GetComponent<Image>().CrossFadeAlpha(0, 0, true);
        else
            transform.GetChild(0).GetComponent<Image>().sprite = item.GetComponent<Item>().icon;

    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (item != null)
        {
            offset = eventData.position - new Vector2(this.transform.position.x, this.transform.position.y);
            originalParent = transform.parent;
            transform.SetParent(transform.parent.parent.parent);
            transform.position = eventData.position;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (item != null)
            transform.position = eventData.position - offset;
        GameManager.instance.uiMenu.gameObject.SetActive(false);
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(originalParent);
        transform.position = originalParent.transform.position;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        GameManager.instance.playerInv.UIUpdate();
    }
}
