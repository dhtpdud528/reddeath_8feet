﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UISpecialMod : MonoBehaviour, IDropHandler
{
    PlayerProfile profile;
    [SerializeField]UIModCapacity capacity;
    public Transform modlist;
    SoundManager soundManager;
    

    // Start is called before the first frame update
    void Awake()
    {
        profile = FindObjectOfType<PlayerProfile>();
        capacity = FindObjectOfType<UIModCapacity>();
        soundManager = FindObjectOfType<SoundManager>();
        

    }
    public void Start()
    {
    }
    public void UpdateUI()
    {
        for (int i = 0; i < modlist.childCount; i++)
        {
            UISpecialModComponent component = modlist.GetChild(i).GetComponent<UISpecialModComponent>();
            component.uiNameText.text = component.modName;
            component.uiCostText.text = component.cost.ToString();
            component.ui = this;
        }
    }
    public void OnDrop(PointerEventData eventData)
    {
        UISpecialModComponent droppedMod = eventData.pointerDrag.GetComponent<UISpecialModComponent>();
        if (droppedMod == null || !droppedMod.canUse) return;
        if (droppedMod.ui == this) { droppedMod.ResetPosition(); return; }
        if (gameObject.name == "Applied Mods - SR" &&
            !capacity.AddCosts(droppedMod.cost))
        {
            droppedMod.ResetPosition();
            return;
        }
        if(gameObject.name == "Mods - SR" &&
            droppedMod.ui != this && !capacity.AddCosts(-droppedMod.cost))
        {
            droppedMod.ResetPosition();
            return;
        }
        
        droppedMod.ui = this;
        soundManager.PlayWrittingSound();
        droppedMod.transform.SetParent(modlist);
    }
}
