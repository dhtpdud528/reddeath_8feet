﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIVicinity : MonoBehaviour, IDropHandler, IPointerClickHandler
{
    
    public List<GameObject> vicinityInv = new List<GameObject>();
    private int vicinityItemCount;
    [SerializeField] private GameObject iconItem;
    private Transform list;

    public int checkTime;
    // Start is called before the first frame update
    void Awake()
    {
        
        checkTime = 2;
        list = transform.parent.GetChild(1);
    }
    public void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        Collider[] hitColliders = Physics.OverlapSphere(GameManager.instance.playerInv.transform.position, 5, (1 << 9));
        if (vicinityItemCount != hitColliders.Length || checkTime < 2)
        {
            checkTime++;
            UpdateUIInfo(hitColliders);
        }
        vicinityInv = ConvertToGameObject_List(hitColliders);
    }
    private List<GameObject> ConvertToGameObject_List(Collider[] array)
    {
        List<GameObject> converts = new List<GameObject>();
        for(int i =0;i<array.Length;i++)
            converts.Add(array[i].gameObject);
        return converts;
    }
    public void UpdateUIInfo(Collider[] hitColliders)
    {
        vicinityItemCount = hitColliders.Length;

        int maxChildCount = list.childCount;
        for (int i = 0; i < maxChildCount; i++)
            GameObject.DestroyImmediate(list.GetChild(0).gameObject);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            if(hitColliders[i].GetComponent<Item>() == GameManager.instance.playerInv.GetCurrentItemInfo())
            {
                continue;
            }
            if (list.childCount == 0)
            {
                GameObject item = Instantiate(iconItem);
                item.GetComponent<UIItemComponent>().item = hitColliders[i].GetComponent<Item>().gameObject;
                item.transform.SetParent(list);
                item.GetComponent<UIItemComponent>().parent = list.GetComponent<UIVicinity>();
                item.transform.localScale = new Vector3(1, 1, 1);
                item.transform.GetChild(1).GetComponent<Text>().text = hitColliders[i].GetComponent<Item>().itemName;
                if (hitColliders[i].GetComponent<Item>().isModed)
                    item.transform.GetChild(1).GetComponent<Text>().text += " (Moded)";
                item.transform.GetChild(2).GetComponent<Text>().text = hitColliders[i].GetComponent<Item>().totalNum.ToString();
            }
            else
            {
                bool exist = false;
                for (int z = 0; z < list.childCount; z++)
                {

                    if (hitColliders[i].GetComponent<Item>().itemName == list.GetChild(z).GetChild(1).GetComponent<Text>().text)
                    {
                        if (GameManager.instance.playerInv.backpackInv[i].GetComponent<Item>().isModed)
                            continue;
                        if (hitColliders[i].GetComponent<Item>() is ItemAmmo)
                            list.GetChild(z).GetChild(2).GetComponent<Text>().text = (System.Convert.ToInt32(list.GetChild(z).GetChild(2).GetComponent<Text>().text) + hitColliders[i].GetComponent<ItemAmmo>().totalNum).ToString();
                        else
                            list.GetChild(z).GetChild(2).GetComponent<Text>().text = (System.Convert.ToInt32(list.GetChild(z).GetChild(2).GetComponent<Text>().text) + 1).ToString();
                        exist = true;
                    }
                }
                if (!exist)
                {
                    GameObject item = Instantiate(iconItem);
                    item.GetComponent<UIItemComponent>().item = hitColliders[i].GetComponent<Item>().gameObject;
                    item.transform.SetParent(list);
                    item.GetComponent<UIItemComponent>().parent = list.GetComponent<UIVicinity>();
                    item.transform.localScale = new Vector3(1, 1, 1);
                    item.transform.GetChild(1).GetComponent<Text>().text = hitColliders[i].GetComponent<Item>().itemName;
                    if (hitColliders[i].GetComponent<Item>().isModed)
                        item.transform.GetChild(1).GetComponent<Text>().text += " (Moded)";
                    item.transform.GetChild(2).GetComponent<Text>().text = hitColliders[i].GetComponent<Item>().totalNum.ToString();
                }
            }
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        UIItemComponent droppedItem = eventData.pointerDrag.GetComponent<UIItemComponent>();
        if (GameManager.instance.playerInv.SearchItem_inAllInv(droppedItem.item.GetComponent<Item>()) != null)
        {
            if(droppedItem.parent is UIHand)
                GameManager.instance.uiInv.OnDrop(eventData);
            if (Input.GetKey(KeyCode.LeftShift))
            {
                GameManager.instance.uiCount.gameObject.SetActive(true);
                GameManager.instance.uiCount.GetComponent<UICount>().splitItem = droppedItem.item;
                GameManager.instance.uiCount.from = GameManager.instance.playerInv.backpackInv;
                GameManager.instance.uiCount.toTypes = UICount.ToTypes.toVicini;
            }
            else
                GameManager.instance.playerInv.DropItem_fromBackpack(droppedItem.item.GetComponent<Item>());
            GameManager.instance.playerInv.UIUpdate();
        }

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.pointerDrag.GetComponent<UIItemComponent>() == null) return;
        UIItemComponent droppedItem = eventData.pointerDrag.GetComponent<UIItemComponent>();
        if (Input.GetKeyUp(KeyCode.Mouse1))
            GameManager.instance.uiInv.OnDrop(eventData);
    }
}
