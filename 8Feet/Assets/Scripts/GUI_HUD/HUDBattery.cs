﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDBattery : MonoBehaviour
{
    
    private Image barCurrentBattery;
    private Text hudLeftBattery;
    // Use this for initialization
    void Awake()
    {
        
        barCurrentBattery = GameObject.Find("BarCurrentBattery").GetComponent<Image>();
        hudLeftBattery = GameObject.Find("HUDLeftBattery").GetComponent<Text>();
    }
    public void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        if(GameManager.instance.flash.battery == null)
        {
            barCurrentBattery.fillAmount = 0;
            hudLeftBattery.text = "+" + 0;
            return;
        }
        if(barCurrentBattery != null)
        {
            barCurrentBattery.fillAmount 
                = GameManager.instance.flash.battery.leftPower / GameManager.instance.flash.battery.maxPower;
            int count = GameManager.instance.playerInv.CountItem_inBackpack(GameManager.instance.flash.battery.itemName);
            if (count > 0)
                hudLeftBattery.text = "+" + (count - 1);
            else
                hudLeftBattery.text = "+" + 0;
        }
        
    }
}
