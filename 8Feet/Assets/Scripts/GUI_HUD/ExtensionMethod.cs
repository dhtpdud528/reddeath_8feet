﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ExtensionMethod
{
    public static void CrossFadeAlphaWithCallBack(this Text img, float alpha, float duration)
    {
        MonoBehaviour mnbhvr = img.GetComponent<MonoBehaviour>();
        mnbhvr.StartCoroutine(CrossFadeAlphaCOR(img, alpha, duration));
    }

    public static IEnumerator CrossFadeAlphaCOR(Text img, float alpha, float duration)
    {
        Color currentColor = img.color;

        Color visibleColor = img.color;
        visibleColor.a = alpha;


        float counter = 0;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            img.color = Color.Lerp(currentColor, visibleColor, counter / duration);
            yield return null;
        }
    }
}
