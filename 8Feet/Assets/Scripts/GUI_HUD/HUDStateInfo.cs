﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDStateInfo : MonoBehaviour
{
    
    private Text HUDWeaponName;
    private Text HUDCurrentAmmo;
    private Text HUDLeftAmmo;
    private Text HUDLeftDynamite;
    private Text HUDLeftFirstAid;

    private Image IconDynamite;
    private Image IconLeftFirstAid;
    private Image IconBloody;

    private Image BarHealth;
    private Image BarMaxHealth;
    private Text HealthText;
    private RectTransform healthInfo;

    private float uiDisableTime = 0;
    private float damagedTime;
    [SerializeField]private float takenDamage;
    private Vector3 hudHealthOrigin;
    private float blueColor;

    // Use this for initialization
    void Awake()
    {
        
        IconBloody = GameObject.Find("Bloody").GetComponent<Image>();
        healthInfo = GameObject.Find("HealthInfo").GetComponent<RectTransform>();
        hudHealthOrigin = healthInfo.localPosition;
        HUDWeaponName = GameObject.Find("HUDWeaponName").GetComponent<Text>();
        HUDCurrentAmmo = GameObject.Find("HUDCurrentAmmo").GetComponent<Text>();
        HUDLeftAmmo = GameObject.Find("HUDLeftAmmo").GetComponent<Text>();
        HUDLeftDynamite = GameObject.Find("HUDDynamite").GetComponent<Text>();
        HUDLeftFirstAid = GameObject.Find("HUDFirstAid").GetComponent<Text>();
        IconDynamite = GameObject.Find("IconDynamite").GetComponent<Image>();
        IconLeftFirstAid = GameObject.Find("IconFirstAid").GetComponent<Image>();
        BarHealth = GameObject.Find("BarHealth").GetComponent<Image>();
        BarMaxHealth = GameObject.Find("BarMaxHealth").GetComponent<Image>();
        HealthText = GameObject.Find("HealthText").GetComponent<Text>();
    }
    public int GetCurrentAmmo()
    {
        if(HUDCurrentAmmo.text != "")
            return System.Convert.ToInt32(HUDCurrentAmmo.text);
        return 1;
    }
    private void FixedUpdate()
    {
        BarHealth.fillAmount = (float)GameManager.instance.playerState.GetHealth() / GameManager.instance.playerState.maxHeath;
        HealthText.text = GameManager.instance.playerState.GetHealth()+ "/"+GameManager.instance.playerState.maxHeath;
        blueColor = GameManager.instance.playerState.breathTime / 20 * 255;
        

        if (damagedTime > 0)
        {
            BarHealth.color = new Color(255, 0, 0);
            damagedTime -= Time.deltaTime;
            healthInfo.position =
                new Vector3(
                healthInfo.position.x + Random.Range(-takenDamage, takenDamage),
                healthInfo.position.y + Random.Range(-takenDamage, takenDamage),
                healthInfo.position.z + Random.Range(-takenDamage, takenDamage));
            
        }
        else if (!GameManager.instance.playerState.bloody)
            BarHealth.color = new Color(255, 255, 255);
        takenDamage /= 2;

        healthInfo.localPosition = Vector3.Lerp(healthInfo.localPosition, hudHealthOrigin, 5 * Time.deltaTime);
        if (GameManager.instance.playerState.bloody)
        {
            BarHealth.color = Color.Lerp(Color.white, Color.red, Mathf.PingPong(Time.time, 1));
            IconBloody.enabled = true;
        }
        else
            IconBloody.enabled = false;

        if (GameManager.instance.playerState.down)
            BarMaxHealth.color = new Color(255, 0, 0);
        else
            BarMaxHealth.color = new Color(0, 0, 0);
        //위에 부분 나중에 피드백받아서 고칠예정 의도적 불편함
        if (uiDisableTime < 0)
        {
            HUDWeaponName.CrossFadeAlpha(0, 1f, true);
            HUDCurrentAmmo.CrossFadeAlpha(0, 1f, true);
            HUDLeftAmmo.CrossFadeAlpha(0, 1f, true);
            HUDLeftDynamite.CrossFadeAlpha(0, 1f, true);
            HUDLeftFirstAid.CrossFadeAlpha(0, 1f, true);
            IconDynamite.CrossFadeAlpha(0, 1f, true);
            IconLeftFirstAid.CrossFadeAlpha(0, 1f, true);
        }
        else
        {
            uiDisableTime -= Time.deltaTime;
            HUDWeaponName.CrossFadeAlpha(100, 0f, true);
            HUDCurrentAmmo.CrossFadeAlpha(100, 0f, true);
            HUDLeftAmmo.CrossFadeAlpha(100, 0f, true);
            HUDLeftDynamite.CrossFadeAlpha(100, 0f, true);
            HUDLeftFirstAid.CrossFadeAlpha(100, 0f, true);
            IconDynamite.CrossFadeAlpha(100, 0f, true);
            IconLeftFirstAid.CrossFadeAlpha(100, 0f, true);
        }
        if (HUDCurrentAmmo.text == "0")
            HUDCurrentAmmo.color = new Color(255, 0, 0);
        else
            HUDCurrentAmmo.color = new Color(255, 255, 255);

        if (HUDLeftAmmo.text == "0")
            HUDLeftAmmo.color = new Color(255, 0, 0);
        else
            HUDLeftAmmo.color = new Color(255, 255, 255);

        if (HUDLeftDynamite.text == "0")
            HUDLeftDynamite.color = new Color(255, 0, 0);
        else
            HUDLeftDynamite.color = new Color(255, 255, 255);

        if (HUDLeftFirstAid.text == "0")
            HUDLeftFirstAid.color = new Color(255, 0, 0);
        else
            HUDLeftFirstAid.color = new Color(255, 255, 255);
        if(GameManager.instance.playerState.isUnderWater)
            BarHealth.color = new Color(50 / blueColor, 50 / blueColor, blueColor);

    }
    // Update is called once per frame
    public void UpdateHUDInfo()
    {
        uiDisableTime = 0;
        if (GameManager.instance.playerInv != null)
        {
            HUDLeftDynamite.text = GameManager.instance.playerInv.CountItem_inBackpack("Dynamite").ToString();
            HUDLeftFirstAid.text = GameManager.instance.playerInv.CountItem_inBackpack("FirstAidKit").ToString();
            if (GameManager.instance.playerInv.GetCurrentItemInfo().itemName == "")
            {
                HUDWeaponName.text = "Empty";
                HUDCurrentAmmo.text = "";
                HUDLeftAmmo.text = "";
            }
            else if (GameManager.instance.playerInv.GetCurrentItemInfo() is ItemGun)
            {
                HUDWeaponName.text = GameManager.instance.playerInv.GetCurrentItemInfo().itemName;
                HUDCurrentAmmo.text = ((ItemGun)GameManager.instance.playerInv.GetCurrentItemInfo()).currentBlullets.ToString();
                GameObject ammo = GameManager.instance.playerInv.SearchAmmo(((ItemGun)GameManager.instance.playerInv.GetCurrentItemInfo()).ammoType);
                if (ammo != null)
                    HUDLeftAmmo.text = ammo.GetComponent<ItemAmmo>().totalNum.ToString();
                else
                    HUDLeftAmmo.text = "0";


            }
            else
            {
                int count = GameManager.instance.playerInv.CountItem_inBackpack(GameManager.instance.playerInv.GetCurrentItemInfo().itemName);
                HUDWeaponName.text = GameManager.instance.playerInv.GetCurrentItemInfo().itemName;
                HUDCurrentAmmo.text = count.ToString();
                HUDLeftAmmo.text = "";
            }
        }
    }
    public void TakeDamage_HUD(float damage)
    {
        damagedTime = 0.2f;
        takenDamage += damage / 4;
        UpdateHUDInfo();
    }
    public void activeHUD()
    {
        HUDWeaponName.CrossFadeAlpha(100, 0f, true);
        HUDCurrentAmmo.CrossFadeAlpha(100, 0f, true);
        HUDLeftAmmo.CrossFadeAlpha(100, 0f, true);
        HUDLeftDynamite.CrossFadeAlpha(100, 0f, true);
        HUDLeftFirstAid.CrossFadeAlpha(100, 0f, true);
        IconDynamite.CrossFadeAlpha(100, 0f, true);
        IconLeftFirstAid.CrossFadeAlpha(100, 0f, true);
    }
}
