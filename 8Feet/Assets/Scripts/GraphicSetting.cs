﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphicSetting : MonoBehaviour
{
    public void OptionOnOff()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
    public void VeryLow()
    {
        QualitySettings.SetQualityLevel(0);
    }
    public void Low()
    {
        QualitySettings.SetQualityLevel(1);
    }
    public void Medium()
    {
        QualitySettings.SetQualityLevel(2);
    }
    public void High()
    {
        QualitySettings.SetQualityLevel(3);
    }
    public void VeryHigh()
    {
        QualitySettings.SetQualityLevel(4);
    }
    public void Ultra()
    {
        QualitySettings.SetQualityLevel(5);
    }

}
