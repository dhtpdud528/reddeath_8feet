﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTimeSkill : MonoBehaviour
{
    
    public bool bulletTime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            bulletTime = !bulletTime;
            if (bulletTime)
                StartCoroutine(BulletTimeEvent());
            else
                EndEvent();
        }
    }
    private IEnumerator BulletTimeEvent()
    {
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        while (Time.timeScale > 0.05f)
        {
            for (int i = 0; i < audioSources.Length; i++)
                if (audioSources[i] != null && audioSources[i] != GameManager.instance.playerState.chaseAudioSources && audioSources[i].pitch > 0.1f )
                    audioSources[i].pitch *= Time.timeScale;
            Time.timeScale -= 0.05f;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            yield return new WaitForSecondsRealtime(0.05f);
        }


    }
    private void EndEvent()
    {
        if (Time.timeScale != 1)
        {
            AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
            for (int i = 0; i < audioSources.Length; i++)
                audioSources[i].pitch = 1;
        }
        Time.fixedDeltaTime = 0.02f;
        Time.timeScale = 1f;
        StopAllCoroutines();
    }
}
