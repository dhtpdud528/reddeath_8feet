﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveEvent : MonoBehaviour
{
    private bool start;
    public GameObject[] gameObjects;
    public float[] times;
    public bool doActive;
    public bool isTriggerEvent;
    public GameObject collitionObj;
    // Start is called before the first frame update
    void Start()
    {
        start = false;
        if (!isTriggerEvent)
            StartCoroutine(ActiveObjs());
    }

    IEnumerator ActiveObjs()
    {
        start = true;
        for (int i = 0; i < gameObjects.Length; i++)
        {
            gameObjects[i].SetActive(doActive);
            yield return new WaitForSecondsRealtime(times[i]);
        }
        StopAllCoroutines();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!start && isTriggerEvent && other.gameObject.Equals(collitionObj))
            StartCoroutine(ActiveObjs());
    }
}
