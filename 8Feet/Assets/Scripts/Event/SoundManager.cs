﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip[] paperSounds;
    public AudioClip[] writtingSounds;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void PlayPaperSound()
    {
        audioSource.PlayOneShot(paperSounds[Random.Range(0,paperSounds.Length)]);
    }
    public void PlayWrittingSound()
    {
        audioSource.PlayOneShot(writtingSounds[Random.Range(0, writtingSounds.Length)]);
    }
        // Update is called once per frame
        void Update()
    {
        
    }
}
