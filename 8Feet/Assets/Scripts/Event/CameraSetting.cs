﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSetting : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private float far;
    // Start is called before the first frame update
    void Start()
    {
        camera = FindObjectOfType<EZCameraShake.CameraShaker>().GetComponent<Camera>();
        camera.farClipPlane = far;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
