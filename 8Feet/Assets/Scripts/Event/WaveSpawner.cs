﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaveSpawner : MonoBehaviour
{
    public enum SpawnState { SPAWNING, WAITING, COUNTING, END }
    [System.Serializable]
    public class Wave
    {
        public string name;
        public GameObject[] spawnObjects;
        public int spawnCount;
        public float rate;
        public float timeBetweenWaves = 10;
    }

    public Wave[] waves;
    int nextWave = 0;
    float waveCountdown;
    public SpawnState state = SpawnState.COUNTING;
    public bool spawnDead;
    Vector3 spawnPos;
    public Transform toPos;
    private bool spawnStart;
    private void Start()
    {
        spawnStart = false;
        waveCountdown = waves[0].timeBetweenWaves;
    }
    // Start is called before the first frame update
    void Update()
    {
        spawnPos = transform.position;

        if (state == SpawnState.WAITING)
            WaveComplete();
        if (state == SpawnState.END) return;
        if (waveCountdown <= 0)
        {
            if (state != SpawnState.SPAWNING)
                StartCoroutine(SpawnWave(waves[nextWave]));
        }
        else
        {
            waveCountdown -= Time.deltaTime;
        }
    }
    private void WaveComplete()
    {
        state = SpawnState.COUNTING;

        if (nextWave + 1 > waves.Length - 1)
        {
            state = SpawnState.END;
            return;
        }
        nextWave++;
        waveCountdown = waves[nextWave].timeBetweenWaves;
    }

    IEnumerator SpawnWave(Wave wave)
    {
        state = SpawnState.SPAWNING;
        for (int i = 0; i < wave.spawnCount; i++)
        {
            Spawn(wave.spawnObjects[Random.Range(0, wave.spawnObjects.Length)]);
            yield return new WaitForSeconds(wave.rate);
        }
        state = SpawnState.WAITING;

        yield break;
    }

    private void Spawn(GameObject target)
    {
        RaycastHit hit;
        if (Physics.Raycast(new Vector3(spawnPos.x, spawnPos.y + 300, spawnPos.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14)))
            spawnPos.y = hit.point.y;
        GameObject spawnedObj = Instantiate(target, spawnPos, new Quaternion(0, 0, 0, 0));
        if (spawnedObj.GetComponent<AI_Zombie>() != null)
        {
            if (toPos != null)
            {
                spawnedObj.GetComponent<NavMeshAgent>().SetDestination(toPos.position);
                spawnedObj.GetComponent<AI_Zombie>().destinationPos = toPos.position;
            }
            if (spawnDead && spawnedObj.GetComponent<AI_Zombie>().health > 0)
                spawnedObj.GetComponent<AI_Zombie>().takeDamage(999, HitPoint.HitParts.Head, false);
        }
        else if (spawnedObj.GetComponent<Item>() != null)
        {
            spawnedObj.transform.position = new Vector3(spawnedObj.transform.position.x, spawnedObj.transform.position.y + 0.5f, spawnedObj.transform.position.z);
            spawnedObj.GetComponent<Item>().DropItem();
            spawnedObj.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-.1f, .1f), 1f, Random.Range(-.1f, .1f) * spawnedObj.GetComponent<Rigidbody>().mass), ForceMode.Impulse);
        }
    }
    public void setSpawnDead(bool spawndead)
    {
        spawnDead = spawndead;
    }
    private void OnDisable()
    {
        StopAllCoroutines();
        spawnStart = false;
    }
}
