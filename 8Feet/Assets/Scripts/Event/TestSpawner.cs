﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TestSpawner : MonoBehaviour
{
    public GameObject[] objects;
    public float spawnLate;
    public int maxSpawn;
    [SerializeField] private int num;
    public bool spawnDead;
    Vector3 spawnPos;
    public Transform toPos;
    private bool spawnStart;
    private void Start()
    {
        spawnStart = false;
        if (spawnLate < 0.0001f)
            spawnLate = 0.0001f;
    }
    // Start is called before the first frame update
    void Update()
    {
        spawnPos = transform.position;
        if (!spawnStart)
            StartCoroutine(StartSpawn());
        spawnStart = true;
        if (num >= maxSpawn+1)
            StopAllCoroutines();
    }
    public void setSpawnDead(bool spawndead)
    {
        spawnDead = spawndead;
    }
    public void setInterval(string val)
    {
        spawnLate = float.Parse(val);
    }
    public void setCount(string val)
    {
        maxSpawn = int.Parse(val);
    }
    private IEnumerator StartSpawn()
    {
        RaycastHit hit;

        while (true)
        {
            num++;
            if (Physics.Raycast(new Vector3(spawnPos.x, spawnPos.y + 300, spawnPos.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14)))
                spawnPos.y = hit.point.y;
            yield return new WaitForSecondsRealtime(spawnLate);

            GameObject spawnedObj = Instantiate(objects[Random.Range(0, objects.Length)], spawnPos, new Quaternion(0, 0, 0, 0));
            if (spawnedObj.GetComponent<AI_Zombie>() != null)
            {
                if(toPos != null)
                {
                    spawnedObj.GetComponent<NavMeshAgent>().SetDestination(toPos.position);
                    spawnedObj.GetComponent<AI_Zombie>().destinationPos = toPos.position;
                }
                if (spawnDead && spawnedObj.GetComponent<AI_Zombie>().health > 0)
                    spawnedObj.GetComponent<AI_Zombie>().takeDamage(999, HitPoint.HitParts.Head, false);
            }
            else if(spawnedObj.GetComponent<Item>() != null)
            {
                spawnedObj.transform.position = new Vector3(spawnedObj.transform.position.x, spawnedObj.transform.position.y + 0.5f, spawnedObj.transform.position.z);
                spawnedObj.GetComponent<Item>().DropItem();
                spawnedObj.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-.1f,.1f),1f, Random.Range(-.1f, .1f) * spawnedObj.GetComponent<Rigidbody>().mass), ForceMode.Impulse);
            }
        }
    }
    private void OnDisable()
    {
        StopAllCoroutines();
        num = 0;
        spawnStart = false;
    }
}
