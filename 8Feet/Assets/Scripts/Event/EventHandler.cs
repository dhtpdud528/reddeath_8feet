﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventHandler : MonoBehaviour
{
    public GameObject[] events;
    private bool eventStarted = false;
    void Start()
    {
        if (events.Length > 0)
            StartCoroutine(PlayEventList());
    }
    IEnumerator PlayEventList()
    {
        for (int i = 0; i < events.Length; i++)
        {
            GameObject obj = Instantiate(events[i]);
            obj.GetComponent<GameEvent>().Play();
            yield return new WaitUntil(() => obj == null);
        }
    }
}
