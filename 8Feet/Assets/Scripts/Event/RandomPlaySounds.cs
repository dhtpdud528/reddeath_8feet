﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPlaySounds : MonoBehaviour
{
    public static void PlayRandomAudio(AudioSource m_AudioSource, AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, audios.Length);
        m_AudioSource.clip = audios[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = m_AudioSource.clip;
    }
}
