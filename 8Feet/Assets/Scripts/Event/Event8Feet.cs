﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event8Feet : MonoBehaviour
{
    
    public bool runAndDisappear;
    public Transform SpawnPoint;
    public bool spawnbutton;
    private float time;

    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
        if (runAndDisappear)
        {
            GameManager.instance.ai_8Feet.ai_state = AI_8Feet.STATE.Run;
            GameManager.instance.ai_8Feet.currentRunStack = 1;
        }
        if (SpawnPoint != null)
            spawnset();

    }
    void spawnset()
    {
        GameManager.instance.ai_8Feet.gameObject.SetActive(true);
        GameManager.instance.ai_8Feet.agent.enabled = false;
        Vector3 pos = transform.position;
        RaycastHit hit;
        if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y + 300, transform.position.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14)))
            pos.y = hit.point.y;
        GameManager.instance.ai_8Feet.transform.position = pos;
        GameManager.instance.ai_8Feet.ai_state = AI_8Feet.STATE.Rush;
        GameManager.instance.ai_8Feet.currentRunStack = int.MaxValue;
        GameManager.instance.ai_8Feet.agent.enabled = true;
    }
}
