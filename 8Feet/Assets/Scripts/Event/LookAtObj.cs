﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtObj : MonoBehaviour
{
    public Transform target;
    public bool onlyStart;

    private void Start()
    {
        transform.LookAt(target);
    }
    void Update()
    {
        if (!onlyStart)
            transform.LookAt(target);
    }
}
