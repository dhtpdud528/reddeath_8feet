﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicOnOff : MonoBehaviour
{
    public AudioSource spicker;
    public float originVol;
    // Start is called before the first frame update
    void Start()
    {
        originVol = spicker.volume;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit = new RaycastHit();
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray.origin, ray.direction, out hit) && hit.collider.gameObject == gameObject)
            {
                if (spicker.volume >= originVol)
                    spicker.volume = 0;
                else
                    spicker.volume = originVol;
            }
        }
    }
}
