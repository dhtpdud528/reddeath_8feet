﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEvent : MonoBehaviour
{
    private Animator allOfPlayerAnim;
    [SerializeField] private string playerStateName;
    [SerializeField] private float timeForAutoEndEvent;
    [SerializeField] private bool onlyDialogue;
    private bool endDialogue;
    private Text playerDialogue;
    private Dialogue[] dialogue;
    [SerializeField] private bool slowMotion;
    [SerializeField] private KeyCode key;
    [SerializeField] private bool disableUI;
    private bool enabledDialogue;
    private int enabledDialogueTick;
    [SerializeField] private bool cantMove;
    [SerializeField] private bool cantWeapon;

    private void Awake()
    {
        
        allOfPlayerAnim = GameObject.Find("AllOfPlayer").GetComponent<Animator>();
        playerDialogue = GameObject.Find("PlayerDialogue").GetComponent<Text>();
        dialogue = GetComponents<Dialogue>();
    }
    private void Start()
    {
        playerDialogue.CrossFadeAlpha(0, 0, false);
    }
    private void FixedUpdate()
    {
        GameManager.instance.playerController.cantMove = cantMove;
        GameManager.instance.weaponController.isCinematic = cantWeapon;
        if (enabledDialogue)
        {
            enabledDialogueTick = 0;
            playerDialogue.CrossFadeAlpha(0, 0.01f,true);
            playerDialogue.CrossFadeAlpha(1, 0.01f, true);
            // Debug.Log("킴");
        }
        else
        {
            playerDialogue.CrossFadeAlpha(0,1f,true);
            playerDialogue.CrossFadeAlpha(0, 1f, true);
            //Debug.Log("끔");
        }
    }
    public void Update()
    {
        if (timeForAutoEndEvent > 0)
            StartCoroutine(WaitSecondsAndEndEvent(timeForAutoEndEvent));
        if (key != KeyCode.None && Input.GetKey(key))
            EndEvent();
        else if (onlyDialogue && endDialogue)
            EndEvent();
    }
    public void Play()
    {
        if (disableUI)
        {
            GameManager.instance.weaponController.uiItems.SetActive(false);
            GameManager.instance.uiVicini.transform.parent.parent.gameObject.SetActive(true);
            GameManager.instance.uiVicini.transform.parent.parent.GetChild(0).GetComponent<Text>().enabled = true;
            GameManager.instance.uiItemBox.transform.parent.parent.gameObject.SetActive(false);
            GameManager.instance.uiItemBox.transform.parent.parent.GetChild(0).GetComponent<Text>().enabled = false;
        }
        if (playerStateName != "")
            allOfPlayerAnim.CrossFadeInFixedTime(playerStateName, 0.001f);
        if (slowMotion)
            StartCoroutine(BulletTimeEvent());
        StartCoroutine(DialogueEvent());

    }
    private IEnumerator BulletTimeEvent()
    {
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        while (Time.timeScale > 0.05f)
        {
            for (int i = 0; i < audioSources.Length; i++)
                if(audioSources[i]!=null && audioSources[i] != GameManager.instance.playerState.chaseAudioSources)
                audioSources[i].pitch *= Time.timeScale;
            Time.timeScale -= 0.01f;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            yield return new WaitForSecondsRealtime(0.05f);
        }
    }
    private IEnumerator DialogueEvent()
    {
        playerDialogue.enabled = true;
        for (int i = 0; i < dialogue.Length; i++)
        {
            enabledDialogue = true;
            playerDialogue.text = dialogue[i].dialogue;
            yield return new WaitForSecondsRealtime(dialogue[i].dialogueTime-0.9f);
            enabledDialogue = false;
            yield return new WaitForSecondsRealtime(1);
        }
        playerDialogue.enabled = false;
        endDialogue = true;
    }
    private void EndEvent()
    {
        playerDialogue.enabled = false;
        if(Time.timeScale != 1)
        {
            AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
            for(int i =0; i < audioSources.Length; i++)
                audioSources[i].pitch = 1;
        }
        Time.fixedDeltaTime = 0.02f;
        Time.timeScale = 1f;
        StopAllCoroutines();
        DestroyImmediate(gameObject);
        allOfPlayerAnim.CrossFadeInFixedTime("Normal", 0.001f);
    }
    IEnumerator WaitSecondsAndEndEvent(float time)
    {
        yield return new WaitForSecondsRealtime(time);
        EndEvent();
    }
}
