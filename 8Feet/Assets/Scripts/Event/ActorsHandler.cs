﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorsHandler : MonoBehaviour
{
    public SoldierController[] actors;
    public bool fire;
    // Start is called before the first frame update
    void Start()
    {
        foreach (SoldierController s in actors)
            s.fire = fire;
    }
}
