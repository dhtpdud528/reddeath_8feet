﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIHitBox : MonoBehaviour, IDropHandler
{
    public MonoBehaviour target;
    public void OnDrop(PointerEventData eventData)
    {
        ((IDropHandler)target).OnDrop(eventData);
    }
}
