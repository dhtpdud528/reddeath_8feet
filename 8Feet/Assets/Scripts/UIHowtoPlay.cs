﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHowtoPlay : MonoBehaviour
{
    
    public GameObject buttons;
    public GameObject[] tuts;
    public int index;
    
    // Start is called before the first frame update
    private void Awake()
    {
        
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (buttons.active && Input.GetKeyDown(KeyCode.Escape))
            CloseHTP();
    }
    public void OpenHTP()
    {
        index = 0;
        tuts[index].SetActive(true);
        GameManager.instance.escMenu.gameObject.SetActive(false);

        GameManager.instance.playerController.m_MouseLook.SetCursorLock(false);
        GameManager.instance.playerController.m_MouseLook.XSensitivity = 0;
        GameManager.instance.playerController.m_MouseLook.YSensitivity = 0;
    }
    public void CloseHTP()
    {
        foreach (GameObject ui in tuts)
            ui.SetActive(false);
        buttons.SetActive(false);

        GameManager.instance.escMenu.gameObject.SetActive(true);
    }
    public void NextHTP()
    {
        tuts[index].SetActive(false);
        index++;
        checkIndex();
        tuts[index].SetActive(true);
    }
    public void BreforeHTP()
    {
        tuts[index].SetActive(false);
        index--;
        checkIndex();
        tuts[index].SetActive(true);
    }
    public void checkIndex()
    {
        if (index < 0)
            index = tuts.Length - 1;
        if (index > (tuts.Length - 1))
            index = 0;
    }
}
