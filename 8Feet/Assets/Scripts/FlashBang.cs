﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class FlashBang : MonoBehaviour
{
    public RawImage image;
    // Start is called before the first frame update
    private void Awake()
    {
        image = GetComponent<RawImage>();
        image.enabled = false;
    }
    public void DoFlashBang(Texture texture)
    {
        StartCoroutine(FlashBangCoroutine(texture));
    }

    IEnumerator FlashBangCoroutine(Texture texture)
    {
        image.texture = texture;
        image.enabled = true;
        yield return new WaitForSeconds(1);
        image.DOFade(0, 4);
        yield return new WaitForSeconds(4);
        image.enabled = false;
        image.DOFade(1, 0);

    }
}
