﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityFlare : Entity
{
    Rigidbody rigidbody;
    AudioSource audioSource;
    public AudioClip boomSound;
    public float Impulse;
    Light light;
    private float initIntensity;
    public float maxlifeTime;
    private float lifeTime;
    // Start is called before the first frame update
    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        lifeTime = maxlifeTime;
        light = GetComponent<Light>();
        initIntensity = light.intensity;
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.AddForce(new Vector3(Random.Range(-1f,1f), 5, Random.Range(-1f, 1f)) * Impulse, ForceMode.Impulse);
        StartCoroutine(CheckBoom());
    }
    IEnumerator CheckBoom()
    {
        yield return new WaitUntil(() => rigidbody.velocity.y < 0);
        while (rigidbody.velocity.y > 0)
            yield return new WaitForSecondsRealtime(0.01f);
        light.enabled = true;
        rigidbody.drag = 5;
        audioSource.PlayOneShot(boomSound);
        while(lifeTime > 0)
        {
            light.intensity = initIntensity * (lifeTime / maxlifeTime) * Random.Range(0.8f, 1);
            lifeTime -= Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(gameObject);
    }
}
