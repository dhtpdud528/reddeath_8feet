﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorLook : MonoBehaviour
{
    private Animator anim;
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnAnimatorIK(int layerIndex)
    {
        anim.SetLookAtWeight(1);
        anim.SetLookAtPosition(target.position);
    }
}
