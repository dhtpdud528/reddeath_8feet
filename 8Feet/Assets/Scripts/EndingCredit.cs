﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class EndingCredit : MonoBehaviour
{
    Text text;
    public Text text2;
    public bool nextText;
    // Start is called before the first frame update
    void Awake()
    {
        text = GetComponent<Text>();
        StartCoroutine(Ending());
        StartCoroutine(Ending2());
    }

    IEnumerator Ending()
    {
        yield return new WaitForSeconds(1);

        text.DOText("Programming: ", 1);

        yield return new WaitForSeconds(5);
        yield return new WaitForSeconds(1);

        text.DOText("Pictures: ", 1);

        yield return new WaitForSeconds(4);
        yield return new WaitForSeconds(1);

        text.DOText("3D Models: ", 0.5f);

        yield return new WaitForSeconds(4 - 0.5f);
        yield return new WaitForSeconds(1);

        text.DOText("Map Design: ", 0.5f);

        yield return new WaitForSeconds(4 - 0.5f);
        yield return new WaitForSeconds(1);

        text.DOText("Director: ", 1);
    }
    IEnumerator Ending2()
    {
        yield return new WaitForSeconds(30);
        text2.DOText("Sounds", 1);

        yield return new WaitForSeconds(4);
        yield return new WaitForSeconds(1);

        text2.DOText("Models & Animations", 1);
    }
}
