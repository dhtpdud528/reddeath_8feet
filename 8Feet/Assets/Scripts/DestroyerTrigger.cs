﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyerTrigger : MonoBehaviour
{
    public GameObject gameObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void DoDestroy()
    {
        Destroy(gameObject);
    }
}
