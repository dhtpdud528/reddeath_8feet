﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroSoldierEvent : MonoBehaviour
{
    public SoldierController[] soldiers;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<SoldierController>() != null)
            for (int i = 0; i < soldiers.Length; i++)
            {
                soldiers[i].vertical = 0;
                soldiers[i].horizontal = 0;
                soldiers[i].crouch = true;
            }
    }
}
