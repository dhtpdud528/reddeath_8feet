﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProfile : MonoBehaviour {
    public float maxStemina;
    public float maxHealth;
    public float moveSpeed;
    public float reloadSpeed;
    public float itemUsageSpeed;
    public enum MOS {Rifle=1, SMG=2, Medic=3, Test = 4, Empty = 5 };
    public MOS mos;
    public bool manualDexterity;
}
