﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModsProfile : MonoBehaviour
{
    public bool immunityConfusion;
    public bool immunityBleeding;
    public bool manualDexterity;
    public float maxStemina;
    public float maxHealth;
    public float recoverySteminaSpeed;
    public float moveSpeed;
    public float reloadSpeed;
    public float meleeDamage;
    public float noiseLimit;
    public float itemModCosts;

    public bool immunityPushed;
    public float itemDistribution;
    public float zombieHealth;
    public float zombierMoveSpeed;
    public float zombieDamage;
    public bool withstand;
    public bool compass;

    public void ResetMods()
    {
        immunityConfusion = false;
        immunityBleeding = false;
        manualDexterity = false;
        maxStemina = 1;
        maxHealth = 1;
        recoverySteminaSpeed = 1;
        moveSpeed = 1;
        reloadSpeed = 1;
        meleeDamage = 1;
        noiseLimit = 1;
        itemModCosts = 1;

        immunityPushed = false;
        itemDistribution = 1;
        zombieHealth = 1;
        zombierMoveSpeed = 1;
        zombieDamage = 1;
        withstand = false;
        compass = false;
    }
}
