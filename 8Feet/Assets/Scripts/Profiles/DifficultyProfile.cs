﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyProfile : MonoBehaviour {
    public float itemDistribution;
    public float enermyHealth;
    public float enermyDamage;
    public float noiseLimit;
    public float speedRush;
    public float speedPain;
}
