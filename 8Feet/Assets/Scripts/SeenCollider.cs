﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeenCollider : MonoBehaviour
{
    
    private void Start()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.instance.flash.on && other.gameObject.name == "FlashHit")
        {
            GameManager.instance.ai_8Feet.beSeen = true;
            if (GameManager.instance.ai_8Feet.ai_state == AI_8Feet.STATE.Stalk)
            {
                //player.audioSource.PlayOneShot(warningSounds[Random.RandomRange(0, warningSounds.Length)]);
                GameManager.instance.playerState.chaseAudioSources.volume = 1;
                GameManager.instance.playerState.chaseAudioSources.Play();
                if (Vector3.Distance(transform.position, GameManager.instance.playerState.transform.position) > GameManager.instance.ai_8Feet.rushRange)
                    GameManager.instance.ai_8Feet.ai_state = AI_8Feet.STATE.Run;
                else
                    GameManager.instance.ai_8Feet.ai_state = AI_8Feet.STATE.Rush;
                GameManager.instance.ai_8Feet.anim.CrossFadeInFixedTime("StandUP", 0);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "FlashHit")
            GameManager.instance.ai_8Feet.beSeen = false;
    }
}
