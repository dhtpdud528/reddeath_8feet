﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProfileDestroyer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Destroy(FindObjectOfType<DontDestroyOnOtherScene>().gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
