﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstBlood : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SteamAchievements.UnlockAchievement("ACHIEVEMENT_Dead");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
