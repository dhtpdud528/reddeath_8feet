﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckAchievement : MonoBehaviour
{
    public GameObject[] modsList;
    // Start is called before the first frame update
    private void Awake()
    {
        for (int i = 0; i < modsList.Length; i++)
            Instantiate(modsList[i], transform);
    }
    void Start()
    {
        FindObjectOfType<UISpecialMod>().UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
