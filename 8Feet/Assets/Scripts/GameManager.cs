﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public PlayerInv playerInv;
    public PlayerState playerState;
    public playerRigidbody playerRigidbody;
    public PlayerProfile playerProfile;
    public WeaponController weaponController;
    public ModsProfile modsProfile;
    public FirstPersonController playerController;
    public DifficultyProfile difficultyProfile;
    public ItemList itemPactory;
    public HUDProgressBar hudProgressBar;
    public Spawner spawner;
    public Flash flash;
    public AI_8Feet ai_8Feet;
    public UIHowtoPlay uiHowtoPlay;
    public GraphicSetting graphicSetting;
    public OptionProfile optionProfile;
    public DontDestroyOnOtherScene dontDestroyOnOtherScene;
    public UICount uiCount;
    public HUDInvInfo hudInvInfo;
    public HUDStateInfo hudStateInfo;
    public UIItemMenu uiMenu;
    public UIInventory uiInv;
    public UIVicinity uiVicini;
    public UIBox uiItemBox;
    public UIHand uiHand;
    public UIMod uiMod;
    public HUDInvInfo hudQuickSlotsInfo;
    public PlayerAnimController playerAnimCon;
    public GameObject uiCompass;
    public GameObject hand;
    public FlashBang flashBang;
    public ESCMenuController escMenu;
    public Text scrapsCount;
    public List<AI_Zombie> zombiesInWorld;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 게임 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        ai_8Feet = FindObjectOfType<AI_8Feet>();
        playerInv = FindObjectOfType<PlayerInv>();
        playerState = FindObjectOfType<PlayerState>();
        playerRigidbody = FindObjectOfType<playerRigidbody>();
        playerProfile = FindObjectOfType<PlayerProfile>();
        weaponController = FindObjectOfType<WeaponController>();
        modsProfile = FindObjectOfType<ModsProfile>();
        playerController = FindObjectOfType<FirstPersonController>();
        difficultyProfile = FindObjectOfType<DifficultyProfile>();
        itemPactory = FindObjectOfType<ItemList>();
        hudProgressBar = FindObjectOfType<HUDProgressBar>();
        spawner = FindObjectOfType<Spawner>();
        flash = FindObjectOfType<Flash>();
        optionProfile = FindObjectOfType<OptionProfile>();
        uiHowtoPlay = FindObjectOfType<UIHowtoPlay>();
        graphicSetting = FindObjectOfType<GraphicSetting>();
        dontDestroyOnOtherScene = FindObjectOfType<DontDestroyOnOtherScene>();
        uiCount = FindObjectOfType<UICount>();
        hudInvInfo = FindObjectOfType<HUDInvInfo>();
        hudStateInfo = FindObjectOfType<HUDStateInfo>();
        uiMenu = FindObjectOfType<UIItemMenu>();
        uiInv = FindObjectOfType<UIInventory>();
        uiVicini = FindObjectOfType<UIVicinity>();
        uiItemBox = FindObjectOfType<UIBox>();
        uiHand = FindObjectOfType<UIHand>();
        uiMod = FindObjectOfType<UIMod>();
        hudQuickSlotsInfo = FindObjectOfType<HUDInvInfo>();
        playerAnimCon = FindObjectOfType<PlayerAnimController>();
        uiCompass = GameObject.Find("UICompass");
        scrapsCount = GameObject.Find("ScrapsCount").GetComponent<Text>();
        hand = GameObject.Find("Hand");
        flashBang = FindObjectOfType<FlashBang>();
        escMenu = FindObjectOfType<ESCMenuController>();
        zombiesInWorld = new List<AI_Zombie>();
    }
    private void Start()
    {
        graphicSetting.gameObject.SetActive(false);
        uiHowtoPlay.gameObject.SetActive(false);
        if(ai_8Feet!=null)
        ai_8Feet.gameObject.SetActive(false);
    }
    public void  zombieNotice(Transform transform)
    {
        for (int i = 0; i < zombiesInWorld.Count; i++)
            if (zombiesInWorld[i] != null)
                zombiesInWorld[i].target = transform;
    }
}
