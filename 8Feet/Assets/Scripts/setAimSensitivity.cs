﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class setAimSensitivity : MonoBehaviour
{
    public Slider slider;
    public InputField inputField;
    public OptionProfile optionProfile;

    // Start is called before the first frame update
    void Start()
    {
        optionProfile = FindObjectOfType<OptionProfile>();
    }
    public void changeValue(Slider target)
    {
        target.value = float.Parse(inputField.text);
        setOption();
    }
    public void changeValue(InputField target)
    {
        target.text = slider.value.ToString();
        setOption();
    }
    public void setOption()
    {
        optionProfile.mouseSensitivity = slider.value;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
