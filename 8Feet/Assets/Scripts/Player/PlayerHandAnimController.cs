﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerHandAnimController : MonoBehaviour
{
    
    private Animator anim;
    // Start is called before the first frame update
    void Awake()
    {
        
        anim = GetComponent<Animator>();
    }
    public void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!GameManager.instance.playerController.m_IsWalking && !GameManager.instance.weaponController.isReloading && !GameManager.instance.weaponController.isAiming&&GameManager.instance.playerController.GetComponent<CharacterController>().velocity.magnitude > 0.1f)
            anim.SetBool("Run",true);
        else
            anim.SetBool("Run", false);
    }
}
