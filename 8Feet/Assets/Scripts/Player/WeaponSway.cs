﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSway : MonoBehaviour {

    public float amoint;
    public float maxAmount;
    public float smothAmount;

    public Vector3 initialIPositing;

	// Use this for initialization
	void Start () {
        this.initialIPositing = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        float movementX = -Input.GetAxis("Mouse X") * amoint;
        float movementY = -Input.GetAxis("Mouse Y") * amoint;

        movementX = Mathf.Clamp(movementX, -maxAmount, maxAmount);
        movementY = Mathf.Clamp(movementY, -maxAmount, maxAmount);

        Vector3 finalPosition = new Vector3(movementX, movementY, 0);
        transform.localPosition = Vector3.Lerp(transform.localPosition, finalPosition + initialIPositing, Time.deltaTime * smothAmount);
    }
}
