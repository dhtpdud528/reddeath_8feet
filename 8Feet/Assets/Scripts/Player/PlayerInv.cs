﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(WeaponController))]
public class PlayerInv : MonoBehaviour
{
    
    public GameObject handIcon;
    private Animator anim;
    
    public List<GameObject> backpackInv = new List<GameObject>();
    public GameObject[] quickInv = new GameObject[5];
    private float weight;
    public int currentAdress = 0;
    public int nextIndex;
    public int previousIndex;
    public bool isSwaping;
    
    public int scraps;
    public int wheelIndex;

    // Use this for initialization
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    void Start()
    {
        
        //OverMind.uiItemBox.gameObject.SetActive(false);
        GameObject ammo;
        switch (GameManager.instance.playerProfile.mos)
        {
            case PlayerProfile.MOS.Rifle:
                ammo = Instantiate(GameManager.instance.itemPactory.FindItem("7.62mm"));
                ammo.GetComponent<ItemAmmo>().totalNum = 40;
                backpackInv.Add(ammo);

                for (int i = 0; i < 3; i++)
                    backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("Battery")));
                for (int i = 0; i < 1; i++)
                    backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("Dynamite")));

                quickInv[0] = Instantiate(GameManager.instance.itemPactory.FindItem("N1Garbine"));
                quickInv[1] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                quickInv[2] = Instantiate(GameManager.instance.itemPactory.FindItem("Knife"));
                quickInv[3] = Instantiate(GameManager.instance.itemPactory.FindItem("Dynamite"));
                quickInv[4] = Instantiate(GameManager.instance.itemPactory.FindItem("FirstAidKit"));
                break;
            case PlayerProfile.MOS.SMG:
                ammo = Instantiate(GameManager.instance.itemPactory.FindItem("11.43mm"));
                ammo.GetComponent<ItemAmmo>().totalNum = 240;
                backpackInv.Add(ammo);

                for (int i = 0; i < 3; i++)
                    backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("Battery")));
                for (int i = 0; i < 2; i++)
                    backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("Dynamite")));

                quickInv[0] = Instantiate(GameManager.instance.itemPactory.FindItem("N1928 Tomas"));
                quickInv[1] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                quickInv[2] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                quickInv[3] = Instantiate(GameManager.instance.itemPactory.FindItem("Dynamite"));
                quickInv[4] = Instantiate(GameManager.instance.itemPactory.FindItem("FirstAidKit"));
                break;
            case PlayerProfile.MOS.Medic:
                ammo = Instantiate(GameManager.instance.itemPactory.FindItem("7.62mm"));
                ammo.GetComponent<ItemAmmo>().totalNum = 20;
                backpackInv.Add(ammo);

                for (int i = 0; i < 3; i++)
                    backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("Battery")));
                for (int i = 0; i < 2; i++)
                    backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("FirstAidKit")));

                quickInv[0] = Instantiate(GameManager.instance.itemPactory.FindItem("N1Garbine"));
                quickInv[1] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                quickInv[2] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                quickInv[3] = Instantiate(GameManager.instance.itemPactory.FindItem("Dynamite"));
                quickInv[4] = Instantiate(GameManager.instance.itemPactory.FindItem("FirstAidKit"));
                break;
            case PlayerProfile.MOS.Test:
                backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("Compass")));
                for (int i = 0; i < 3; i++)
                    backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("Battery")));
                for (int i = 0; i < 10; i++)
                    backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("Dynamite")));
                ammo = Instantiate(GameManager.instance.itemPactory.FindItem("7.62mm"));
                ammo.GetComponent<ItemAmmo>().totalNum = 200;
                backpackInv.Add(ammo);
                ammo = Instantiate(GameManager.instance.itemPactory.FindItem("11.43mm"));
                ammo.GetComponent<ItemAmmo>().totalNum = 999;
                backpackInv.Add(ammo);
                ammo = Instantiate(GameManager.instance.itemPactory.FindItem("9mm"));
                ammo.GetComponent<ItemAmmo>().totalNum = 200;
                backpackInv.Add(ammo);

                quickInv[0] = Instantiate(GameManager.instance.itemPactory.FindItem("N1Garbine"));
                quickInv[1] = Instantiate(GameManager.instance.itemPactory.FindItem("N1928 Tomas"));
                quickInv[2] = Instantiate(GameManager.instance.itemPactory.FindItem("Axe"));
                quickInv[3] = Instantiate(GameManager.instance.itemPactory.FindItem("Dynamite"));
                quickInv[4] = Instantiate(GameManager.instance.itemPactory.FindItem("FirstAidKit"));
                backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("mauiCP96")));
                scraps = int.MaxValue;
                break;
            default:
                for (int i = 0; i < 20; i++)
                    backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("Battery")));

                quickInv[0] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                quickInv[1] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                quickInv[2] = Instantiate(GameManager.instance.itemPactory.FindItem("Knife"));
                quickInv[3] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                quickInv[4] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                break;
        }
        for(int i = 0; i < quickInv.Length;i++)
            quickInv[i].GetComponent<Item>().pickUpItem();
        if(GameManager.instance.modsProfile.compass)
            backpackInv.Add(Instantiate(GameManager.instance.itemPactory.FindItem("Compass")));

        UIUpdate();
        GameManager.instance.hudStateInfo.UpdateHUDInfo();
        QuickInvInitialize();
    }



    private void FixedUpdate()
    {
        //QuickInvUpdate();
        for (int i = 0; i < backpackInv.Count; i++)
            backpackInv[i].transform.position = new Vector3(0, -999, 0);
    }
    public void EquipItemToHand(GameObject item)
    {
        if (item.GetComponent<Item>() is ItemGun)
        {
            int index = 0;

            if (quickInv[1].GetComponent<Item>().itemName == "")
                index = 1;
            else if (quickInv[0].GetComponent<Item>().itemName == "")
                index = 0;
            else if (currentAdress >= 0 && currentAdress <= 1)
                index = currentAdress;

            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                quickInv[index].GetComponent<Item>().DropItem_fromPlayer();
                quickInv[index].transform.SetParent(null);
                backpackInv.Add(quickInv[index]);
                quickInv[index].GetComponent<Item>().pickUpItem();
            }
            //dropItem(quickInv[index]);
            else
                GameObject.Destroy(quickInv[index]);
            quickInv[index] = item;
            GrapItem(quickInv[index]);
            quickInv[index].GetComponent<Item>().pickUpItem();
            DoSwap(index);
        }
        else if (item.GetComponent<Item>() is ItemDynamite)
        {
            int index = 3;

            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                quickInv[index].GetComponent<Item>().DropItem_fromPlayer();
                quickInv[index].transform.SetParent(null);
                PickUpItemToinv(quickInv[index]);
            }
            //dropItem(quickInv[index]);
            else
                GameObject.Destroy(quickInv[index]);
            quickInv[index] = item;
            GrapItem(quickInv[index]);
            quickInv[index].GetComponent<Item>().pickUpItem();
            DoSwap(index);
        }
        else if (item.GetComponent<Item>() is ItemMelee)
        {
            int index = 2;

            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                quickInv[index].GetComponent<Item>().DropItem_fromPlayer();
                quickInv[index].transform.SetParent(null);
                PickUpItemToinv(quickInv[index]);
            }
            //dropItem(quickInv[index]);
            else
                GameObject.Destroy(quickInv[index]);
            quickInv[index] = item;
            GrapItem(quickInv[index]);
            quickInv[index].GetComponent<Item>().pickUpItem();
            DoSwap(index);
        }
        else if (item.GetComponent<Item>() is ItemFirstAidKit)
        {
            int index = 4;

            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                quickInv[index].GetComponent<Item>().DropItem_fromPlayer();
                quickInv[index].transform.SetParent(null);
                PickUpItemToinv(quickInv[index]);
            }
            //dropItem(quickInv[index]);
            else
                GameObject.Destroy(quickInv[index]);
            quickInv[index] = item;
            GrapItem(quickInv[index]);
            quickInv[index].GetComponent<Item>().pickUpItem();
            DoSwap(index);
        }
        
    }
    public void PickUpItemToinv(GameObject item, MonoBehaviour parent)
    {
        if (parent is UIHand)
        {
            backpackInv.Add(item);
            item.GetComponent<Item>().pickUpItem();
        }
        else if (parent is UIVicinity)
            if (item is ItemGun && quickInv[0].GetComponent<Item>().itemName != "" && quickInv[1].GetComponent<Item>().itemName != "")
            {
                backpackInv.Add(item);
                item.GetComponent<Item>().pickUpItem();
            }
            else
                PickUpItemToinv(item);
        else
            PickUpItemToinv(item);
    }
    public void PickUpItemToinv(GameObject item)
    {
        if (item.GetComponent<Item>() is ItemGun)
        {
            int index = 0;

            if (quickInv[0].GetComponent<Item>().itemName == "")
                index = 0;
            else if (quickInv[1].GetComponent<Item>().itemName == "")
                index = 1;
            else if (currentAdress >= 0 && currentAdress <= 1)
                index = currentAdress;
            if (quickInv[index].GetComponent<Item>().itemName == "")
            {
                GameObject.Destroy(quickInv[index]);
                quickInv[index] = item;
                GrapItem(quickInv[index]);
                quickInv[index].GetComponent<Item>().pickUpItem();
                DoSwap(index);
            }
            else
            {
                item.GetComponent<Item>().pickUpItem();
                backpackInv.Add(item);
            }
        }
        else if (item.GetComponent<Item>() is ItemDynamite)
        {
            if (((ItemDynamite)item.GetComponent<Item>()).Ping) return;
            int index = 3;
            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                backpackInv.Add(item);
                item.GetComponent<Item>().pickUpItem();
            }
            else
            {
                quickInv[index] = item;
                GrapItem(quickInv[index]);
                quickInv[index].GetComponent<Item>().pickUpItem();
            }
        }
        else if (item.GetComponent<Item>() is ItemFirstAidKit)
        {
            int index = 4;
            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                backpackInv.Add(item);
                item.GetComponent<Item>().pickUpItem();
            }
            else
            {
                quickInv[index] = item;
                GrapItem(quickInv[index]);
                quickInv[index].GetComponent<Item>().pickUpItem();
            }
        }
        else if (item.GetComponent<Item>() is ItemAmmo)
        {
            GameObject searchedAmmo = SearchAmmo(item.GetComponent<ItemAmmo>().ammoType);
            if (searchedAmmo != null)
            {
                searchedAmmo.GetComponent<ItemAmmo>().totalNum += item.GetComponent<ItemAmmo>().totalNum;
                Destroy(item);
            }
            else
            {
                item.GetComponent<Item>().pickUpItem();
                backpackInv.Add(item);
            }
        }
        else if (item.GetComponent<Item>() is ItemMelee)
        {
            int index = 2;
            if (quickInv[index].GetComponent<Item>().itemName != "")
            {
                backpackInv.Add(item);
                item.GetComponent<Item>().pickUpItem();
            }
            else
            {
                quickInv[index] = item;
                GrapItem(quickInv[index]);
                quickInv[index].GetComponent<Item>().pickUpItem();
            }
        }
        else
        {
            item.GetComponent<Item>().pickUpItem();
            backpackInv.Add(item);
        }
        UIUpdate();
    }
    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.uiInv.gameObject.active && Input.GetAxis("Mouse ScrollWheel") != 0)
            SwitchItem(Input.GetAxis("Mouse ScrollWheel"));
        if (Input.GetKeyDown(KeyCode.Q))//버리기
        {
            /*anim.CrossFadeInFixedTime("Idle", 0.01f);
            OverMind.hudProgressBar.imageProgressBar.fillAmount = 0;
            if (GetCurrentItemInfo().itemName != "")
                DropItemFromHand();*/
            DoSwap(previousIndex);
            UIUpdate();
        }
        RaycastHit hitray;
        if (Physics.Raycast(GameManager.instance.weaponController.shootPoint.position, GameManager.instance.weaponController.shootPoint.transform.forward, out hitray, 3, (1 << 9) + (1 << 13)))
            handIcon.SetActive(true);
        else
            handIcon.SetActive(false);
        if (Input.GetButtonDown("Interactive"))
        {
            UIUpdate();
            RaycastHit hit;
            if (Physics.Raycast(GameManager.instance.weaponController.shootPoint.position, GameManager.instance.weaponController.shootPoint.transform.forward, out hit, 3, 1 << 9))//아이템일 경우
                PickUpItemToinv(hit.collider.gameObject);
            else if (Physics.Raycast(GameManager.instance.weaponController.shootPoint.position, GameManager.instance.weaponController.shootPoint.transform.forward, out hit, 3, 1 << 13))//상자일 경우
            {
                StopCoroutine(hit.collider.GetComponent<ItemBox>().emissionPingPong);
                hit.collider.GetComponent<ItemBox>().mat.DisableKeyword("_EMISSION");

                GameManager.instance.uiVicini.transform.parent.parent.gameObject.SetActive(false);
                GameManager.instance.uiVicini.transform.parent.parent.GetChild(0).GetComponent<Text>().enabled = false;
                GameManager.instance.uiItemBox.transform.parent.parent.gameObject.SetActive(true);
                GameManager.instance.uiItemBox.transform.parent.parent.GetChild(0).GetComponent<Text>().enabled = true;
                GameManager.instance.uiItemBox.box = hit.collider.GetComponent<ItemBox>();
                GameManager.instance.uiItemBox.UpdateUIInfo();
                GameManager.instance.weaponController.uiItems.SetActive(!GameManager.instance.weaponController.uiItems.active);
            }

        }
        if (GameManager.instance.uiItemBox.gameObject.active && Vector3.Distance(transform.position, GameManager.instance.uiItemBox.box.transform.position) > 4)
            GameManager.instance.weaponController.uiItems.SetActive(false);
        if (Input.GetKeyDown(KeyCode.R))
            GameManager.instance.hudStateInfo.UpdateHUDInfo();
        if (!GameManager.instance.uiCount.gameObject.active)
            if (Input.GetKeyDown(KeyCode.Alpha1))
                DoSwap(0);
            else if (Input.GetKeyDown(KeyCode.Alpha2))
                DoSwap(1);
            else if (Input.GetKeyDown(KeyCode.Alpha3))
                DoSwap(2);
            else if (Input.GetKeyDown(KeyCode.Alpha4))
                DoSwap(3);
            else if (Input.GetKeyDown(KeyCode.Alpha5))
                DoSwap(4);
    }

    public void DropItemFromHand()
    {
        GameObject findItem;
        string findItemName = quickInv[currentAdress].GetComponent<Item>().itemName;
        findItem = SearchItem_inBackpack(findItemName);
        GetCurrentItemInfo().GetComponent<Item>().DropItem_fromPlayer();
        quickInv[currentAdress].transform.SetParent(null);
        if (findItem != null)
        {
            //아이템 리필
            backpackInv.Remove(findItem);
            if (quickInv[currentAdress].GetComponent<Item>().itemName == "")
                Destroy(quickInv[currentAdress]);
            quickInv[currentAdress] = GrapItem(findItem);
        }
        else
        {
            GameManager.instance.playerAnimCon.NoItem();
            quickInv[currentAdress] = Instantiate(GameManager.instance.itemPactory.FindItem(""));
            quickInv[currentAdress].transform.SetParent(GameManager.instance.hand.transform);
        }
    }
    public void DropItem(GameObject item)
    {
        item.GetComponent<Item>().DropItem_fromPlayer();
        item.transform.SetParent(null);
        item = Instantiate(GameManager.instance.itemPactory.FindItem(""));
        item.transform.SetParent(GameManager.instance.hand.transform);
    }
    public void DropItem_fromBackpack(Item item)
    {
        if (backpackInv.Count == 0)
            return;
        int index = 0;
        GameObject itemOBJ = SearchItem_inBackpack(item, out index);

        itemOBJ.GetComponent<Item>().DropItem_fromPlayer();
        itemOBJ.transform.SetParent(null);
        backpackInv.RemoveAt(index);
    }
    public void DrawItem_fromBackpack(Item item)
    {
        if (backpackInv.Count == 0)
            return;
        int index = 0;
        GameObject itemOBJ = SearchItem_inBackpack(item, out index);

        itemOBJ.transform.SetParent(null);
        backpackInv.RemoveAt(index);
    }

    void CheckIndex()
    {
        if (currentAdress > 4)
            currentAdress = 0;
        else if (currentAdress < 0)
            currentAdress = 4;
        if (quickInv[currentAdress].GetComponent<Item>().itemName == "")
            GameManager.instance.playerAnimCon.NoItem();
    }

    public float GetInvWeight()
    {
        weight = 0;
        for (int i = 0; i < 4; i++)
            if (quickInv[i] != null)
                weight += quickInv[i].GetComponent<Item>().i_Weight;
        if (backpackInv.Count > 0)
            for (int i = 0; i < backpackInv.Count; i++)
                if (backpackInv[i] != null)
                    weight += backpackInv[i].GetComponent<Item>().i_Weight;
        return weight;
    }

    public GameObject SearchAmmo(ItemAmmo.AmmoTypes search)
    {
        GameObject find = null;
        if (backpackInv.Count == 0)
            return null;
        for (int i = 0; i < backpackInv.Count; i++)
        {
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>() is ItemAmmo && backpackInv[i].GetComponent<ItemAmmo>().ammoType == search)
            {
                find = backpackInv[i];
                break;
            }
        }
        if (find != null)
            return find;
        else
            return null;
    }
    public GameObject SearchItem_inAllInv(Item item)
    {
        GameObject find = null;
        if (quickInv.Length == 0)
            return null;
        for (int i = 0; i < quickInv.Length; i++)
            if (quickInv[i] != null && quickInv[i].GetComponent<Item>().id == item.id)
            {
                find = quickInv[i];
                break;
            }
        if (find == null)
            find = SearchItem_inBackpack(item);
        return find;
    }
    public GameObject SearchItem_inBackpack(string search)
    {
        GameObject find = null;
        if (backpackInv.Count == 0)
            return null;
        for (int i = 0; i < backpackInv.Count; i++)
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>().itemName == search)
            {
                find = backpackInv[i];
                break;
            }
        return find;
    }
    public GameObject SearchItem_inBackpack(string search, out int adress)
    {
        GameObject find = null;
        adress = 0;
        if (backpackInv.Count == 0)
            return null;
        for (int i = 0; i < backpackInv.Count; i++)
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>().itemName == search)
            {
                find = backpackInv[i];
                adress = i;
                break;
            }
        return find;
    }
    public GameObject SearchItem_inBackpack(Item item)
    {
        GameObject find = null;
        if (backpackInv.Count == 0)
            return null;
        for (int i = 0; i < backpackInv.Count; i++)
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>().id == item.id)
            {
                find = backpackInv[i];
                break;
            }
        return find;
    }
    public GameObject SearchItem_inBackpack(Item item, out int adress)
    {
        GameObject find = null;
        adress = 0;
        if (backpackInv.Count == 0)
            return null;
        for (int i = 0; i < backpackInv.Count; i++)
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>().id == item.id)
            {
                find = backpackInv[i];
                adress = i;
                break;
            }
        return find;
    }
    public int CountItem_inBackpack(string search)
    {
        int find = 0;
        if (backpackInv.Count == 0)
            return 0;
        for (int i = 0; i < backpackInv.Count; i++)
            if (backpackInv[i] != null && backpackInv[i].GetComponent<Item>().itemName == search)
                find++;
        for (int i = 0; i < quickInv.Length; i++)
            if (quickInv[i] != null && quickInv[i].GetComponent<Item>().itemName == search)
                find++;
        return find;
    }
    void QuickInvInitialize()
    {
        for (int i = 0; i < 5; i++)
            if (quickInv[i] != null)
            {
                quickInv[i].transform.SetParent(GameManager.instance.hand.transform);
                quickInv[i].transform.localPosition = quickInv[i].GetComponent<Item>().HandPosition;
                quickInv[i].transform.localRotation = Quaternion.Euler(quickInv[i].GetComponent<Item>().HandRotation.x, quickInv[i].GetComponent<Item>().HandRotation.y, quickInv[i].GetComponent<Item>().HandRotation.z);
                if (i != 0)
                    quickInv[i].SetActive(false);
            }
    }
    public void UIUpdate()
    {
        GameManager.instance.uiMenu.gameObject.SetActive(false);
        if (GetCurrentItemInfo() is ItemGun)
            GameManager.instance.weaponController.gun = (ItemGun)GetCurrentItemInfo();
        if(SearchItem_inBackpack("Compass"))
            GameManager.instance.uiCompass.SetActive(true);
        else
            GameManager.instance.uiCompass.SetActive(false);
        for (int i = 0; i < 5; i++)
        {
            if (quickInv[currentAdress] != null)
            {
                if (quickInv[i] != null)
                {
                    if (quickInv[i].GetComponent<Item>().totalNum <= 0)
                    {
                        GameObject findItem;
                        string findItemName = quickInv[i].GetComponent<Item>().itemName;
                        findItem = SearchItem_inBackpack(findItemName);
                        GameObject handItem = Instantiate(GameManager.instance.itemPactory.FindItem(""));
                        quickInv[i].transform.SetParent(null);
                        quickInv[i] = GrapItem(handItem);
                        if (findItem != null)
                        {
                            //아이템 리필
                            backpackInv.Remove(findItem);
                            quickInv[i] = GrapItem(findItem);
                            StopCoroutine(quickInv[i].GetComponent<Item>().emissionPingPong);
                            quickInv[i].GetComponent<Item>().stopEmission();
                        }
                    }
                    else
                    {
                        if (quickInv[i].GetComponent<Item>().id == quickInv[currentAdress].GetComponent<Item>().id)
                            quickInv[i].SetActive(true);
                        else
                            quickInv[i].SetActive(false);
                        StopCoroutine(quickInv[i].GetComponent<Item>().emissionPingPong);
                        quickInv[i].GetComponent<Item>().stopEmission();
                    }
                }
            }
            else if (GameManager.instance.hand.transform.childCount >= i + 1 && GameManager.instance.hand.transform.GetChild(i) != null)
                GameManager.instance.hand.transform.GetChild(i).gameObject.SetActive(false);
        }
        GameManager.instance.hudStateInfo.UpdateHUDInfo();
        GameManager.instance.hudQuickSlotsInfo.UpdateHUDInfo();
        GameManager.instance.uiInv.checkTime = 0;
        GameManager.instance.uiItemBox.checkTime = 0;
        GameManager.instance.uiVicini.checkTime = 0;
        GameManager.instance.uiHand.checkTime = 0;
        GameManager.instance.playerAnimCon.NoItem();
        GameManager.instance.playerAnimCon.FindHandPosition();
        GameManager.instance.scrapsCount.text = "Scraps : " + scraps;
    }

    public Item GetCurrentItemInfo()
    {
        if (quickInv[currentAdress] != null)
        {
            Item item = quickInv[currentAdress].GetComponent<Item>();
            return item;
        }
        return null;
    }

    public void SwitchItem(float i)
    {
        if (i < 0)
            wheelIndex++;
        else
            wheelIndex--;
        if (wheelIndex > 4)
            wheelIndex = 0;
        else if (wheelIndex < 0)
            wheelIndex = 4;
        DoSwap(wheelIndex);
        /*checkIndex();
        UIInvUpdate();
        getInvWeight();*/
    }
    private void DoSwap(int i)
    {
        if (currentAdress == i || nextIndex == i || GameManager.instance.weaponController.isMeleeAttacking)
            return;
        if (GetCurrentItemInfo() is ItemMelee && !((ItemMelee)GetCurrentItemInfo()).canSwing) return;
        ItemDynamite dynamite = GetCurrentItemInfo() as ItemDynamite;

        if (dynamite != null && dynamite.Ping)
            dynamite.ThrowDynamite(GameManager.instance.weaponController.shootPoint.transform.forward);
        else
            GetCurrentItemInfo().CancelUsing();

        GameManager.instance.hudProgressBar.imageProgressBar.fillAmount = 0;

        if (GameManager.instance.hudProgressBar != null)
            GameManager.instance.hudProgressBar.imageProgressBar.fillAmount = 0;
        isSwaping = true;
        nextIndex = i;
        previousIndex = currentAdress;
        if (nextIndex > 4)
            nextIndex = 0;
        else if (nextIndex < 0)
            nextIndex = 4;
        wheelIndex = nextIndex;
        GameManager.instance.hudQuickSlotsInfo.UpdateHUDInfo();
        anim.CrossFadeInFixedTime("PutIn", 0.01f);
    }
    public void ChangeCurrentAdress(int i)
    {
        currentAdress = i;
        CheckIndex();
        GameManager.instance.weaponController.isAiming = false;
        GameManager.instance.hand.transform.parent.GetComponent<WeaponSway>().initialIPositing = GameManager.instance.weaponController.originPos;
        GameManager.instance.weaponController.maincam.fieldOfView = 60;
        UIUpdate();
    }
    public GameObject GrapItem(GameObject item)
    {
        item.transform.SetParent(GameManager.instance.hand.transform);
        item.transform.localPosition = item.GetComponent<Item>().HandPosition;
        item.transform.localRotation = Quaternion.Euler(item.GetComponent<Item>().HandRotation.x, item.GetComponent<Item>().HandRotation.y, item.GetComponent<Item>().HandRotation.z);
        return item;
    }
}