﻿using System;
using EZCameraShake;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class WeaponController : MonoBehaviour
{
    
    public bool isCinematic;
    public AudioClip HitMarkSFX;
    public Animator animWeapon;
    private Animator animBody;
    public AudioSource _AudioSource;
    public CharacterController characterCon;
    public Camera maincam;

    public float range = 100f;

    public Transform shootPoint;
    private float fireTimer = 0;
    public bool isReloading;
    public bool isMeleeAttacking;

    public ItemGun gun;

    public Vector3 aimPos;
    public Vector3 originPos;
    public float aodSpeed;

    public bool isAiming;

    [SerializeField] private GameObject bullet;
    public GameObject uiItems;
    float reloadSpeed;
    private void Awake()
    {
        uiItems = GameObject.Find("UIItems");
        
    }
    // Use this for initialization
    void Start()
    {
        
        animBody = FindObjectOfType<PlayerAnimController>().GetComponent<Animator>();
        _AudioSource = GetComponent<AudioSource>();
        uiItems.SetActive(false);
        GameManager.instance.escMenu.gameObject.SetActive(false);
        maincam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        animWeapon = GetComponent<Animator>();
        originPos = GameManager.instance.hand.transform.parent.localPosition;
    }

    private int checkLeftAmmo(ItemAmmo.AmmoTypes ammoType)
    {
        return GameManager.instance.playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum;
    }

    // Update is called once per frame
    void Update()
    {
        

        if (isCinematic
            || GameManager.instance.uiHowtoPlay.buttons.gameObject.active
            ) return;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.instance.escMenu.gameObject.SetActive(!GameManager.instance.escMenu.gameObject.active);
            uiItems.SetActive(false);
        }
        if (!GameManager.instance.escMenu.gameObject.active && Input.GetButtonDown("Inventory"))
        {
            GameManager.instance.playerInv.UIUpdate();
            uiItems.SetActive(!uiItems.active);
            GameManager.instance.uiVicini.transform.parent.parent.gameObject.SetActive(true);
            GameManager.instance.uiVicini.transform.parent.parent.GetChild(0).GetComponent<Text>().enabled = true;
            GameManager.instance.uiItemBox.transform.parent.parent.gameObject.SetActive(false);
            GameManager.instance.uiItemBox.transform.parent.parent.GetChild(0).GetComponent<Text>().enabled = false;
            GameManager.instance.uiItemBox.box = null;
        }
        if (GameManager.instance.hand.transform.parent.GetComponent<WeaponSway>() != null)
            if (GameManager.instance.escMenu.gameObject.active || uiItems.active
                || GameManager.instance.uiHowtoPlay.buttons.gameObject.active
                )
            {
                GameManager.instance.hand.transform.parent.GetComponent<WeaponSway>().enabled = false;
                return;
            }
            else
                GameManager.instance.hand.transform.parent.GetComponent<WeaponSway>().enabled = true;
        if (GameManager.instance.uiItemBox.gameObject.active && Vector3.Distance(transform.position, GameManager.instance.playerInv.transform.position) > 5)
            uiItems.SetActive(false);

        if (GameManager.instance.playerInv.GetCurrentItemInfo() != null && !GameManager.instance.playerState.confusion)
        {
            if (Input.GetKeyDown(KeyCode.V) && !GameManager.instance.playerInv.isSwaping)//밀리어택
                DoMelee(1);
            if (GameManager.instance.playerInv.GetCurrentItemInfo() is ItemGun)
            {
                bool shootingMode;
                if (gun.fireType == ItemGun.fireTypes.Auto)
                    shootingMode = Input.GetButton("Fire1");
                else
                    shootingMode = Input.GetButtonDown("Fire1");

                if (!isMeleeAttacking)
                    DoAim();

                if (shootingMode)
                {
                    if (GameManager.instance.playerController.TimeDodge >= 0.5f && !GameManager.instance.playerInv.isSwaping)
                    {
                        if (gun.currentBlullets > 0)
                            Fire();
                        else if (GameManager.instance.playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum > 0)
                        {
                            DoReload();
                            gun.muzzleFlash.Stop();
                        }
                        else
                            gun.muzzleFlash.Stop();
                    }
                }
                else
                    gun.muzzleFlash.Stop();

                if (Input.GetKeyDown(KeyCode.R) && !GameManager.instance.playerInv.isSwaping)//재장전
                    if (gun.currentBlullets < gun.bulletsPerMag && GameManager.instance.playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum > 0)
                        DoReload();

                if (fireTimer < gun.fireRate)
                    fireTimer += Time.deltaTime;
            }
            else if (GameManager.instance.playerInv.GetCurrentItemInfo() is ItemDynamite)
            {
                if (Input.GetButton("Fire1"))
                    ((ItemDynamite)GameManager.instance.playerInv.GetCurrentItemInfo()).PrepareItem();
                else if (Input.GetButtonUp("Fire1"))
                {
                    if (((ItemDynamite)GameManager.instance.playerInv.GetCurrentItemInfo()).Ping)
                    {
                        ((ItemDynamite)GameManager.instance.playerInv.GetCurrentItemInfo()).ThrowDynamite(shootPoint.transform.forward);
                        GameManager.instance.hudStateInfo.UpdateHUDInfo();
                        GameManager.instance.playerInv.UIUpdate();
                    }
                    else
                        ((ItemDynamite)GameManager.instance.playerInv.GetCurrentItemInfo()).CancelUsing();
                }

            }
            else if (GameManager.instance.playerInv.GetCurrentItemInfo() is ItemFirstAidKit)
            {
                if (Input.GetButton("Fire1") && GameManager.instance.playerState.GetHealth() < GameManager.instance.playerState.maxHeath)
                    ((ItemFirstAidKit)GameManager.instance.playerInv.GetCurrentItemInfo()).PrepareItem();
                else if (Input.GetButtonUp("Fire1"))
                {
                    ((ItemFirstAidKit)GameManager.instance.playerInv.GetCurrentItemInfo()).CancelUsing();
                    GameManager.instance.hudStateInfo.UpdateHUDInfo();
                    GameManager.instance.playerInv.UIUpdate();
                }
            }
            else if (GameManager.instance.playerInv.GetCurrentItemInfo() is ItemMelee)
            {
                if (Input.GetButton("Fire1") && ((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).canSwing && !GameManager.instance.playerInv.isSwaping)
                    ((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).PrepareItem();
                else if (Input.GetButtonUp("Fire1") && ((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).canSwing && !GameManager.instance.playerInv.isSwaping)
                {
                    GameManager.instance.playerState.stemina += ((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).needStemina;
                    GameManager.instance.playerState.AddRelax(-2 * ((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).needStemina);
                    ((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).SwingMelee();
                }
            }
        }


    }

    internal void PlayHitMark()
    {
        _AudioSource.PlayOneShot(HitMarkSFX);
    }

    private void DoAim()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            //transform.localPosition = Vector3.Lerp(transform.localPosition, aimPos, Time.deltaTime * aodSpeed);
            GameManager.instance.hand.transform.parent.GetComponent<WeaponSway>().initialIPositing = aimPos;
            isAiming = true;
            maincam.fieldOfView = 50;
        }
        else if (Input.GetButtonUp("Fire2"))
        {
            //transform.localPosition = Vector3.Lerp(transform.localPosition, originPos, Time.deltaTime * aodSpeed);
            GameManager.instance.hand.transform.parent.GetComponent<WeaponSway>().initialIPositing = originPos;
            isAiming = false;
            maincam.fieldOfView = 60;
        }
    }

    private void FixedUpdate()
    {
        AnimatorStateInfo infoHand = animWeapon.GetCurrentAnimatorStateInfo(0);

        reloadSpeed = gun.reloadSpeed;
        if (GameManager.instance.playerProfile != null)
            reloadSpeed *= GameManager.instance.playerProfile.reloadSpeed;
        if (GameManager.instance.modsProfile != null)
            reloadSpeed *= GameManager.instance.modsProfile.reloadSpeed;
        isReloading = infoHand.IsName("Reload");
        isMeleeAttacking = infoHand.IsName("Melee");
        animWeapon.SetFloat("ReloadSpeed", reloadSpeed);
    }

    private void Fire()
    {
        if (fireTimer < gun.fireRate || gun.currentBlullets <= 0 || isMeleeAttacking || GameManager.instance.playerState.isUnderWater || isReloading) return;
        gun.currentBlullets--;

        GameManager.instance.spawner.AddNoise(gun.noise, GameManager.instance.playerController.transform.position);
        GameManager.instance.zombieNotice(GameManager.instance.playerState.transform);

        if (gun.lastShootSound != null && gun.currentBlullets <= 0)
            _AudioSource.PlayOneShot(gun.lastShootSound);
        _AudioSource.PlayOneShot(gun.shootSound);

        RaycastHit hit;
        Vector3 shootDir = shootPoint.transform.forward;
        int aim = 1;
        if (isAiming)
            aim = 3;
        float moveSpeed = characterCon.velocity.magnitude / 200;

        shootDir.z += UnityEngine.Random.Range(-gun.MOA / aim - moveSpeed, gun.MOA / aim + moveSpeed);
        shootDir.x += UnityEngine.Random.Range(-gun.MOA / aim - moveSpeed, gun.MOA / aim + moveSpeed);
        shootDir.y += UnityEngine.Random.Range(-gun.MOA / aim - moveSpeed, gun.MOA / aim + moveSpeed);
        GameObject bulletobj = Instantiate(bullet, shootPoint.position, Quaternion.FromToRotation(Vector3.forward, shootPoint.forward));
        bulletobj.transform.GetChild(0).GetComponent<Rigidbody>().AddForce(shootDir * 300, ForceMode.Impulse);
        bulletobj.transform.GetChild(1).GetComponent<EntityBullet>().gun = gun;
        bulletobj.transform.GetChild(1).GetComponent<Rigidbody>().AddForce(shootDir * 300, ForceMode.Impulse);
        bulletobj.transform.GetChild(1).GetComponent<BoxCollider>().size = new Vector3(
            bulletobj.transform.GetChild(1).GetComponent<BoxCollider>().size.x * gun.bulletWidth,
            bulletobj.transform.GetChild(1).GetComponent<BoxCollider>().size.y * gun.bulletHeight,
            bulletobj.transform.GetChild(1).GetComponent<BoxCollider>().size.z * gun.bulletLength);
        /*if (Physics.Raycast(shootPoint.position, shootDir, out hit, range, (1 << 10) + (1 << 0) + (1 << 12) + (1 << 14)))
        {
            GameObject bulletImpact;
            if (hit.collider.gameObject.tag == "Monster")
                bulletImpact = bodyBulletImpact;
            else
                bulletImpact = rockBulletImpact;
            GameObject bulletHole = Instantiate(bulletImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
            bulletHole.transform.SetParent(hit.transform);
        }*/

        animWeapon.CrossFadeInFixedTime("Fire", 0.01f);
        gun.muzzleFlash.Play();
        //PlayShootSound();


        fireTimer = 0;
        maincam.GetComponent<CameraShaker>().ShakeOnce(gun.recoil/ aim, gun.recoil/ aim, 0, 0.3f);
        GameManager.instance.hudStateInfo.UpdateHUDInfo();
    }

    private void DoMelee(int damage)
    {
        if (isMeleeAttacking) return;
        GameManager.instance.playerState.stemina += 10;
        GameManager.instance.playerState.AddRelax(-10);
        animWeapon.CrossFadeInFixedTime("Melee", 0.01f);
    }

    private void DoReload()
    {
        if (isReloading || isMeleeAttacking) return;
        animWeapon.CrossFadeInFixedTime("Reload", 0.01f);
        animBody.CrossFadeInFixedTime("Reload", 0.01f);
    }

    public void Reload()
    {
        gun.muzzleFlash.Stop();
        if (GameManager.instance.playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum <= 0) return;
        int bulletsToLoad = gun.bulletsPerMag - gun.currentBlullets;
        int bulletsToDeduct = (GameManager.instance.playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum >= bulletsToLoad) ? bulletsToLoad : GameManager.instance.playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum;

        GameManager.instance.playerInv.SearchAmmo(gun.ammoType).GetComponent<Item>().totalNum -= bulletsToDeduct;
        gun.currentBlullets += bulletsToDeduct;
        GameManager.instance.hudStateInfo.UpdateHUDInfo();
    }

    private void PlayShootSound()
    {
        _AudioSource.PlayOneShot(gun.shootSound);
    }

}
