﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadState : StateMachineBehaviour {

    public float reloadTime = 0.7f;
    bool hasReload = false;
    ItemGun gun;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        hasReload = false;
        gun = (ItemGun)animator.GetComponent<PlayerInv>().GetCurrentItemInfo();
    }

	//OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (hasReload) return;
        if (0.125f <= stateInfo.normalizedTime && stateInfo.normalizedTime < 0.25f  && animator.GetComponent<AudioSource>().clip != gun.ReloadSound[0])
        {
            animator.GetComponent<AudioSource>().clip = gun.ReloadSound[0];
            animator.GetComponent<AudioSource>().Play();
        }
        if (0.25f <= stateInfo.normalizedTime && stateInfo.normalizedTime < 0.405f && animator.GetComponent<AudioSource>().clip != gun.ReloadSound[1])
        {
            animator.GetComponent<AudioSource>().clip = gun.ReloadSound[1];
            animator.GetComponent<AudioSource>().Play();
        }
        if (0.405f <= stateInfo.normalizedTime && stateInfo.normalizedTime < 0.5f && animator.GetComponent<AudioSource>().clip != gun.ReloadSound[2])
        {
            animator.GetComponent<AudioSource>().clip = gun.ReloadSound[2];
            animator.GetComponent<AudioSource>().Play();
        }
        if (0.5f <= stateInfo.normalizedTime && stateInfo.normalizedTime < 0.625f && animator.GetComponent<AudioSource>().clip != gun.ReloadSound[3])
        {
            animator.GetComponent<AudioSource>().clip = gun.ReloadSound[3];
            animator.GetComponent<AudioSource>().Play();
        }
        if (stateInfo.normalizedTime >= reloadTime)
        {
            animator.GetComponent<WeaponController>().Reload();
            hasReload = true;
        }
	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        hasReload = false;

    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
