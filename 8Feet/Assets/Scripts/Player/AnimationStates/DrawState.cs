﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawState : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetFloat("DrawSpeed", animator.GetComponent<PlayerInv>().GetCurrentItemInfo().drawSpeed);
    }
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<PlayerInv>().isSwaping = false;
    }
}
