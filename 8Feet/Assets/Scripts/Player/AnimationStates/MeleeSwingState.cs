﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeSwingState : StateMachineBehaviour
{
    
    private void Awake()
    {
        
    }
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        if (GameManager.instance.playerInv.GetCurrentItemInfo() is ItemMelee)
            ((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).canSwing = false;
        GameManager.instance.weaponController.shootPoint.GetComponent<MeleeHitBox>().maxStack = GameManager.instance.playerInv.GetCurrentItemInfo().maxHit;
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (0.1f <= stateInfo.normalizedTime && stateInfo.normalizedTime <= 0.7f)
        {
            GameManager.instance.weaponController.shootPoint.GetComponent<MeleeHitBox>().damage =
                GameManager.instance.playerInv.GetCurrentItemInfo() is ItemMelee ?
                GameManager.instance.weaponController.isMeleeAttacking ? 1 : ((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).damege :
                1;
            if (GameManager.instance.playerInv.GetCurrentItemInfo() is ItemMelee)
                if (((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).damege > ((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).damegeOrigin)//차지상태일때
                {
                    if (((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).chargedMulti)//멀티타격
                        GameManager.instance.weaponController.shootPoint.GetComponents<BoxCollider>()[1].enabled = true;
                    else
                        GameManager.instance.weaponController.shootPoint.GetComponents<BoxCollider>()[0].enabled = true;
                }
                else//차지상태아닐때
                {
                    if (((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).quickMulti)//멀티타격
                        GameManager.instance.weaponController.shootPoint.GetComponents<BoxCollider>()[1].enabled = true;
                    else
                        GameManager.instance.weaponController.shootPoint.GetComponents<BoxCollider>()[0].enabled = true;
                }
            else
                GameManager.instance.weaponController.shootPoint.GetComponents<BoxCollider>()[1].enabled = true;

        }
        else
        {
            for (int i = 0; i < GameManager.instance.weaponController.shootPoint.GetComponents<BoxCollider>().Length; i++)
                GameManager.instance.weaponController.shootPoint.GetComponents<BoxCollider>()[i].enabled = false;
            GameManager.instance.weaponController.shootPoint.GetComponent<MeleeHitBox>().ids.Clear();
            GameManager.instance.weaponController.shootPoint.GetComponent<MeleeHitBox>().currentStack = 0;
        }
        if (GameManager.instance.playerInv.GetCurrentItemInfo() is ItemMelee)
            if (stateInfo.normalizedTime >= 0.9)
                ((ItemMelee)GameManager.instance.playerInv.GetCurrentItemInfo()).canSwing = true;
    }
}
