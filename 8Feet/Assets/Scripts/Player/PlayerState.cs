﻿using EZCameraShake;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using Random = UnityEngine.Random;

public class PlayerState : MonoBehaviour
{
    
    public float maxHeath = 100;
    private float health;

    public float maxStemina = 100;

    public float stemina;
    public enum m_deburf { Down = 0, Bloody = 1, Confusion = 2, Slow = 3 };
    public bool down = false;
    public bool bloody = false;
    public bool confusion = false;
    public bool slow = false;
    public float confusionTimeMax = 4;
    [SerializeField] private float confusionTime;

    private float relax;
    public float maxDownTime;
    [SerializeField] private float downTime = 0;

    float playerHeight_Origin;
    Vector3 playerCenter_Origin;

    /*private PostProcessingBehaviour postProcessing;
    [SerializeField] private MotionBlurModel.Settings settings;*/
    private float playerCurrentCenter;
    public bool standUp;
    public GameObject maincam;
    [SerializeField] private float slowTime;
    public float slowMaxTime;

    public float walkSpeedOrigin;
    public float runSpeedOrigin;
    public AudioSource audioSource;
    public AudioClip[] punchSounds;
    public AudioClip[] stompSounds;
    public AudioClip[] gruntsSounds;
    public AudioClip[] criticalSounds;
    public AudioClip[] bloodSounds;
    public bool isPain;
    public bool isOnWater;
    public bool isUnderWater;
    public float breathTime;

    public GameObject[] getUpTutEvent;

    public Image emergency;
    public float bearHealth;
    [SerializeField] private int downStack;
    private GameObject allOfPlayer;
    public IEnumerator bleeding;
    IEnumerator slowWalking;
    private bool dead;
    private IEnumerator dyingCoroutine;

    public AudioSource confusionAudioSources;
    public AudioClip tinnitusSound;
    public AudioSource dyingAudioSources;
    public AudioSource chaseAudioSources;

    private IEnumerator confusionCoroutine;

    private void Awake()
    {
        
        allOfPlayer = GameObject.Find("AllOfPlayer");
        audioSource = GetComponent<AudioSource>();
        maincam = FindObjectOfType<CameraShaker>().gameObject;
        playerHeight_Origin = GetComponent<CharacterController>().height;
        playerCenter_Origin = GetComponent<CharacterController>().center;
    }
    // Use this for initialization
    void Start()
    {
        
        bleeding = Bleeding();
        slowWalking = SlowWalking();
        relax = 100;
        slowTime = 0;
        standUp = true;
        if (GameManager.instance.playerProfile != null)
        {
            maxHeath *= GameManager.instance.playerProfile.maxHealth;
            maxStemina *= GameManager.instance.playerProfile.maxStemina;
        }
        if (GameManager.instance.modsProfile != null)
        {
            maxHeath *= GameManager.instance.modsProfile.maxHealth;
            maxStemina *= GameManager.instance.modsProfile.maxStemina;
        }
        health = maxHeath;
        bearHealth = 100;
        confusionTime = confusionTimeMax;
        /*postProcessing = FindObjectOfType<PostProcessingBehaviour>();
        settings.shutterAngle = 360;
        settings.sampleCount = 10;
        settings.frameBlending = 0;
        postProcessing.profile.motionBlur.settings = settings;*/
        emergency.CrossFadeAlpha(0, 0, false);
    }

    IEnumerator SlowWalking()
    {
        yield return new WaitForSeconds(20);
        SteamAchievements.UnlockAchievement("ACHIEVEMENT_Adrenaline");
    }

    public void AddRelax(float i)
    {
        if (relax + i < 0)
            relax = 0;
        else
            relax += i;
    }
    public float GetRelax()
    {
        return relax;
    }
    // Update is called once per frame
    void Update()
    {
        if (health < maxHeath)
            dyingAudioSources.volume = 1 / health;
        else
            dyingAudioSources.volume = 0;
        if (!isUnderWater && breathTime > 0)
            breathTime -= Time.deltaTime * 2;
        if (bloody && health > maxHeath / 200)
            TakeBleedingDamage(maxHeath / 200 * Time.deltaTime);
        if (GameManager.instance.playerController.TimeDodge < 1)
        {
            if (GetComponent<CharacterController>().height > 0)
                GetComponent<CharacterController>().height -= Time.deltaTime * 2;
            if (GetComponent<CharacterController>().center.y < 0.5f)
            {
                playerCurrentCenter += Time.deltaTime * 2;
                if (playerCurrentCenter > 0.5f)
                    playerCurrentCenter = 0.5f;
                GetComponent<CharacterController>().center = new Vector3(0, playerCurrentCenter, 0);
            }
        }
        else if (!down)
        {
            GameManager.instance.playerController.m_RunSpeed = runSpeedOrigin / (slow ? 3 : 1) / (GameManager.instance.weaponController.isAiming ? 2 : 1) / (isOnWater ? 2 : 1) / (isUnderWater ? 2 : 1);
            GameManager.instance.playerController.m_WalkSpeed = walkSpeedOrigin / (slow ? 3 : 1) / (GameManager.instance.weaponController.isAiming ? 2 : 1) / (isOnWater ? 2 : 1) / (isUnderWater ? 2 : 1);
            if (GetComponent<CharacterController>().height < playerHeight_Origin)
            {
                standUp = false;
                GetComponent<CharacterController>().height += Time.deltaTime * 2;
                if (GetComponent<CharacterController>().height > playerHeight_Origin)
                {
                    standUp = true;
                    GetComponent<CharacterController>().height = playerHeight_Origin;
                }
            }
            if (GetComponent<CharacterController>().center.y > playerCenter_Origin.y)
            {
                playerCurrentCenter -= Time.deltaTime * 2;
                if (playerCurrentCenter < playerCenter_Origin.y)
                    playerCurrentCenter = playerCenter_Origin.y;
                GetComponent<CharacterController>().center = new Vector3(0, playerCurrentCenter, 0);
            }
        }
        if (confusion)
        {
            if (confusionTime > 0)
                confusionTime -= Time.deltaTime;
            else
            {
                confusionTime = confusionTimeMax;
                confusion = false;
            }
        }
        if (slow)
        {
            if (slowTime < slowMaxTime)
                slowTime += Time.deltaTime;
            else
            {
                slowTime = 0;
                StopCoroutine(slowWalking);
                slow = false;
            }
        }
        if (down)
        {
            if (!dead && Input.GetKey(KeyCode.X))
            {
                downTime += Time.deltaTime;
                if (GameManager.instance.hudProgressBar.imageProgressBar.fillAmount < 1)
                    GameManager.instance.hudProgressBar.imageProgressBar.fillAmount += Time.deltaTime / maxDownTime;
                GameManager.instance.playerController.m_WalkSpeed = 0;
                GameManager.instance.playerController.m_RunSpeed = 0;
            }
            else
            {
                GameManager.instance.hudProgressBar.imageProgressBar.fillAmount = 0;
                downTime = 0;
                GetComponent<CharacterController>().height = 0;
                GetComponent<CharacterController>().center = new Vector3(0, 1, 0);
                GameManager.instance.playerController.m_WalkSpeed = 1;
                GameManager.instance.playerController.m_RunSpeed = 1;
            }
            if (downTime >= maxDownTime)
            {
                GameManager.instance.hudProgressBar.imageProgressBar.fillAmount = 0;
                GetComponent<CharacterController>().height = playerHeight_Origin;
                GetComponent<CharacterController>().center = playerCenter_Origin;
                transform.position = new Vector3(transform.position.x, transform.position.y + GetComponent<CharacterController>().height, transform.position.z);
                GameManager.instance.playerController.m_WalkSpeed = walkSpeedOrigin;
                GameManager.instance.playerController.m_RunSpeed = runSpeedOrigin;
                downTime = 0;

                if (health <= 0)
                    health = 10;

                down = false;
                if (downStack >= 5)
                    SteamAchievements.UnlockAchievement("ACHIEVEMENT_Marathon");
            }
        }
        if (health > maxHeath)
            health = maxHeath;
        else if (health <= 0)
        {
            health = 0;
            if (GameManager.instance.ai_8Feet == null ||
                Vector3.Distance(transform.position, GameManager.instance.ai_8Feet.transform.position) > 2f)
                down = true;
        }

        if (stemina > maxStemina)
            stemina = maxStemina;
        else if (stemina < 0)
            stemina = 0;

        if (relax > 100)
            relax = 100;
        else if (relax < 0)
            relax = 0;
    }

    public void Dead_8feet()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
        SteamAchievements.UnlockAchievement("ACHIEVEMENT_Keen_sense");
        if (confusion)
        {
            StopCoroutine(confusionCoroutine);
            confusionTime = confusionTimeMax;
            confusion = false;
            AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
            for (int i = 0; i < audioSources.Length; i++)
            {
                if (audioSources[i] != confusionAudioSources && audioSources[i] != dyingAudioSources && audioSources[i] != chaseAudioSources)
                    audioSources[i].volume = 1;
            }
        }
        chaseAudioSources.volume = 0;
        GameManager.instance.ai_8Feet.ai_state = AI_8Feet.STATE.Run;
        GameManager.instance.ai_8Feet.dead.SetActive(true);
        GameObject.Destroy(allOfPlayer);
        audioSource.PlayOneShot(bloodSounds[Random.Range(0, bloodSounds.Length)]);
        
    }
    public void Dead()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
        chaseAudioSources.volume = 0;
        if (dyingCoroutine != null)
            StopCoroutine(dyingCoroutine);
        dead = true;
        audioSource.PlayOneShot(bloodSounds[Random.Range(0, bloodSounds.Length)]);
        StartCoroutine(MoveDeadScene());
        emergency.CrossFadeAlpha(10f, 0.01F, false);
        
    }

    IEnumerator MoveDeadScene()
    {
        yield return new WaitForSecondsRealtime(4);
        if (Time.timeScale != 1)
            Time.timeScale = 1;
        SceneManager.LoadScene("GameOver");
    }
    
    public void TakeDamage(float damage, m_deburf deburf)
    {
        TakeDamage(damage);

        switch (deburf)
        {
            case m_deburf.Bloody:
                if (GameManager.instance.modsProfile.immunityBleeding) break;
                if (!bloody)
                {
                    bloody = true;
                    StartCoroutine(bleeding);
                    break;
                }
                break;
            case m_deburf.Confusion:
                SteamAchievements.confusedCount++;
                if (SteamAchievements.confusedCount > 8)
                    SteamAchievements.UnlockAchievement("ACHIEVEMENT_Helmet");
                if (!confusion && !GameManager.instance.modsProfile.immunityConfusion)
                {
                    StartCoroutine(confusionCoroutine = ConfusionEffects());
                    confusion = true;
                }
                break;
            case m_deburf.Down:
                down = true;
                break;
            case m_deburf.Slow:
                slowTime = 0;
                StartCoroutine(slowWalking);
                slow = true;
                break;
        }
        GameManager.instance.hudStateInfo.TakeDamage_HUD(damage * 5);
    }
    public void EndChase()
    {
        StartCoroutine(EndChaseCoroutine());
    }
    IEnumerator EndChaseCoroutine()
    {
        while (chaseAudioSources.volume > 0)
        {
            chaseAudioSources.volume -= 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        chaseAudioSources.Stop();
    }
    IEnumerator ConfusionEffects()
    {
        Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        Rect rect = new Rect(0, 0, tex.width, tex.height);
        yield return new WaitForEndOfFrame();
        tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        tex.Apply();

        GameManager.instance.flashBang.DoFlashBang(tex);


        confusionAudioSources.PlayOneShot(tinnitusSound);
        IEnumerator startConfusion = ConfusionSounds(true);
        StartCoroutine(startConfusion);
        yield return new WaitUntil(() => (confusionTime <= 0));
        StopCoroutine(startConfusion);
        StartCoroutine(ConfusionSounds(false));



    }
    IEnumerator ConfusionSounds(bool isConfusion)
    {
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        if (isConfusion)
        {
            confusionAudioSources.volume = 1;

            while (true)
            {
                for (int i = 0; i < audioSources.Length; i++)
                    if (audioSources[i] != confusionAudioSources && audioSources[i] != dyingAudioSources && audioSources[i] != chaseAudioSources)
                        audioSources[i].volume -= 0.01f;
                yield return new WaitForSecondsRealtime(0.01f);
            }
        }
        else
        {
            float soundValue = 0;
            while (soundValue <= 1)
            {
                for (int i = 0; i < audioSources.Length; i++)
                {
                    soundValue += 0.01f;
                    if (audioSources[i] != confusionAudioSources && audioSources[i] != dyingAudioSources && audioSources[i] != chaseAudioSources)
                        audioSources[i].volume = soundValue;
                }
                yield return new WaitForSecondsRealtime(0.1f);
            }
        }

    }

    IEnumerator Bleeding()
    {
        yield return new WaitForSeconds(60 * 3);
        SteamAchievements.UnlockAchievement("ACHIEVEMENT_Bandage");
    }
    public void TakeBleedingDamage(float damage)
    {
        if (damage > maxHeath && !GameManager.instance.modsProfile.withstand)
        {
            GameManager.instance.hudStateInfo.TakeDamage_HUD(damage * 10);
            health = 0;
            Dead();
            return;
        }
        if (damage > 0)
            health -= damage;
        if (damage > maxHeath)
            damage = health;
        SteamAchievements.totalReceivedDamage += damage;
        if (SteamAchievements.totalReceivedDamage >= 200)
            SteamAchievements.UnlockAchievement("ACHIEVEMENT_Vest");
    }
    public void TakeDamage(float damage)
    {
        if (damage > maxHeath && !GameManager.instance.modsProfile.withstand)
        {
            GameManager.instance.hudStateInfo.TakeDamage_HUD(damage * 10);
            health = 0;
            Dead();
            return;
        }
        if (damage > maxHeath)
            damage = health;

        if (damage > 0)
            health -= damage;
        if (!bloody)
            GameManager.instance.hudStateInfo.TakeDamage_HUD(damage * 10);
        if (damage >= 30 * GameManager.instance.difficultyProfile.enermyDamage)
        {
            audioSource.PlayOneShot(stompSounds[Random.Range(0, stompSounds.Length)]);
            if (!down)
                if (health / maxHeath < 0.5f)
                    audioSource.PlayOneShot(criticalSounds[Random.Range(0, criticalSounds.Length)]);
                else
                    audioSource.PlayOneShot(gruntsSounds[Random.Range(0, gruntsSounds.Length)]);
            else
            {
                Dead_8feet();
                return;
            }
            StartCoroutine(playerPain());
        }
        maincam.GetComponent<CameraShaker>().ShakeOnce(damage * 2, damage * 2, 0, 0.5f);
        audioSource.PlayOneShot(punchSounds[Random.Range(0, punchSounds.Length)]);
        if (health <= 0 && !down)
        {
            down = true;
            downStack++;

            if (!dead)
                StartCoroutine(dyingCoroutine = Dying());
            if (!GameManager.instance.dontDestroyOnOtherScene.getUpTut)
            {
                Instantiate(getUpTutEvent[0]).GetComponent<GameEvent>().Play();
                Instantiate(getUpTutEvent[1]).GetComponent<GameEvent>().Play();
                GameManager.instance.dontDestroyOnOtherScene.getUpTut = true;
            }
        }
    }

    IEnumerator Dying()
    {
        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        while (down)
        {
            for (int i = 0; i < audioSources.Length; i++)
                if (audioSources[i] != null)
                {
                    if (audioSources[i] != dyingAudioSources)
                    {
                        if (!confusion)
                            audioSources[i].volume = bearHealth / 100;
                    }
                    else
                        audioSources[i].volume += 0.01f;
                }
            bearHealth -= Time.deltaTime * downStack * 2;
            emergency.CrossFadeAlpha(10f / bearHealth, 0.01F, false);
            if (bearHealth <= 0)
            {
                SteamAchievements.UnlockAchievement("ACHIEVEMENT_Fatal_wound");
                SceneManager.LoadScene("GameOver");
            }
            yield return new WaitForSeconds(0.01f);
        }
        emergency.CrossFadeAlpha(0, 1, false);
        bearHealth = 100;
        for (int i = 0; i < audioSources.Length; i++)
            if (audioSources[i] != null)
            {
                if (audioSources[i] != dyingAudioSources && audioSources[i] != chaseAudioSources)
                    audioSources[i].volume = 1;
                else
                    audioSources[i].volume = 0;
            }
    }

    public double GetHealth()
    {
        return Math.Truncate(health * 10) / 10;
    }
    public void AddHealth(float i)
    {
        health += i;
    }
    IEnumerator playerPain()
    {
        isPain = true;
        yield return new WaitForSeconds(1);
        isPain = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Water") && breathTime < 0)
            breathTime = 0;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Water"))
        {
            isOnWater = true;
            if (transform.position.y <= 5.6)
            {
                isUnderWater = true;
                breathTime += Time.deltaTime;
                if (breathTime > 20)
                {
                    TakeDamage(Time.deltaTime);
                }
            }
            else
                isUnderWater = false;

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Water"))
        {
            isOnWater = false;
            isUnderWater = false;
        }
    }
}
