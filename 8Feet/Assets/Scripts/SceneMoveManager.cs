﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMoveManager : MonoBehaviour {

    public int sceneNum;
    public float waitTime;
    private void Start()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
    }
    private void FixedUpdate()
    {
        StartCoroutine(WaitSeconds(waitTime));
    }
    IEnumerator WaitSeconds(float t)
    {
        yield return new WaitForSeconds(t);
        SceneManager.LoadScene(sceneNum);
    }
}
