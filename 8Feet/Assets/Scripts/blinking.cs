﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blinking : MonoBehaviour {
    private new Light light;
    private float onrigin_light;

    // Use this for initialization
    void Start () {
        light = GetComponent<Light>();
        onrigin_light = light.intensity;
    }

    // Update is called once per frame
    void Update()
    {
        light.intensity = onrigin_light * Random.Range(0.7f, 1.0f);
        StartCoroutine(switchingLight());
    }
    IEnumerator switchingLight()
    {
        yield return new WaitForSeconds(0.2f);
    }
}
