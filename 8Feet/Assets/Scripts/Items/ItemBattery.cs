﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBattery : Item {
    public float leftPower = 100;
    public float maxPower = 100;

    // Use this for initialization
    protected void Start () {
        base.Start();
        leftPower = maxPower;
        itemName = "Battery";
        i_Weight = 0.015f;
	}
}
