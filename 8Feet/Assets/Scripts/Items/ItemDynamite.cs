﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDynamite : Item
{

    public float maxCountDown;
    public float countDown;

    public GameObject fuseParticle;
    public GameObject boomParticle;
    public bool Ping = false;

    public override void PrepareItem()
    {
        if (GameManager.instance.playerState.isUnderWater) return;
        base.PrepareItem();
        if (GameManager.instance.hudProgressBar.imageProgressBar.fillAmount >= 1)
            Ping = true;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        preparingItem = false;
        Ping = false;
        countDown = maxCountDown;
    }
    private void FixedUpdate()
    {
        if (Ping)
        {
            int layerMask1 = (1 << 12);
            
            for (int i = 0; i < GameManager.instance.zombiesInWorld.Count; i++)
            {
                if (Vector3.Distance (GameManager.instance.zombiesInWorld[i].transform.position, transform.position) < 50)
                    GameManager.instance.zombiesInWorld[i].GetComponent<AI_Zombie>().target = transform;
            }
            fuseParticle.SetActive(true);
            countDown -= Time.deltaTime;
            if (countDown <= 0)
            {
                if (GameManager.instance.playerInv.GetCurrentItemInfo() == gameObject.GetComponent<Item>())
                    ThrowDynamite(GameManager.instance.weaponController.shootPoint.transform.forward);
                GameManager.instance.playerInv.UIUpdate();
                boomParticle.SetActive(true);
                boomParticle.transform.SetParent(null);
                //Instantiate(boomParticle, transform.position, transform.rotation);
                GameManager.instance.spawner.AddNoise(30, transform.position);
                int layerMask = (1 << 11) + (1 << 12)+(1 << 9);
                Collider[] hitColliders = Physics.OverlapSphere(transform.position, 5, layerMask);
                for (int i = 0; i < hitColliders.Length; i++)
                {
                    if (hitColliders[i].gameObject == gameObject) continue;
                    if (hitColliders[i].GetComponent<PlayerState>() != null)
                    {
                        GameManager.instance.playerState.TakeDamage(200 / Vector3.Distance(hitColliders[i].transform.position, transform.position), PlayerState.m_deburf.Confusion);
                        GameManager.instance.playerRigidbody.addAcceleration(
                            new Vector3(
                                5 * (hitColliders[i].transform.position.x - transform.position.x),
                                Random.Range(1f, 2f)*3,
                                5 * (hitColliders[i].transform.position.z - transform.position.z)), 30);
                    }
                    else if (hitColliders[i].GetComponent<AI_8Feet>() != null)
                    {
                        GameManager.instance.ai_8Feet.currentHideStack = 1;
                        GameManager.instance.ai_8Feet.currentRunStack = 1;
                        GameManager.instance.ai_8Feet.takeDamage(200 / Vector3.Distance(hitColliders[i].transform.position, transform.position), HitPoint.HitParts.Legs, false);
                    }
                    else if (hitColliders[i].GetComponent<HitPoint>() != null)
                        hitColliders[i].GetComponent<HitPoint>().giveDamage(200);
                    else if (hitColliders[i].GetComponent<ItemDynamite>() != null)
                    {
                        ItemDynamite dynamite = hitColliders[i].GetComponent<ItemDynamite>();
                        dynamite.Ping = true;
                        if (dynamite.countDown > 1)
                            dynamite.countDown = Random.Range(0.1f, 1f);
                    }
                    if (hitColliders[i].GetComponent<Rigidbody>() != null && hitColliders[i].GetComponent<Rigidbody>().useGravity)
                    {
                        hitColliders[i].GetComponent<Rigidbody>().AddForce(
                            new Vector3(
                                Random.Range(1f, 2f) * (hitColliders[i].transform.position.x - transform.position.x),
                                Random.Range(1f, 2f) * 3,
                                Random.Range(1f, 2f) * (hitColliders[i].transform.position.z - transform.position.z)) * hitColliders[i].GetComponent<Rigidbody>().mass,
                            ForceMode.Impulse);
                    }
                }
                GameManager.instance.zombieNotice(transform);
                GameObject.Destroy(gameObject);
            }
        }
    }
    public void ThrowDynamite(Vector3 dir)
    {

        if (GameManager.instance.playerInv.GetCurrentItemInfo() == gameObject.GetComponent<Item>())
            DoUse();
        GameManager.instance.weaponController.animWeapon.CrossFadeInFixedTime("HeavyMeleeSwing1", 0.01f);
        GameManager.instance.hudProgressBar.imageProgressBar.fillAmount = 0;
        totalNum--;
        itemRigdbody.useGravity = true;
        for (int i = 0; i < itemboxColliders.Length; i++)
            itemboxColliders[i].enabled = true;
        itemRigdbody.AddForce(dir * 600, ForceMode.Acceleration);
    }
}
