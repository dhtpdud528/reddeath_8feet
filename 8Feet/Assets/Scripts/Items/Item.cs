﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEditor;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Item : MonoBehaviour
{
    
    public Sprite icon;
    protected Rigidbody itemRigdbody;
    protected Collider[] itemboxColliders;
    public float MaxReadyTime = 1;
    public string itemName = "";
    public float i_Weight = 0.03f;
    public int totalNum;
    public int maxNum = 1;
    public int id;
    public Vector3 HandPosition;
    public Vector3 HandRotation;
    public float drawSpeed = 1;
    protected bool preparingItem;
    public string[] prepareAnimations;
    public string[] useAnimations;
    public int maxHit;
    public int scraps;

    public int modCost;
    public int modedCount;
    public int MaxModCount;
    public bool isModed;
    public List<string> modedList;
    public Material mat;
    public IEnumerator emissionPingPong;
    public GameObject itemParticles;
    protected GameObject particles;

    protected virtual void Awake()
    {
        
        modedCount = 0;
        if (maxHit <= 0)
            maxHit = 1;
        if (totalNum <= 0)
            totalNum = 1;
        
        itemRigdbody = GetComponent<Rigidbody>();
        itemboxColliders = GetComponents<Collider>();
        id = UnityEngine.Random.Range(0, 100000);
        if (GetComponent<Renderer>() != null)
            mat = GetComponent<Renderer>().material;
        emissionPingPong = PingPongEmission();
        if (itemName != "")
        {
            particles = Instantiate(itemParticles, transform.position, new Quaternion());
            particles.transform.SetParent(transform);
            stopEmission();
        }
    }
    protected virtual void Start()
    {
        
        if (GameManager.instance.playerProfile != null)
            MaxReadyTime /= GameManager.instance.playerProfile.itemUsageSpeed;
    }
    protected IEnumerator StartInit()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        Debug.Log(name);
        
    }
    IEnumerator PingPongEmission()
    {
        if (itemName != "")
        {
            mat.EnableKeyword("_EMISSION");
            while (true)
            {
                mat.DOColor(Color.white, "_EmissionColor", 1);
                yield return new WaitForSecondsRealtime(1f);

                mat.DOColor(Color.black, "_EmissionColor", 1);
                yield return new WaitForSecondsRealtime(2f);
            }
        }
    }
    public void startEmission()
    {
        StartCoroutine(emissionPingPong = PingPongEmission());
        particles.SetActive(true);
    }
    public void stopEmission()
    {
        if (particles != null)
            particles.SetActive(false);
        if (mat != null)
        {
            StopCoroutine(emissionPingPong);
            mat.DisableKeyword("_EMISSION");
        }

    }
    //private void Update()
    //{

    //    float emission = Mathf.PingPong(Time.time, 1.0f);
    //    Color baseColor = Color.white; //Replace this with whatever you want for your base color at emission level '1'

    //    Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

    //    mat.SetColor("_EmissionColor", finalColor);
    //}
    public virtual void PrepareItem()
    {
        if (GameManager.instance.playerInv.GetCurrentItemInfo() == gameObject.GetComponent<Item>())
            if (GameManager.instance.hudProgressBar.imageProgressBar.fillAmount < 1)
                DoPrepare();
    }

    public void DoPrepare()
    {
        GameManager.instance.hudProgressBar.imageProgressBar.fillAmount += Time.deltaTime / MaxReadyTime;
        if (preparingItem) return;
        preparingItem = true;
        if (prepareAnimations.Length > 0)
            GameManager.instance.weaponController.animWeapon.CrossFadeInFixedTime(prepareAnimations[UnityEngine.Random.Range(0, prepareAnimations.Length)], 0.01f);
    }
    public void DoUse()
    {
        GameManager.instance.hudProgressBar.imageProgressBar.fillAmount = 0;
        GameManager.instance.playerController.usingItem = false;
        preparingItem = false;
        if (useAnimations.Length > 0)
            GameManager.instance.weaponController.animWeapon.CrossFadeInFixedTime(useAnimations[UnityEngine.Random.Range(0, useAnimations.Length)], 0.01f);
    }


    public virtual void CancelUsing()
    {
        if (GameManager.instance.playerInv.GetCurrentItemInfo() == gameObject.GetComponent<Item>())
            GameManager.instance.hudProgressBar.imageProgressBar.fillAmount = 0;
        GameManager.instance.playerController.usingItem = false;
        preparingItem = false;
    }
    public virtual void DropItem_fromPlayer()
    {
        if (itemName == "") return;
        gameObject.transform.position = GameManager.instance.weaponController.shootPoint.position;
        gameObject.SetActive(true);
        itemRigdbody.freezeRotation = false;
        itemRigdbody.useGravity = true;
        for(int i = 0; i < itemboxColliders.Length;i++)
            itemboxColliders[i].enabled = true;
        itemRigdbody.AddForce(GameManager.instance.weaponController.shootPoint.forward * 200, ForceMode.Acceleration);
        startEmission();

    }
    public void DropItem()
    {
        if (itemName == "") return;
        gameObject.SetActive(true);
        itemRigdbody.freezeRotation = false;
        itemRigdbody.useGravity = true;
        for (int i = 0; i < itemboxColliders.Length; i++)
            itemboxColliders[i].enabled = true;
        startEmission();
    }
    public virtual void pickUpItem()
    {
        if (itemName == "") return;
        stopEmission();
        itemRigdbody.freezeRotation = true;
        itemRigdbody.velocity *= 0;
        itemRigdbody.useGravity = false;
        for (int i = 0; i < itemboxColliders.Length; i++)
            itemboxColliders[i].enabled = false;
    }
    protected virtual void OnDisable()
    {
        
        preparingItem = false;
    }
    public void AddTotalNum(int i)
    {
        this.totalNum += i;
        if (totalNum <= 0)
        {
            if (GameManager.instance.playerInv.SearchItem_inBackpack(GetComponent<Item>()) != null)
                GameManager.instance.playerInv.backpackInv.Remove(gameObject);
            GameObject.Destroy(gameObject);
        }

    }
}
