﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFirstAidKit : Item
{
    public bool use = false;
    public override void PrepareItem()
    {
        base.PrepareItem();
        if (GameManager.instance.playerState.down)
        {
            GameManager.instance.hudProgressBar.imageProgressBar.fillAmount = 0;
            return;
        }
        GameManager.instance.playerController.usingItem = true;
        if (GameManager.instance.hudProgressBar.imageProgressBar.fillAmount >= 1)
            use = true;
    }
    public override void CancelUsing()
    {
        base.CancelUsing();
        GameManager.instance.playerController.usingItem = false;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        GameManager.instance.playerController.usingItem = false;
        use = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (use)
        {
            if (GameManager.instance.playerInv.GetCurrentItemInfo() == gameObject.GetComponent<Item>())
                GameManager.instance.hudProgressBar.imageProgressBar.fillAmount = 0;
            totalNum--;
            SteamAchievements.bandageCount++;
            if(SteamAchievements.bandageCount >= 15)
            {
                SteamAchievements.UnlockAchievement("ACHIEVEMENT_Callus");
            }
            GameManager.instance.playerInv.UIUpdate();
            if (GameManager.instance.playerInv.GetCurrentItemInfo() != gameObject.GetComponent<Item>())
            {
                StopCoroutine(GameManager.instance.playerState.bleeding);
                GameManager.instance.playerState.bloody = false;
                GameManager.instance.playerController.usingItem = false;
                GameManager.instance.playerState.AddHealth(60);
                GameObject.Destroy(gameObject);
            }
        }
    }
}
