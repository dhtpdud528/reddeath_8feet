﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flash : MonoBehaviour
{
    
    private Light Flashlight;
    public ItemBattery battery;
    public bool on = true;
    [SerializeField] private bool infMod;
    Queue<AI_8Feet> ai_8FeetList = new Queue<AI_8Feet>();

    // Use this for initialization
    void Start()
    {
        
        Flashlight = transform.parent.GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
            if (battery != null && battery.leftPower > 0)
                on = !on;
        if (GameManager.instance.playerInv.SearchItem_inBackpack("Battery") != null)
            battery = GameManager.instance.playerInv.SearchItem_inBackpack("Battery").GetComponent<ItemBattery>();
        else
        {
            on = false;
            battery = null;
            Flashlight.enabled = false;
        }
        if (on)
        {
            if (battery == null)
            {
                on = false;
                return;
            }
            if (GameManager.instance.spawner != null)
                GameManager.instance.spawner.AddNoise(Time.deltaTime * 0.8f,
                GameManager.instance.playerInv.transform.position);

            if (battery.leftPower <= 0)
            {
                battery.leftPower = 0;
                GameManager.instance.playerInv.backpackInv.Remove(battery.gameObject);
                GameObject.Destroy(battery.gameObject);
                return;
            }
            Flashlight.enabled = true;
            if (!infMod)
                battery.leftPower -= Time.deltaTime;
        }
        else
            Flashlight.enabled = false;
    }
}
