﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ItemBox : MonoBehaviour
{
    
    public List<GameObject> BoxInv = new List<GameObject>();
    public bool ammoBox;
    public Material mat;
    public IEnumerator emissionPingPong;

    private void Awake()
    {
        
    }
    // Use this for initialization
    void Start()
    {
        
        mat = GetComponent<MeshRenderer>().material;

        StartCoroutine(initItems());
        StartCoroutine(emissionPingPong = PingPongEmission());
    }
    IEnumerator initItems()
    {
        int itemCount = Random.Range(0, 6);
        if (Random.Range(0, 100) <= 40 * GameManager.instance.difficultyProfile.itemDistribution * GameManager.instance.modsProfile.itemDistribution)
            itemCount += (int)(GameManager.instance.difficultyProfile.itemDistribution * GameManager.instance.modsProfile.itemDistribution) - 1;
        if (itemCount < 0)
            itemCount = 0;
        for (int i = 0; i < itemCount; i++)
        {
            GameObject item = Instantiate(GameManager.instance.itemPactory.items[Random.Range(0, GameManager.instance.itemPactory.items.Length)]);
            if (item.GetComponent<Item>().itemName.Equals(""))
            {
                Destroy(item);
                i--;
                continue;
            }
            if (item.GetComponent<Item>() as ItemAmmo)
            {
                int num = 0;
                switch (item.GetComponent<ItemAmmo>().ammoType)
                {
                    case ItemAmmo.AmmoTypes.MachineGun:
                        num = Random.Range(10, 100);
                        break;
                    case ItemAmmo.AmmoTypes.Rifle:
                        num = Random.Range(1, 20);
                        break;
                    case ItemAmmo.AmmoTypes.Pistol:
                        num = Random.Range(1, 20);
                        break;
                }
                item.GetComponent<Item>().totalNum = (int)(num * GameManager.instance.difficultyProfile.itemDistribution * GameManager.instance.modsProfile.itemDistribution);
                AddItem(item);
                yield return new WaitForEndOfFrame();
                continue;
            }
            else if (item.GetComponent<Item>() as ItemMelee)
            {
                if (item.GetComponent<Item>().itemName.Equals("Axe"))
                {
                    if (!(Random.Range(0, 10) < 9))
                    {
                        Destroy(item);
                        i--;
                    }
                }
                else if (item.GetComponent<Item>().itemName.Equals("Knife"))
                {
                    if (Random.Range(0, 10) < 6)
                    {
                        Destroy(item);
                        i--;
                    }
                }
                else if (item.GetComponent<Item>().itemName.Equals("Pick Axe"))
                {
                    if (!(Random.Range(0, 10) < 9))
                    {
                        Destroy(item);
                        i--;
                    }
                }
            }
            else if (item.GetComponent<Item>().itemName.Equals("Compass"))
            {
                if (Random.Range(0, 10) == 1)
                    AddItem(item);
                else
                {
                    Destroy(item);
                    i--;
                }
            }
            else if (item.GetComponent<Item>() as ItemGun)
                if (Random.Range(0, 5) == 0)
                    AddItem(item);
                else
                {
                    Destroy(item);
                    i--;
                }
            else if (ammoBox && !(item.GetComponent<Item>() as ItemDynamite))
            {
                Destroy(item);
                i--;
            }
            else
                AddItem(item);
            yield return new WaitForEndOfFrame();
        }
    }
    public void AddItem(GameObject item)
    {
        if (item.GetComponent<Item>() is ItemAmmo)
        {
            ItemAmmo searchedAmmo = searchAmmo(item.GetComponent<ItemAmmo>().ammoType);
            if (searchedAmmo != null)
            {
                searchedAmmo.totalNum += item.GetComponent<ItemAmmo>().totalNum;
                Destroy(item);
            }
            else
            {
                item.GetComponent<Item>().pickUpItem();
                BoxInv.Add(item);
            }
        }
        else
        {
            item.transform.SetParent(transform);
            BoxInv.Add(item);
        }
        

    }
    public void sortInv()
    {
        if(BoxInv.Count > 1)
        {
            ItemAmmo searchedAmmo;
            for (int i=0; i < BoxInv.Count;i++)
            {
                if(BoxInv[i].GetComponent<Item>() is ItemAmmo)
                {
                }
            }
        }
    }
    public ItemAmmo searchAmmo(ItemAmmo.AmmoTypes search)
    {
        GameObject find = null;
        if (BoxInv.Count == 0)
            return null;
        for (int i = 0; i < BoxInv.Count; i++)
        {
            if (BoxInv[i] != null && BoxInv[i].GetComponent<Item>() is ItemAmmo && BoxInv[i].GetComponent<ItemAmmo>().ammoType == search)
            {
                find = BoxInv[i];
                break;
            }
        }
        if (find != null)
            return find.GetComponent<ItemAmmo>();
        else
            return null;
    }
    IEnumerator PingPongEmission()
    {
        mat.EnableKeyword("_EMISSION");
        while (true)
        {
            mat.DOColor(new Color(0.6f, 0.6f, 0.6f), "_EmissionColor", 1);
            yield return new WaitForSecondsRealtime(1f);
            mat.DOColor(Color.black, "_EmissionColor", 1);
            yield return new WaitForSecondsRealtime(2f);
        }
    }
}
