﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemList : MonoBehaviour
{
    public GameObject[] items;
    public GameObject FindItem(string name)
    {
        for(int i= 0; i<items.Length;i++)
        {
            if (name == items[i].GetComponent<Item>().itemName)
                return items[i];
        }
        return null;
    }
    public GameObject FindRandomItems()
    {
        return items[Random.Range(0, items.Length)];
    }
}
