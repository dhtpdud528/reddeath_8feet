﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeHitBox : MonoBehaviour
{
    
    public float damage;
    public int maxStack;
    public int currentStack;
    public List<int> ids = new List<int>();
    // Start is called before the first frame update
    private void Awake()
    {
        
    }
    void Start()
    {
        
        for (int i = 0; i < GetComponents<BoxCollider>().Length; i++)
            GetComponents<BoxCollider>()[i].enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (currentStack <= maxStack && other.GetComponent<HitPoint>() != null && !MultiHitCheck(other.GetComponent<HitPoint>().bodyOperator.id))
        {
            Item item = GameManager.instance.playerInv.GetCurrentItemInfo();
            if (item as ItemMelee)
                ((ItemMelee)item).PlayRandomAudio(((ItemMelee)item).hitSounds);
            if (GameManager.instance.weaponController.isMeleeAttacking && other.GetComponent<HitPoint>().bodyOperator.GetComponent<AI_Zombie>() != null)
            {
                other.GetComponent<HitPoint>().bodyOperator.GetComponent<AI_Zombie>().DoKnockBack();
                SteamAchievements.pushCount++;
                if(SteamAchievements.pushCount >= 50)
                    SteamAchievements.UnlockAchievement("ACHIEVEMENT_Violent");
            }
            other.GetComponent<HitPoint>().giveDamage(damage * GameManager.instance.modsProfile.meleeDamage, true);
            ids.Add(other.GetComponent<HitPoint>().bodyOperator.id);
            currentStack++;
            if (other.GetComponent<Rigidbody>() != null)
                other.GetComponent<Rigidbody>().AddForce(transform.forward * 500, ForceMode.Acceleration);
        }

    }
    private bool MultiHitCheck(int id)
    {
        bool found = false;
        for (int i = 0; i < ids.Count; i++)
        {
            if (ids[i] == id)
            {
                found = true;
                break;
            }
        }
        return found;
    }
}
