﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class EntityBullet : Entity
{
    private Collision collided;
    public ItemGun gun;
    [SerializeField] private GameObject rockBulletImpact;
    [SerializeField] private GameObject bodyBulletImpact;
    private Vector3 tmpDirection;
    private float lifeTime;

    // Use this for initialization
    void Start()
    {
        //GetComponent<Rigidbody>().mass += gun.damage/4;
    }

    // Update is called once per frame
    void Update()
    {
        lifeTime += Time.deltaTime;
        if (lifeTime >= 5)
            transform.parent.GetComponent<DestroyerTrigger>().DoDestroy();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collided != null && collision == collided) return;
        collided = collision;
        GameObject bulletImpact = rockBulletImpact;
        ItemDynamite dynamite = collision.gameObject.GetComponent<ItemDynamite>() != null ? collision.gameObject.GetComponent<ItemDynamite>() : null;
        if (collision.collider.CompareTag("Monster"))
        {
            bulletImpact = bodyBulletImpact;
            if (collision.collider.GetComponent<HitPoint>()&& collision.collider.GetComponent<HitPoint>().bodyOperator.hurtTime <= 0)
            {
                collision.collider.GetComponent<HitPoint>().giveDamage(gun.damage);
                collision.collider.GetComponent<HitPoint>().bodyOperator.hurtTime = 0.02f;
            }
        }
        else if (dynamite != null && dynamite.Ping)
            dynamite.countDown = 0;
        lifeTime += 1/gun.bulletPenetration;
        GameObject bulletHole = Instantiate(bulletImpact, collision.GetContact(0).point, Quaternion.FromToRotation(Vector3.forward, collision.GetContact(0).normal));
        bulletHole.transform.SetParent(collision.transform);
    }
    public void OnTriggerStay(Collider other)
    {
        ItemDynamite dynamite = other.gameObject.GetComponent<ItemDynamite>() != null ? other.gameObject.GetComponent<ItemDynamite>() : null;
        if (dynamite != null && dynamite.Ping)
        {
            other.GetComponent<ItemDynamite>().countDown = 0;
            transform.parent.GetComponent<DestroyerTrigger>().DoDestroy();
        }
    }
}
