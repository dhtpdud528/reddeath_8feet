﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PainState : StateMachineBehaviour
{

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<AI_8Feet>().painStack--;
        if (animator.GetComponent<AI_8Feet>().painStack <= 0)
        {
            animator.GetComponent<AI_8Feet>().painStack = 2;
            if(animator.GetComponent<AI_8Feet>().ai_state != AI_8Feet.STATE.Run)
                animator.GetComponent<AI_8Feet>().health = animator.GetComponent<AI_8Feet>().maxHealth;
            animator.GetComponent<AI_8Feet>().currentHideStack--;

            if (animator.GetComponent<AI_8Feet>().currentHideStack <= 0)
                animator.GetComponent<AI_8Feet>().ai_state = AI_8Feet.STATE.Run;
        }

    }
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetComponent<AI_8Feet>().painStack > 0)
        {
            if (animator.GetComponent<AI_8Feet>().ai_state != AI_8Feet.STATE.Run)
                animator.GetComponent<AI_8Feet>().health = animator.GetComponent<AI_8Feet>().maxHealth;
            if (!animator.GetComponent<AI_8Feet>().isPaining())
                animator.GetComponent<AI_8Feet>().currentHideStack--;

            if (animator.GetComponent<AI_8Feet>().currentHideStack <= 0)
                animator.GetComponent<AI_8Feet>().ai_state = AI_8Feet.STATE.Run;
        }
        else
            animator.GetComponent<AI_8Feet>().painStack = 2;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
