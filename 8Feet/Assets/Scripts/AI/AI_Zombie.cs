using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using DG.Tweening;

public enum ZombieType
{
    Captin,
    Normal,
    NonArmored
}

public class AI_Zombie : AI_Monster
{
    public Rigidbody[] bodyParts;
    public AudioClip[] rageSounds;
    //public AudioClip[] rushSounds;
    public AudioClip[] dieSounds;
    

    public ZombieType zombieType;
    [SerializeField] private GameObject armor;
    [SerializeField] private GameObject helmet;
    [SerializeField] private GameObject skirt;
    public Vector3 destinationPos;
    public float checkRate = 0.4f;
    public float lookRadius = 10f;

    public Transform target;
    private float charAngle;
    private float direction = 0f;
    private AnimatorStateInfo stateInfo;
    private AnimatorTransitionInfo transInfo;
    private float wanderRange = 10f;
    private NavMeshHit navHit;
    private Vector3 wanderTarget;
    private Transform t;
    private float nextCheck = 0f;
    private float timer = 0f;
    private float time = 7f;
    [SerializeField] private Material[] materials;
    [SerializeField] private Material dissolveMaterial;
    public bool isAttacking;
    public float attackRange;
    public bool isKnockBacking;

    public GameObject[] Eyes;
    IEnumerator playLivingSoundCoroutine;

    public override void Awake()
    {
        base.Awake();
        t = GetComponent<Transform>();
        StartCoroutine(playLivingSoundCoroutine = PlayLivingSound());
    }
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        ZombieAttackState[] attackList = anim.GetBehaviours<ZombieAttackState>();
        foreach (ZombieAttackState i in attackList)
            i.zombie = GetComponent<AI_Zombie>();
        if (transform.GetChild(0).name == "Modeling")
            transform.GetChild(0).GetChild(0).GetComponent<SkinnedMeshRenderer>().material = materials[Random.Range(0, materials.Length)];
        else
            transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material = materials[Random.Range(0, materials.Length)];
        if (armor != null)
            if (zombieType == ZombieType.NonArmored)
                armor.SetActive(false);
            else if (Random.Range(0, 5) == 0)
                armor.SetActive(true);
            else
                armor.SetActive(false);
        if (helmet != null)
            if (Random.Range(0, 5) == 0)
                helmet.SetActive(true);
            else
                helmet.SetActive(false);
        float tall = Random.Range(-0.05f, 0.1f);
        transform.localScale = new Vector3(transform.localScale.x + tall, transform.localScale.y + tall, transform.localScale.z + tall);
        GameManager.instance.zombiesInWorld.Add(this);
    }
    // Update is called once per frame
    public void Update()
    {
        stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        transInfo = anim.GetAnimatorTransitionInfo(0);

        if (isKnockBacking)
        {
            agent.speed = -7;
            return;
        }
        if (target != null && agent.isOnNavMesh)
        {
            destinationPos = target.transform.position;
            agent.SetDestination(target.position);
            agent.speed = 7 * GameManager.instance.modsProfile.zombierMoveSpeed / (inWater ? 3 : 1);

            float distanceToTarget = Vector3.Distance(target.transform.position, transform.position);
            if (distanceToTarget <= attackRange && !isKnockBacking)
                DoAttack();
        }
        if (destinationPos != null && agent.isOnNavMesh)
            agent.SetDestination(destinationPos);

        if (Time.time > nextCheck)
        {
            nextCheck = Time.time + checkRate;
            CheckIfIShouldWander();
        }
        float distanceToPlayer = Vector3.Distance(GameManager.instance.playerState.transform.position, transform.position);
        if (distanceToPlayer <= lookRadius)
        {
            if (target == null) target = GameManager.instance.playerState.transform;
        }
        else if (distanceToPlayer > lookRadius)
        {
            if (target != null)
                if (GameManager.instance.flash.on)
                {
                    if (timer < time)
                    {
                        timer += Time.deltaTime;
                        if (agent.isOnNavMesh)
                            agent.SetDestination(target.position);
                    }
                    if (timer >= time)
                    {
                        timer = 0;
                        target = null;
                        agent.speed = 5 * GameManager.instance.modsProfile.zombierMoveSpeed / (inWater ? 3 : 1);
                    }
                }
                else
                {
                    target = null;
                    agent.speed = 5 * GameManager.instance.modsProfile.zombierMoveSpeed / (inWater ? 3 : 1);
                }
        }
    }
    IEnumerator Dying()
    {
        for (int i = 0; i < bodyParts.Length; i++)
        {
            bodyParts[i].isKinematic = false;
            bodyParts[i].useGravity = true;
        }
        StartCoroutine(DropItemRandom());
        SteamAchievements.killCount_Total++;
        if (SteamAchievements.killCount_Total >= 100)
            SteamAchievements.UnlockAchievement("ACHIEVEMENT_Runner");
        if(Eyes.Length > 0)
        {
            Destroy(Eyes[0]);
            Destroy(Eyes[1]);
        }
        yield return new WaitForSeconds(10);
        if (transform.GetChild(0).name == "Modeling")
        {
            transform.GetChild(0).GetChild(0).GetComponent<SkinnedMeshRenderer>().material.DOColor(Color.black, 5);
            transform.GetChild(0).GetChild(1).GetComponent<SkinnedMeshRenderer>().material.DOColor(Color.black, 5);
            skirt.GetComponent<SkinnedMeshRenderer>().material.DOColor(Color.black, 5);
        }
        else
            transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material.DOColor(Color.black, 5);
        yield return new WaitForSeconds(5);
        if (transform.GetChild(0).name == "Modeling")
        {
            transform.GetChild(0).GetChild(0).GetComponent<SkinnedMeshRenderer>().material = dissolveMaterial;
            transform.GetChild(0).GetChild(1).GetComponent<SkinnedMeshRenderer>().material = dissolveMaterial;
            skirt.GetComponent<SkinnedMeshRenderer>().material = dissolveMaterial;

            transform.GetChild(0).GetChild(0).GetComponent<SkinnedMeshRenderer>().material.DOFloat(0, "_Progress", 5);
            transform.GetChild(0).GetChild(1).GetComponent<SkinnedMeshRenderer>().material.DOFloat(0, "_Progress", 5);
            skirt.GetComponent<SkinnedMeshRenderer>().material.DOFloat(0, "_Progress", 5);
        }
        else
        {
            transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material = dissolveMaterial;
            transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().material.DOFloat(0, "_Progress", 5);
        }
        yield return new WaitForSeconds(Random.Range(4f,5f));
        GameObject.Destroy(gameObject);
    }
    private void FixedUpdate()
    {
        base.FixedUpdate();
        if (health > 0)
        {
            AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);

            /*for (int i = 0; i < 3; i++)
                if (info.IsName("Attack" + i))
                {
                    isAttacking = true;
                    break;
                }*/
            if (info.IsName("Dodging Back"))
                isKnockBacking = true;
            float rootSpeed = 0;
            Vector3 lookrotation = agent.steeringTarget - transform.position;
            if (IsInPivot())
                rootSpeed = 10;
            if (lookrotation != Vector3.zero)
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lookrotation), rootSpeed * Time.deltaTime);
            charAngle = 0f;
            direction = 0f;

            // Translate controls stick coordinates into world/cam/character space
            StickToWorldspace(this.transform, ref direction, ref charAngle, IsInPivot());
            anim.SetFloat("Speed", agent.velocity.magnitude / 10 * 2, 0.05f, Time.deltaTime);
            anim.SetFloat("Direction", direction, 0.25f, Time.deltaTime);

            if (agent.velocity.magnitude > LocomotionThreshold)    // Dead zone
                if (!IsInPivot())
                    anim.SetFloat("Angle", charAngle);
            if (agent.velocity.magnitude < LocomotionThreshold)    // Dead zone
            {
                anim.SetFloat("Direction", 0f);
                anim.SetFloat("Angle", 0f);
            }
        }
    }
    private void DoAttack()
    {
        if (isAttacking)
            return;
        anim.CrossFadeInFixedTime("UpperBody.Attack" + Random.Range(1, 7), 0.1f);
        agent.velocity /= 2;
    }
    public void DoKnockBack()
    {
        if (isKnockBacking || GameManager.instance.modsProfile != null && GameManager.instance.modsProfile.immunityPushed)
            return;
        anim.CrossFadeInFixedTime("Dodging Back", 0.01f);
        anim.CrossFadeInFixedTime("UpperBody.Dodging Back", 0.01f);
    }
    public void CheckIfIShouldWander()
    {
        if (target == null)
            if (RandomWanderTarget(t.position, wanderRange, out wanderTarget) && agent.isOnNavMesh)
                agent.SetDestination(wanderTarget);
    }

    bool RandomWanderTarget(Vector3 centre, float range, out Vector3 result)
    {
        Vector3 randomPoint = centre + Random.insideUnitSphere * wanderRange;
        if (NavMesh.SamplePosition(randomPoint, out navHit, 1f, NavMesh.AllAreas))
        {
            result = navHit.position;
            return true;
        }
        else
        {
            result = centre;
            return false;
        }
    }

    public void Spawn()
    {
        Vector3 spawnPos = GameManager.instance.playerState.gameObject.transform.position;
        spawnPos.x += UnityEngine.Random.Range(-40f, 40f);
        spawnPos.z += UnityEngine.Random.Range(-40f, 40f);
        while (Vector3.Distance(spawnPos, GameManager.instance.playerState.gameObject.transform.position) < 20)
        {
            spawnPos.x += UnityEngine.Random.Range(-40f, 40f);
            spawnPos.z += UnityEngine.Random.Range(-40f, 40f);

            new WaitForSeconds(2f);
        }
    }

    public override void takeDamage(float damage, HitPoint.HitParts hitPart, bool isMelee)
    {

        base.takeDamage(damage, hitPart, isMelee);
        //When Dead
        if (health <= 0 && anim.enabled)
        {
            //Debug.Break();
            GameManager.instance.zombiesInWorld.Remove(this);
            StopCoroutine(playLivingSoundCoroutine);
            if (isMelee)
            {
                SteamAchievements.killCount_Melee++;
                if(SteamAchievements.killCount_Melee > 50)
                    SteamAchievements.UnlockAchievement("ACHIEVEMENT_Steroid");
            }
            m_AudioSource.PlayOneShot(dieSounds[Random.Range(0,dieSounds.Length)]);
            StartCoroutine(Dying());
            if (skirt != null)
                skirt.GetComponent<Cloth>().damping = 1;
            
            anim.enabled = false;
            agent.enabled = false;
        }
    }
    IEnumerator DropItemRandom()
    {
        yield return new WaitForSeconds(
            Random.Range(0f, 0.1f)
            );
        int itemCount = 0;
        if (Random.Range(0, 10) >= 6)
            itemCount = Random.Range(1, 3);
        if (armor != null && armor.active)
            itemCount += (int)(Random.Range(1, 5) * GameManager.instance.difficultyProfile.itemDistribution * GameManager.instance.modsProfile.itemDistribution);
        if (itemCount < 0)
            itemCount = 0;

        for (int i = 0; i < itemCount; i++)
        {
            yield return new WaitForSeconds(
                Random.Range(0f, 0.1f)
                );
            GameObject item = GameManager.instance.itemPactory.FindRandomItems();

            if (item.GetComponent<Item>().itemName.Equals(""))
            {
                i--;
                continue;
            }
            if (item.GetComponent<Item>() as ItemAmmo)
            {
                item = Instantiate(item, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), new Quaternion(0, 0, 0, 1));
                switch (item.GetComponent<ItemAmmo>().ammoType)
                {
                    case ItemAmmo.AmmoTypes.MachineGun:
                        item.GetComponent<Item>().totalNum = Random.Range(10, 51);
                        break;
                    case ItemAmmo.AmmoTypes.Rifle:
                        item.GetComponent<Item>().totalNum = Random.Range(1, 21);
                        break;
                    case ItemAmmo.AmmoTypes.Pistol:
                        item.GetComponent<Item>().totalNum = Random.Range(1, 21);
                        break;
                }
                item.GetComponent<Item>().totalNum = (int)(item.GetComponent<Item>().totalNum * GameManager.instance.difficultyProfile.itemDistribution * GameManager.instance.modsProfile.itemDistribution);
            }
            else if (item.GetComponent<Item>() as ItemGun)
            {
                if (zombieType != ZombieType.Captin || !item.GetComponent<Item>().itemName.Equals("mauiCP96") || Random.Range(0, 2) == 0)
                {
                    i--;
                    continue;
                }
                else
                    item = Instantiate(item, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), new Quaternion(0, 0, 0, 1));
            }
            else if (item.GetComponent<Item>() as ItemMelee)
            {
                if (item.GetComponent<Item>().itemName.Equals("Axe"))
                {
                    if (zombieType != ZombieType.NonArmored || Random.Range(0, 10) < 8)
                    {
                        i--;
                        continue;
                    }
                    else
                        item = Instantiate(item, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), new Quaternion(0, 0, 0, 1));
                }
                else if (item.GetComponent<Item>().itemName.Equals("Knife"))
                {
                    if (zombieType == ZombieType.NonArmored || Random.Range(0, 11) < 6)
                    {
                        i--;
                        continue;
                    }
                    else
                        item = Instantiate(item, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), new Quaternion(0, 0, 0, 1));
                }
                else if (item.GetComponent<Item>().itemName.Equals("PickAxe"))
                {
                    if (zombieType != ZombieType.NonArmored || Random.Range(0, 10) < 8)
                    {
                        i--;
                        continue;
                    }
                    else
                        item = Instantiate(item, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), new Quaternion(0, 0, 0, 1));
                }
            }
            else if (item.GetComponent<Item>() as ItemDynamite)
            {
                if (zombieType == ZombieType.NonArmored || Random.Range(0, 10) < 6)
                {
                    i--;
                    continue;
                }
                else
                    item = Instantiate(item, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), new Quaternion(0, 0, 0, 1));
            }
            else if (item.GetComponent<Item>().itemName.Equals("Compass"))
            {
                if (zombieType != ZombieType.Captin || Random.Range(0, 5) == 0)
                {
                    i--;
                    continue;
                }
                else
                    item = Instantiate(item, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), new Quaternion(0, 0, 0, 1));
            }
            else//�� �����۵��� �ƴѰ��
            {
                item = Instantiate(item, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), new Quaternion(0, 0, 0, 1));
                item.GetComponent<Item>().totalNum = (int)(item.GetComponent<Item>().totalNum * GameManager.instance.difficultyProfile.itemDistribution * GameManager.instance.modsProfile.itemDistribution);
            }
            item.GetComponent<Item>().DropItem();
        }
    }
    void OnAnimatorIK(int layerIndex)
    {
        if (anim != null && anim.enabled && health > 0)
            if (target != null)
            {
                anim.SetLookAtWeight(0.7f);
                if (target.GetComponent<PlayerState>() == null)
                    anim.SetLookAtPosition(target.transform.position);
                else
                    anim.SetLookAtPosition(target.transform.GetChild(0).position);
            }
    }

    public float LocomotionThreshold { get { return 0.2f; } }
    public bool IsInPivot()
    {
        return stateInfo.IsName("loco.LocomotionPivotL") ||
            stateInfo.IsName("loco.LocomotionPivotR") ||
            transInfo.IsName("loco.Locomotion -> loco.LocomotionPivotL") ||
            transInfo.IsName("loco.Locomotion -> loco.LocomotionPivotR");
    }
    public void StickToWorldspace(Transform root, ref float directionOut, ref float angleOut, bool isPivoting)
    {
        Vector3 rootDirection = root.forward;
        Vector3 stickDirection = new Vector3(destinationPos.x, 0, destinationPos.z) - new Vector3(transform.position.x, 0, transform.position.z);
        // Convert joystick input in Worldspace coordinates
        Vector3 moveDirection = stickDirection;
        Vector3 axisSign = Vector3.Cross(moveDirection, rootDirection);
        float angleRootToMove = Vector3.Angle(rootDirection, moveDirection) * (axisSign.y >= 0 ? -1f : 1f);
        if (!isPivoting)
        {
            angleOut = angleRootToMove;
        }
        angleRootToMove /= 180f;

        directionOut = angleRootToMove * 1.5f;
    }
    IEnumerator PlayLivingSound()
    {
        while (health > 0)
        {
            if (target == null)
            {
                yield return new WaitForSeconds(1f);
                continue;
            }
            PlayRandomAudio(rageSounds);
            yield return new WaitForSeconds(Random.Range(3f, 5f));
        }

    }
}