﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockBackState : StateMachineBehaviour
{

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<AI_Zombie>().isKnockBacking = true;
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<AI_Zombie>().isAttacking = false;
        if(stateInfo.normalizedTime>0.9)
            animator.GetComponent<AI_Zombie>().isKnockBacking = false;
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<AI_Zombie>().isKnockBacking = false;
    }
}
