﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Spawner : MonoBehaviour
{
    
    [SerializeField] private GameObject[] zombies;
    [SerializeField] private float _8FeetSpawnTime = 1;
    [SerializeField] private float noise;
    private int spawnStack;
    [SerializeField] private float zombieSpawnValue;

    public bool canSpawn;
    private bool spawning;

    // Use this for initialization
    void Start()
    {
        
        zombieSpawnValue *= GameManager.instance.difficultyProfile.noiseLimit;
        zombieSpawnValue *= GameManager.instance.modsProfile.noiseLimit;
        StartCoroutine(CheckNoiseInterval());
    }

    // Update is called once per frame
    void Update()
    {
        if (!canSpawn) return;
        if (GameManager.instance.ai_8Feet!= null)
        {
            if (!GameManager.instance.ai_8Feet.gameObject.active && !spawning)
                StartCoroutine(spawn8Feet());
        }
    }
    IEnumerator spawn8Feet()
    {
        spawning = true;
        yield return new WaitForSeconds(Random.Range(_8FeetSpawnTime - 20, _8FeetSpawnTime + 10));
        GameManager.instance.ai_8Feet.gameObject.SetActive(true);
        GameManager.instance.ai_8Feet.transform.position = new Vector3(0, -100, 0);
        GameManager.instance.ai_8Feet.GetComponent<AI_8Feet>().ai_state = AI_8Feet.STATE.Stalk;
        GameManager.instance.ai_8Feet.GetComponent<AI_8Feet>().Spawn();
        spawning = false;
    }
    IEnumerator CheckNoiseInterval()
    {
        while(true)
        {
            if (spawnStack > 5)
                SteamAchievements.UnlockAchievement("ACHIEVEMENT_Silent_night");
            spawnStack --;
            yield return new WaitForSeconds(2f);
        }
    }


    public void AddNoise(float addValue, Vector3 sourcePosition)
    {
        StartCoroutine(NoiseCalculater(addValue, sourcePosition));
    }
    IEnumerator NoiseCalculater(float addValue, Vector3 sourcePosition)
    {
        if (canSpawn)
        {
            noise += addValue;
            while (zombieSpawnValue > 1 && noise >= zombieSpawnValue)
            {
                spawnStack++;
                Vector3 spawnPos;
                RaycastHit hit;
                do
                {
                    spawnPos = GameManager.instance.playerState.transform.position;
                    spawnPos.x += Random.Range(-100, 100);
                    spawnPos.z += Random.Range(-100, 100);
                    spawnPos.y += 9999;
                    if (Physics.Raycast(new Vector3(spawnPos.x, spawnPos.y, spawnPos.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14) + (1 << 4)) && !hit.collider.CompareTag("Water"))
                        spawnPos.y = hit.point.y;
                    else
                        continue;
                }
                while (Vector3.Distance(spawnPos, GameManager.instance.playerState.transform.position) < 50 || spawnPos.y >= 9999);


                GameObject spawnedZombie;
                int i = Random.Range(0, 100);
                if (0 <= i && i < 50)
                    spawnedZombie = Instantiate(zombies[0], spawnPos, new Quaternion(0, 0, 0, 1));
                else if (50 <= i && i < 80)
                    spawnedZombie = Instantiate(zombies[1], spawnPos, new Quaternion(0, 0, 0, 1));
                else if (80 <= i && i < 90)
                    spawnedZombie = Instantiate(zombies[2], spawnPos, new Quaternion(0, 0, 0, 1));
                else if (Random.Range(0, 2) == 0)
                    spawnedZombie = Instantiate(zombies[3], spawnPos, new Quaternion(0, 0, 0, 1));
                else
                    spawnedZombie = Instantiate(zombies[4], spawnPos, new Quaternion(0, 0, 0, 1));
                noise -= zombieSpawnValue;
                if (spawnedZombie.GetComponent<NavMeshAgent>().isOnNavMesh)
                    spawnedZombie.GetComponent<NavMeshAgent>().SetDestination(sourcePosition);
                //spawnedZombie.GetComponent<AI_Zombie>().status = Status.anger;
                spawnedZombie.GetComponent<AI_Zombie>().destinationPos = sourcePosition;
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}
