﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieAttackState : StateMachineBehaviour
{
    
    [SerializeField]private float attackStartTime = 0.7f;
    [SerializeField] private float attackEndTime;
    private bool attackSucceeded = false;
    public AI_Zombie zombie;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        if (zombie != null)
        zombie.isAttacking = true;
        attackSucceeded = false;
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (zombie != null)
            if (attackStartTime <= stateInfo.normalizedTime && stateInfo.normalizedTime < attackEndTime&&
            Vector3.Distance(zombie.transform.position, GameManager.instance.playerState.transform.position) < zombie.attackRange &&
            !attackSucceeded)
        {
            attackSucceeded = true;
                if (GameManager.instance.difficultyProfile != null)
                {
                    if ((GameManager.instance.modsProfile != null && !GameManager.instance.modsProfile.immunityBleeding) 
                        && Random.Range(0, 10) == 1
                        )
                        GameManager.instance.playerState.TakeDamage(0 * GameManager.instance.difficultyProfile.enermyDamage, PlayerState.m_deburf.Confusion);
                    if ((GameManager.instance.modsProfile != null && !GameManager.instance.modsProfile.immunityConfusion) && Random.Range(0, 10) == 1)
                        GameManager.instance.playerState.TakeDamage(0 * GameManager.instance.difficultyProfile.enermyDamage, PlayerState.m_deburf.Bloody);
                    GameManager.instance.playerState.TakeDamage(2 * GameManager.instance.difficultyProfile.enermyDamage, PlayerState.m_deburf.Slow);
                }
                else
                {
                    GameManager.instance.playerState.TakeDamage(2, PlayerState.m_deburf.Slow);
                }

        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (zombie != null)
            zombie.isAttacking = false;
        attackSucceeded = false;
    }
}
