﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;



public class AI_8Feet : AI_Monster
{

    public bool TestMode;

    public int maxHideStack;
    public int currentHideStack;

    public int maxRunStack = 2;
    public int currentRunStack = 2;
    public int painStack = 2;

    [SerializeField] private float stalkSpeed;
    [SerializeField] private float rushSpeed;
    [SerializeField] private float runSpeed;
    [SerializeField] private float PainingSpeed;
    public float rushRange;

    public enum STATE { Run, Rush, Stalk };
    public STATE ai_state;
    //private Collider coverOBJ;
    [SerializeField] private GameObject target;
    [SerializeField] private Vector3 runWayPos;


    [SerializeField] private AudioClip[] warningSounds;
    [SerializeField] private AudioClip[] painSounds;
    [SerializeField] private AudioClip[] screamSounds;
    [SerializeField] private AudioClip[] rushSounds;
    private AudioSource audioSource;

    private AnimatorStateInfo stateInfo;
    private AnimatorTransitionInfo transInfo;

    private float direction = 0f;
    private float charAngle = 0f;
    private int slideTick = 0;
    private float hideTime = 0;
    public GameObject Head;
    private int screamTick = 0;

    public GameObject dead;
    private float reSpawnTime;
    public bool beSeen;
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        audioSource = GetComponent<AudioSource>();
        GameManager.instance.spawner.canSpawn = true;
        //ai_state = STATE.Stalk;
        rushSpeed *= GameManager.instance.difficultyProfile.speedRush;
        PainingSpeed *= GameManager.instance.difficultyProfile.speedPain;
    }


    // Update is called once per frame
    public void Update()
    {
        if (TestMode)
        {
            stalkSpeed = rushSpeed = runSpeed = PainingSpeed = 0;
            anim.Play("UpperBody.Crying");
            anim.Play("Crying");
            return;
        }
        float rootSpeed = 0;
        stateInfo = anim.GetCurrentAnimatorStateInfo(0);
        transInfo = anim.GetAnimatorTransitionInfo(0);
        if (!anim.enabled)
        {
            if (!GameManager.instance.flash.on)
                beSeen = false;
        }
        else if (!isFaintAway())
        {
            if (!TestMode && !isScreaming())
            {
                switch(ai_state)
                {
                    case STATE.Stalk:
                        reSpawnTime += Time.deltaTime;
                        if (reSpawnTime > 5)
                        {
                            if (!beSeen)
                                ReSpawn();
                            reSpawnTime = 0;
                        }
                        break;
                    default:
                        if(agent.velocity.magnitude <= 0.05f)
                        {
                            reSpawnTime += Time.deltaTime;
                            if (reSpawnTime > 3)
                            {
                                if (!beSeen)
                                    ReSpawn();
                                else
                                {
                                    ai_state = STATE.Run;
                                    FindRunWay();
                                }
                                reSpawnTime = 0;
                            }
                        }
                        break;
                }
                
            }
            else
                reSpawnTime = 0;
            if (ai_state == STATE.Run)
            {
                anim.SetFloat("RunAniSpeed", 1.2f);
                target = null;
                agent.speed = runSpeed;
                agent.acceleration = runSpeed;
                if (runWayPos == Vector3.zero)
                    FindRunWay();
                else
                    agent.SetDestination(runWayPos);
                if (!beSeen)
                {
                    hideTime += Time.deltaTime;
                    if (hideTime > UnityEngine.Random.Range(5, 8))
                    {
                        currentRunStack--;
                        if (currentRunStack <= 0)
                        {
                            GameManager.instance.playerState.EndChase();
                            gameObject.SetActive(false);
                            return;
                        }
                        if (isCrawlling())
                            anim.CrossFadeInFixedTime("StandUP", 0);
                        currentHideStack = maxHideStack;
                        health = maxHealth;
                        ai_state = STATE.Rush;
                        hideTime = 0;
                        if (rushSounds.Length > 0)
                            audioSource.PlayOneShot(rushSounds[UnityEngine.Random.RandomRange(0, rushSounds.Length)]);
                    }
                }
                if (Vector3.Distance(runWayPos, transform.position) < 4)
                {
                    if (GameManager.instance.playerState != null && agent.velocity.magnitude < 1f)
                    {
                        RaycastHit hit;
                        anim.SetFloat("RunAniSpeed", anim.GetFloat("Speed") / 2);
                        if (isPaining() || beSeen)
                            FindRunWay();
                        else if (!beSeen && currentRunStack <= 0)
                            gameObject.SetActive(false);
                    }
                    if (isStand() && slideTick == 0)
                    {
                        slideTick++;
                        anim.CrossFadeInFixedTime("slide", 0);
                    }
                }
                /*
                if (coverOBJ != null)
                {
                    Transform hidePoint = coverOBJ.GetComponent<coverOBJ>().point;
                    target = hidePoint.gameObject;
                    agent.speed = runSpeed;
                    agent.acceleration = runSpeed;
                    if (Vector3.Distance(new Vector3(target.transform.position.x, 0, target.transform.position.z), transform.position) < 4)
                    {
                        if (OverMind.playerState != null && agent.velocity.magnitude < 1f)
                        {
                            RaycastHit hit;
                            anim.SetFloat("RunAniSpeed", anim.GetFloat("Speed") / 2);
                            if (isPaining())
                                StartCoroutine(coroutine);
                            if (Physics.Raycast(new Vector3(OverMind.playerState.transform.position.x, OverMind.playerState.transform.position.y + 0.2f, OverMind.playerState.transform.position.z),
                                transform.position - new Vector3(OverMind.playerState.transform.position.x, OverMind.playerState.transform.position.y + 0.2f, OverMind.playerState.transform.position.z),
                                out hit, Mathf.Infinity, ~((1 << 2) + (1 << 9) + (1 << 11))))
                            {
                                if (hit.collider.gameObject.tag == "8Feet")
                                    StartCoroutine(coroutine);
                                else if (currentRunStack <= 0)
                                    gameObject.SetActive(false);
                            }
                        }
                        if (isStand() && slideTick == 0)
                        {
                            slideTick++;
                            anim.CrossFadeInFixedTime("slide", 0);
                        }
                        hideTime += Time.deltaTime;
                        if (hideTime > UnityEngine.Random.Range(5, 8))
                        {
                            currentRunStack--;
                            anim.CrossFadeInFixedTime("StandUP", 0);
                            currentHideStack = maxHideStack;
                            ai_state = STATE.Rush;
                            hideTime = 0;
                        }
                    }
                    else
                        anim.SetFloat("RunAniSpeed", 1.5f);
                }
                else
                {
                    coroutine = FindClosestCover();
                    StartCoroutine(coroutine);
                }*/

            }
            else if (ai_state == STATE.Rush)
            {
                if(GameManager.instance.playerState.down)
                    anim.SetBool("PlayerDown", true);
                runWayPos = Vector3.zero;

                //coverOBJ = null;
                slideTick = 0;
                target = GameManager.instance.playerState.gameObject;
                agent.speed = rushSpeed;
                agent.acceleration = rushSpeed;
                anim.SetFloat("RunAniSpeed", 1f);
                if (Vector3.Distance(target.transform.position, transform.position) < 4f)
                {
                    rootSpeed = 10;
                    agent.acceleration = rushSpeed * 3;
                }
                if (!isPaining() && !isScreaming() && !GameManager.instance.playerState.isPain && Vector3.Distance(target.transform.position, transform.position) < 1f)
                {
                    if (!GameManager.instance.modsProfile.withstand)
                    {
                        GameManager.instance.playerState.Dead_8feet();
                        return;
                    }
                    GameManager.instance.playerRigidbody.addAcceleration(transform.up, 10);
                    GameManager.instance.playerRigidbody.addAcceleration(transform.forward, 30);
                    if (GameManager.instance.difficultyProfile != null)
                        GameManager.instance.playerState.TakeDamage(30 * GameManager.instance.difficultyProfile.enermyDamage);
                    else
                        GameManager.instance.playerState.TakeDamage(30);

                    if ((GameManager.instance.modsProfile != null && !GameManager.instance.modsProfile.immunityBleeding) && UnityEngine.Random.Range(0, 3) == 1)
                        GameManager.instance.playerState.TakeDamage(0, PlayerState.m_deburf.Bloody);
                    if ((GameManager.instance.modsProfile != null && !GameManager.instance.modsProfile.immunityConfusion) && UnityEngine.Random.Range(0, 3) == 1)
                        GameManager.instance.playerState.TakeDamage(0, PlayerState.m_deburf.Confusion);
                    agent.velocity *= 0;
                    if (screamTick == 0)
                    {
                        screamTick++;
                        PlayRandomAudio(screamSounds);
                        anim.CrossFadeInFixedTime("Scream", 0);
                    }
                }
            }
            else if (ai_state == STATE.Stalk)
            {
                runWayPos = Vector3.zero;
                //StopCoroutine(coroutine);
                //coverOBJ = null;
                slideTick = 0;
                agent.speed = runSpeed;
                agent.acceleration = runSpeed;
                target = GameManager.instance.playerState.gameObject;
                if (Vector3.Distance(transform.position, target.transform.position) < rushRange)
                {
                    ai_state = AI_8Feet.STATE.Rush;
                    GameManager.instance.playerState.audioSource.PlayOneShot(warningSounds[UnityEngine.Random.RandomRange(0, warningSounds.Length)]);
                    PlayRandomAudio(screamSounds);
                    GameManager.instance.playerState.chaseAudioSources.volume = 1;
                    GameManager.instance.playerState.chaseAudioSources.Play();
                    anim.CrossFadeInFixedTime("StandUP", 0);
                }
                anim.SetFloat("RunAniSpeed", 1.5f);
            }
            if (isPaining())
            {
                if(ai_state != STATE.Run)
                {
                    agent.speed = PainingSpeed;
                    agent.acceleration = PainingSpeed;
                    if (agent.velocity.magnitude > PainingSpeed)
                        agent.velocity /= 1.1f;
                }
            }
            if (isScreaming())
                agent.velocity *= 0;
            else
                screamTick = 0;

            Vector3 lookrotation = agent.steeringTarget - transform.position;
            if (IsInPivot())
                rootSpeed = 10;
            if (lookrotation != Vector3.zero)
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lookrotation), rootSpeed * Time.deltaTime);
            if (!isScreaming() && agent.isOnNavMesh && target != null)
                agent.SetDestination(target.transform.position);



        }
    }

    private void ReSpawn()
    {
        agent.enabled = false;
        Vector3 pos;
        RaycastHit hit;
        do
        {
            pos = GameManager.instance.playerState.transform.position;
            pos.x += UnityEngine.Random.Range(-60, 60);
            pos.z += UnityEngine.Random.Range(-60, 60);
            pos.y += 9999;
            if (Physics.Raycast(new Vector3(pos.x, pos.y, pos.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14)))
                pos.y = hit.point.y;
            else
                continue;
        }
        while (Vector3.Distance(new Vector3(pos.x, GameManager.instance.playerState.transform.position.y, pos.z), GameManager.instance.playerState.transform.position) < 50 || pos.y >= 9999);
        transform.position = pos;
        agent.enabled = true;
    }
    public void FixedUpdate()
    {
        base.FixedUpdate();
        //다른 프젝
        charAngle = 0f;
        direction = 0f;

        // Translate controls stick coordinates into world/cam/character space
        if (target != null)
            StickToWorldspace(this.transform, ref direction, ref charAngle, IsInPivot());
        anim.SetFloat("Speed", agent.velocity.magnitude / 5, 0.05f, Time.deltaTime);
        anim.SetFloat("Direction", direction, 0.25f, Time.deltaTime);

        if (agent.velocity.magnitude > LocomotionThreshold)    // Dead zone
            if (!IsInPivot())
                anim.SetFloat("Angle", charAngle);
        if (agent.velocity.magnitude < LocomotionThreshold)    // Dead zone
        {
            anim.SetFloat("Direction", 0f);
            anim.SetFloat("Angle", 0f);
        }
    }
    public void StickToWorldspace(Transform root, ref float directionOut, ref float angleOut, bool isPivoting)
    {
        Vector3 rootDirection = root.forward;

        Vector3 stickDirection = new Vector3(target.transform.position.x, 0, target.transform.position.z) - new Vector3(transform.position.x, 0, transform.position.z);


        // Convert joystick input in Worldspace coordinates
        Vector3 moveDirection = stickDirection;
        Vector3 axisSign = Vector3.Cross(moveDirection, rootDirection);



        float angleRootToMove = Vector3.Angle(rootDirection, moveDirection) * (axisSign.y >= 0 ? -1f : 1f);
        if (!isPivoting)
        {
            angleOut = angleRootToMove;
        }
        angleRootToMove /= 180f;

        directionOut = angleRootToMove * 1.5f;
    }
    /*
    IEnumerator FindClosestCover()
    {
        int layerMask = 1 << 10;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, Mathf.Infinity, layerMask);

        float mDist = float.MaxValue;
        Collider closest = null;

        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].gameObject != gameObject)
            {
                if (coverOBJ != null && hitColliders[i] == coverOBJ)
                    i++;
                if (i < hitColliders.Length)
                {
                    float tDist = Vector3.Distance(hitColliders[i].transform.position, transform.position);
                    if (tDist < mDist)
                    {
                        mDist = tDist;
                        closest = hitColliders[i];
                    }
                }
            }
        }
        coverOBJ = closest;
        yield return new WaitForSeconds(.1f);
    }*/
    private void FindRunWay()
    {
        Vector3 pos;
        RaycastHit hit;

        do
        {
            pos = GameManager.instance.playerState.transform.position;
            pos.x += UnityEngine.Random.Range(-80, 80);
            pos.z += UnityEngine.Random.Range(-80, 80);
            pos.y += 9999;
            if (Physics.Raycast(new Vector3(pos.x, pos.y, pos.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14)))
                pos.y = hit.point.y;
            else
                continue;
        }
        while (Vector3.Distance(pos, GameManager.instance.playerState.transform.position) < 50 || pos.y >= 9999 || !agent.CalculatePath(pos, agent.path));

        runWayPos = pos;
    }
    public override void takeDamage(float damage, HitPoint.HitParts hitPart, bool isMelee)
    {
        /*if (hitPart == HitPoint.HitParts.Head)
        {
            currentfaintStack += damage;
            health -= damage * 2;
        }
        else
        {
            health -= damage;
        }
        if (currentfaintStack > maxfaintStack)
        {
            anim.enabled = false;
            agent.enabled = false;
            return;
        }*/
        //ai.velocity /= damage/1.5f;
        base.takeDamage(damage, hitPart, isMelee);
        if (health <= 0)
        {
            PlayRandomAudio(painSounds);
            if (!isDownPaining())
                if (isStand())
                {
                    if (hitPart == HitPoint.HitParts.Head)
                    {
                        switch (UnityEngine.Random.Range(1, 3))
                        {
                            case 1:
                                anim.CrossFadeInFixedTime("Pain.PainHead", 0);
                                break;
                            case 2:
                                anim.CrossFadeInFixedTime("Pain.PainHead2", 0);
                                break;
                        }
                    }

                    else if (hitPart == HitPoint.HitParts.Others)
                        switch (UnityEngine.Random.Range(1, 3))
                        {
                            case 1:
                                anim.CrossFadeInFixedTime("Pain.PainBody", 0);
                                break;
                            case 2:
                                anim.CrossFadeInFixedTime("Pain.PainBody2", 0);
                                break;
                        }
                    else if (hitPart == HitPoint.HitParts.Legs)
                        switch (UnityEngine.Random.Range(1, 4))
                        {
                            case 1:
                                anim.CrossFadeInFixedTime("DownPain1", 0);
                                anim.CrossFadeInFixedTime("DownPain.DownPain1", 0);
                                break;
                            case 2:
                                anim.CrossFadeInFixedTime("DownPain2", 0);
                                anim.CrossFadeInFixedTime("DownPain.DownPain2", 0);
                                break;
                            case 3:
                                anim.CrossFadeInFixedTime("DownPain3", 0);
                                anim.CrossFadeInFixedTime("DownPain.DownPain3", 0);
                                break;
                        }
                }
                else
                    anim.CrossFadeInFixedTime("Pain", 0);
        }
    }
    public bool isStand()
    {
        return !(stateInfo.IsName("Crawl.Crawlling") || stateInfo.IsName("Crawl.Pain"));
    }
    public float LocomotionThreshold { get { return 0.2f; } }
    public bool IsInPivot()
    {
        return stateInfo.IsName("loco.LocomotionPivotL") ||
            stateInfo.IsName("loco.LocomotionPivotR") ||
            transInfo.IsName("loco.Locomotion -> loco.LocomotionPivotL") ||
            transInfo.IsName("loco.Locomotion -> loco.LocomotionPivotR") ||
            stateInfo.IsName("Stand.slide");
    }
    public bool isPaining()
    {
        return anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain1") ||
            anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain2") ||
            anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain3") ||
            anim.GetCurrentAnimatorStateInfo(1).IsName("PainBody") ||
            anim.GetCurrentAnimatorStateInfo(1).IsName("PainHead") ||
            anim.GetCurrentAnimatorStateInfo(1).IsName("PainBody2") ||
            anim.GetCurrentAnimatorStateInfo(1).IsName("PainHead2") ||
            anim.GetCurrentAnimatorStateInfo(0).IsName("Pain");
    }
    public bool isScreaming()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("Scream");
    }
    public bool isCrawlling()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("Crawlling");
    }
    public bool isDownPaining()
    {
        return anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain1") ||
            anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain2") ||
            anim.GetCurrentAnimatorStateInfo(2).IsName("DownPain3");
    }
    public bool isFaintAway()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("StandingUp");
    }
    void OnAnimatorIK(int layerIndex)
    {
        if (anim != null && !isFaintAway() && target != null)
        {

            anim.SetLookAtWeight(0.7f);
            if (target.GetComponent<PlayerState>() == null)
                anim.SetLookAtPosition(target.transform.position);
            else
                anim.SetLookAtPosition(target.transform.GetChild(0).position);
        }
    }
    public void Spawn()
    {
        agent.enabled = false;
        Vector3 pos = GameManager.instance.playerState.gameObject.transform.position;
        do
        {
            
            pos = GameManager.instance.playerState.transform.position;
            pos.x += UnityEngine.Random.Range(-60, 60);
            pos.z += UnityEngine.Random.Range(-60, 60);
            pos.y += 9999;
            RaycastHit hit;
            if (Physics.Raycast(new Vector3(pos.x, pos.y, pos.z), Vector3.down, out hit, Mathf.Infinity, (1 << 14)))
                pos.y = hit.point.y;
            else
                continue;
        }
        while (Vector3.Distance(new Vector3(pos.x, GameManager.instance.playerState.transform.position.y, pos.z), GameManager.instance.playerState.transform.position) < 50 || pos.y >= 9999);
        maxRunStack++;
        currentRunStack = maxRunStack;
        transform.position = pos;
        agent.enabled = true;
    }

}
