﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
public class AI_Monster : MonoBehaviour
{
    
    [SerializeField] protected AudioSource m_AudioSource;
    public int id;
    public Animator anim;
    public NavMeshAgent agent;
    public float maxHealth;
    public float health;
    

    [SerializeField] protected AudioClip[] stepsSounds;
    public AudioClip[] headShotSounds;
    

    private float m_StepCycle;
    private float m_NextStep;
    [SerializeField] private float m_StepInterval;
    // Start is called before the first frame update
    public bool inWater;

    public float hurtTime;
    public virtual void Awake()
    {
        hurtTime = 0;
        m_StepCycle = 0f;
        m_NextStep = 0.5f;
        anim = GetComponent<Animator>();
        if(GetComponent<AudioSource>() != null)
            m_AudioSource = GetComponent<AudioSource>();
        agent = GetComponent<NavMeshAgent>();
        health = maxHealth;
    }
    public virtual void Start()
    {
        
        id = Random.Range(0, 99999);
            maxHealth *= GameManager.instance.difficultyProfile.enermyHealth * GameManager.instance.modsProfile.zombieHealth;
        
    }
    public void OnEnable()
    {
        StartCoroutine(hurtInit());
    }
    IEnumerator hurtInit()
    {
        while(true)
        {
            if (hurtTime > 0)
                hurtTime -= Time.deltaTime;
            else if (hurtTime < 0)
                hurtTime = 0;
            yield return new WaitForEndOfFrame();
        }
    }

    public void FixedUpdate()
    {
        ProgressStepCycle();
    }
    private void ProgressStepCycle()
    {
        if (agent.velocity.sqrMagnitude > 0)
            m_StepCycle += (agent.velocity.magnitude) * Time.fixedDeltaTime;

        if (!(m_StepCycle > m_NextStep))
            return;

        m_NextStep = m_StepCycle + m_StepInterval;

        PlayFootStepAudio();
    }
    public void PlayFootStepAudio()
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, stepsSounds.Length);
        m_AudioSource.clip = stepsSounds[n];
        m_AudioSource.Play();
        // move picked sound to index 0 so it's not picked next time
        stepsSounds[n] = stepsSounds[0];
        stepsSounds[0] = m_AudioSource.clip;
    }

    public virtual void takeDamage(float damage, HitPoint.HitParts hitPart, bool isMelee)
    {
        if (health <= 0)
            m_AudioSource.Stop();
        GameManager.instance.weaponController.PlayHitMark();
        if (hitPart == HitPoint.HitParts.Head)
        {
            PlayRandomAudio(headShotSounds);
            health -= damage * 2;
        }
        else
            health -= damage;
        
    }
    protected void PlayRandomAudio(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, audios.Length);
        m_AudioSource.clip = audios[n];
        m_AudioSource.PlayOneShot(m_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = m_AudioSource.clip;
    }
}
