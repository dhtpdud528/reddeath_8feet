﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coverOBJ : MonoBehaviour {
    public Transform dir;
    public Transform surfaceChecker;
    public Transform point;
    private Transform playerTf;

    // Use this for initialization
    void Start () {
        dir = transform.GetChild(0);
        surfaceChecker = dir.transform.GetChild(0);
        point = transform.GetChild(1);
        gameObject.layer = 10;
        playerTf = FindObjectOfType<PlayerState>().gameObject.transform;

    }
	
	// Update is called once per frame
	void Update () {
        if(playerTf != null)
        dir.LookAt(new Vector3(playerTf.transform.position.x, transform.position.y, playerTf.transform.position.z));

        RaycastHit hit;
        if (Physics.Raycast(surfaceChecker.position, transform.position - surfaceChecker.position, out hit, Vector3.Distance(surfaceChecker.position, transform.position), 1 << 10))
        {
            point.transform.position = hit.point;
            point.LookAt(transform.position);
            point.Translate(-Vector3.forward);
        }
        new WaitForSeconds(.1f);
	}
}