﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierController : MonoBehaviour
{
    Animator anim;
    AudioSource _AudioSource;
    public float vertical;
    public float horizontal;
    public bool crouch;
    public bool aim;
    public Transform rh;
    public bool fire;
    public ItemGun gun;

    public Transform shootPoint;
    public AI_Monster target;
    [SerializeField] private GameObject bullet;
    private float fireTimer = 0;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        _AudioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("Vertical", vertical);
        anim.SetFloat("Horizontal", horizontal);
        if(vertical == 0 && horizontal == 0)
            anim.SetFloat("Speed", 0);
        else
            anim.SetFloat("Speed", 1);
        anim.SetBool("Crouch", crouch);
        anim.SetBool("Aim", aim);

        if (fireTimer < gun.fireRate)
            fireTimer += Time.deltaTime;
        if (fire)
        {
            if (target != null)
            {
                shootPoint.LookAt(new Vector3(target.transform.position.x, target.transform.position.y+1f, target.transform.position.z));
                if (target.health <= 0)
                    target = null;
                Fire();
            }
            else
                target = searchTarget();
        }
    }
    void OnAnimatorIK(int layerIndex)
    {
        if (rh != null)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
            anim.SetIKPosition(AvatarIKGoal.RightHand, rh.position);
            anim.SetIKRotation(AvatarIKGoal.RightHand, rh.rotation);
        }
    }
    private AI_Monster searchTarget()
    {
        AI_Monster tempTarget = null;
        float tempDis = Mathf.Infinity;
        int layerMask = (1 << 12);
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 50, layerMask);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].GetComponent<AI_Monster>() != null&& hitColliders[i].GetComponent<AI_Monster>().health > 0 && Vector3.Distance(transform.position, hitColliders[i].transform.position) < tempDis)
            {
                tempDis = Vector3.Distance(transform.position, hitColliders[i].transform.position);
                tempTarget = hitColliders[i].GetComponent<AI_Monster>();
            }
        }
        return tempTarget;
    }
    private void Fire()
    {
        if (fireTimer < gun.fireRate) return;
        _AudioSource.PlayOneShot(gun.shootSound);

        Vector3 shootDir = shootPoint.transform.forward;

        shootDir.z += UnityEngine.Random.Range(-gun.MOA / 2, gun.MOA / 2);
        shootDir.x += UnityEngine.Random.Range(-gun.MOA / 2, gun.MOA / 2);
        shootDir.y += UnityEngine.Random.Range(-gun.MOA / 2, gun.MOA / 2);
        GameObject bulletobj = Instantiate(bullet, shootPoint.position, Quaternion.FromToRotation(Vector3.forward, shootPoint.forward));
        bulletobj.transform.GetChild(0).GetComponent<Rigidbody>().AddForce(shootDir * 500, ForceMode.Impulse);
        bulletobj.transform.GetChild(1).GetComponent<EntityBullet>().gun = gun;
        bulletobj.transform.GetChild(1).GetComponent<Rigidbody>().AddForce(shootDir * 500, ForceMode.Impulse);
        bulletobj.transform.GetChild(1).GetComponent<BoxCollider>().size = new Vector3(
            bulletobj.transform.GetChild(1).GetComponent<BoxCollider>().size.x * gun.bulletWidth,
            bulletobj.transform.GetChild(1).GetComponent<BoxCollider>().size.y * gun.bulletHeight,
            bulletobj.transform.GetChild(1).GetComponent<BoxCollider>().size.z * gun.bulletLength);
        /*if (Physics.Raycast(shootPoint.position, shootDir, out hit, range, (1 << 10) + (1 << 0) + (1 << 12) + (1 << 14)))
        {
            GameObject bulletImpact;
            if (hit.collider.gameObject.tag == "Monster")
                bulletImpact = bodyBulletImpact;
            else
                bulletImpact = rockBulletImpact;
            GameObject bulletHole = Instantiate(bulletImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
            bulletHole.transform.SetParent(hit.transform);
        }*/
        gun.muzzleFlash.Play();
        //PlayShootSound();


        fireTimer = 0;
    }
}
