﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaterBurff : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Monster") && other.GetComponent<AI_Monster>() != null)
            other.GetComponent<AI_Monster>().inWater = true;

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Monster") && other.GetComponent<AI_Monster>() != null)
            other.GetComponent<AI_Monster>().inWater = false;
    }
}
